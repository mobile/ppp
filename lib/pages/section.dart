import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:ppp_flutter/assets/app_resources.dart';
import 'package:ppp_flutter/assets/articles_resources.dart';
import 'package:ppp_flutter/views/contacts_page.dart';
import 'package:ppp_flutter/utils/utils.dart';
import 'package:ppp_flutter/pages/html_section.dart';
import "package:ppp_flutter/views/adaptive_page.dart";
import 'package:ppp_flutter/assets/articles_html_resources.dart';
import 'package:ppp_flutter/pages/rapid_section.dart';

final sections = <Section>[
  Section("assets/icons/01.png", "Co je první psychická pomoc",
      article: <Widget>[SectionPage(html_article_1)]),
  Section("assets/icons/02.png", "Základní postupy PPP", subSections: _sub_1),
  Section("assets/icons/03.png", "Další aktivity při PPP", subSections: _sub_2),
  Section("assets/icons/05.png", "Co nedělat", article: <Widget>[
    SectionPage(
      html_article_5_1,
    )
  ]),
  Section("assets/icons/08.png", "Skupinová PPP", subSections: _sub_5),
  Section("assets/icons/04.png", "Specifické skupiny", subSections: _sub_3),
  Section("assets/icons/07.png", "Obranné mechanismy", article: <Widget>[
    SectionPage(
      html_article_7_1,
    )
  ]),
  Section("assets/icons/06.png", "Stresové reakce", subSections: _sub_4),
  Section("assets/icons/09.png", "Jak se postarat po zásahu o sebe",
      article: <Widget>[
        SectionPage(
          html_article_9_1,
        )
      ]),
  Section("assets/icons/11.png", "Jak posílit odolnost", url: youtubeLink),
  Section("assets/icons/10.png", "Kontakty", subPage: ContactsPage()),
];

final _sub_1 = <Section>[
  Section("assets/icons/02-1.png", "RAPID",
      article: <Widget>[RapidSection()], inList: false),
  Section(
      "assets/icons/02-2.png", "Kroky PPP (dle psychologické služby HZS ČR)",
      article: <Widget>[SectionPage(html_article_2_2)]),
  Section("assets/icons/02-3.png", "6P (dle Pančochové)",
      article: <Widget>[SectionPage(html_article_2_3)]),
];
final _sub_2 = <Section>[
  Section("assets/icons/03-1.png", "Stabilizace", article: <Widget>[
    SectionPage(
      html_article_3_1_1,
    ),
    SpoilerButtonView("Postup uzemnění", SectionPage(html_article_3_1_2)),
    SectionPage(html_article_3_1_3)
  ]),
  Section("assets/icons/03-2.png", "Bezpečí a útěcha", article: <Widget>[
    SectionPage(
      html_article_3_2_1,
    ),
    SpoilerButtonView(
        "Asistence dětem bez doprovodu", SectionPage(html_article_3_2_2)),
    SectionPage(html_article_3_2_3),
    SpoilerButtonView("Pomoc lidem, kteří pohřešují někoho blízkého",
        SectionPage(html_article_3_2_4)),
    SectionPage(html_article_3_2_5),
    SpoilerButtonView("Pomoc lidem, kterým zemřel někdo blízký",
        SectionPage(html_article_3_2_6)),
    SectionPage(html_article_3_2_7),
    SpoilerButtonView("Pomoc dětem a dospívajícím se smrtí blízké osoby",
        SectionPage(html_article_3_2_8)),
    SectionPage(html_article_3_2_9),
    SpoilerButtonView(
        "Jak děti chápou v různém věku smrt", SectionPage(html_article_3_2_10))
  ]),
  Section("assets/icons/03-3.png", "Praktická pomoc", article: <Widget>[
    SectionPage(
      html_article_3_3_1,
    ),
    SpoilerButtonView(
        "Příklad vyjasňování potřeb", SectionPage(html_article_3_3_2)),
    SectionPage(
      html_article_3_3_3,
    ),
    SpoilerButtonView("Jednání při řešení potřeb zasažených",
        SectionPage(html_article_3_3_4)),
    SectionPage(
      html_article_3_3_5,
    )
  ]),
  Section("assets/icons/03-4.png", "Navázání kontaktu ", article: <Widget>[
    SectionPage(
      html_article_3_4_1,
    ),
    SpoilerButtonView("Příklad", SectionPage(html_article_3_4_2)),
    SectionPage(
      html_article_3_4_3,
    )
  ]),
  Section("assets/icons/03-5.png", "Shromažďování informací", article: <Widget>[
    SectionPage(
      html_article_3_5_1,
    )
  ]),
  Section("assets/icons/03-6.png", "Propojení se sociální podporou",
      article: <Widget>[
        SectionPage(
          html_article_3_6_1,
        ),
        SpoilerButtonView("Pomáhání těm, co jsou uzavření do sebe",
            SectionPage(html_article_3_6_2)),
        SectionPage(
          html_article_3_6_3,
        ),
        SpoilerButtonView(
            "Pozitivní podporující reakce", SectionPage(html_article_3_6_4))
      ]),
  Section("assets/icons/03-7.png",
      "Informace o reakcích a možnostech jejich zvládání",
      article: <Widget>[
        SectionPage(
          html_article_3_7_1,
        ),
        SpoilerButtonView(
            "Běžné stresové reakce", SectionPage(html_article_3_7_2)),
        SectionPage(
          html_article_3_7_3,
        ),
        SpoilerButtonView("Připomínky traumatu, ztráty nebo změny",
            SectionPage(html_article_3_7_4)),
        SectionPage(html_article_3_7_5),
        SpoilerButtonView(
            "Strategie zvládání",
            SectionPage(
              html_article_3_7_6,
            )),
        SectionPage(html_article_3_7_7),
        SpoilerButtonView(
            "Jednoduché relaxační techniky",
            SectionPage(
              html_article_3_7_8,
            )),
        SectionPage(html_article_3_7_9),
        SpoilerButtonView(
            "Pomoc rodinám při vypořádávání se s problémem",
            SectionPage(
              html_article_3_7_10,
            )),
        SectionPage(
          html_article_3_7_11,
        ),
        SpoilerButtonView(
            "Řešení problémů s hněvem", SectionPage(html_article_3_7_12)),
        SectionPage(
          html_article_3_7_13,
        ),
        SpoilerButtonView(
            "Práce s negativními emocemi", SectionPage(html_article_3_7_14)),
        SectionPage(
          html_article_3_7_15,
        ),
        SpoilerButtonView(
            "Podporujte následující zdravé návyky",
            SectionPage(
              html_article_3_7_16,
            )),
        SectionPage(html_article_3_7_17),
        SpoilerButtonView(
            "Řešení užívání alkoholu a návykových látek",
            SectionPage(
              html_article_3_7_18,
            ))
      ]),
];

final _sub_3 = <Section>[
  Section("assets/icons/04-1.png", "Lidé se zrakovým postižením",
      article: <Widget>[
        SectionPage(
          html_article_4_1,
        )
      ]),
  Section("assets/icons/04-2.png", "Lidé se sluchovým postižením",
      article: <Widget>[
        SectionPage(
          html_article_4_2,
        )
      ]),
  Section("assets/icons/04-3.png", "Lidé s psychickými potížemi",
      article: <Widget>[
        SectionPage(
          html_article_4_3,
        ),
        SpoilerButtonView(
            "Desatero komunikace s pacienty s poruchou autistického spektra",
            SectionPage(html_article_4_3_1))
      ]),
  Section("assets/icons/04-4.png", "Lidé s pohybovým postižením ",
      article: <Widget>[
        SectionPage(
          html_article_4_4,
        )
      ]),
  Section("assets/icons/04-5.png",
      "Cizinci, zástupci národnostních, kulturních a náboženských menšin",
      article: <Widget>[
        SectionPage(
          html_article_4_5,
        )
      ]),
];

final _sub_4 = <Section>[
  Section("assets/icons/06-1.png", "Věk 1 – 5", article: <Widget>[
    SectionPage(
      html_article_6_1,
    )
  ]),
  Section("assets/icons/06-2.png", "Věk 6 – 11", article: <Widget>[
    SectionPage(
      html_article_6_2,
    )
  ]),
  Section("assets/icons/06-3.png", "Věk 12 - 18", article: <Widget>[
    SectionPage(
      html_article_6_3,
    )
  ]),
  Section("assets/icons/06-4.png", "Dospělí", article: <Widget>[
    SectionPage(
      html_article_6_4,
    )
  ]),
  Section("assets/icons/06-5.png", "Senioři", article: <Widget>[
    SectionPage(
      html_article_6_5,
    )
  ]),
];

final _sub_5 = <Section>[
  Section("assets/icons/08-1.png", "Defusing", article: <Widget>[
    SectionPage(
      html_article_8_1,
    )
  ]),
  Section("assets/icons/08-2.png", "Demobilizace", article: <Widget>[
    SectionPage(
      html_article_8_2,
    )
  ]),
  Section("assets/icons/08-3.png", "Krizový briefing", article: <Widget>[
    SectionPage(
      html_article_8_3,
    )
  ]),
];

class Section {
  final String imagePath;
  final String title;
  final List<Section> subSections;
  final List<Widget> article;
  final Widget subPage;
  // TODO: refactor this
  final bool inList;
  final String url;

  Section(this.imagePath, this.title,
      {this.subSections,
      this.article,
      this.subPage,
      this.inList: true,
      this.url});
}

class CustomMarkdownBody extends MarkdownBody {
  static final MarkdownStyleSheet ss = MarkdownStyleSheet(
      p: TextStyle(fontSize: 18.0, color: Colors.black),
      a: TextStyle(fontSize: 18.0, color: Colors.black),
      h1: TextStyle(
          fontSize: 26.0, color: Colors.black, fontWeight: FontWeight.w500),
      h2: TextStyle(
          fontSize: 24.0, color: Colors.black, fontWeight: FontWeight.w500),
      h3: TextStyle(
          fontSize: 22.0, color: Colors.black, fontWeight: FontWeight.w500),
      h4: TextStyle(
          fontSize: 20.0, color: Colors.black87, fontWeight: FontWeight.w500),
      h5: TextStyle(
          fontSize: 20.0, color: Colors.black54, fontWeight: FontWeight.w500),
      h6: TextStyle(
          fontSize: 20.0, color: Colors.black45, fontWeight: FontWeight.w500),
      strong: TextStyle(
          fontSize: 18.0, color: Colors.black, fontWeight: FontWeight.w600),
      em: TextStyle(
          fontSize: 18.0,
          color: Colors.black,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.w500),
      blockSpacing: 8.0,
      listIndent: 32.0,
      blockquotePadding: EdgeInsets.all(8.0),
      blockquoteDecoration: BoxDecoration(
          color: Colors.blue.shade100,
          borderRadius: BorderRadius.circular(2.0)),
      codeblockPadding: EdgeInsets.all(8.0),
      codeblockDecoration: BoxDecoration(
          color: Colors.grey.shade100,
          borderRadius: BorderRadius.circular(2.0)));

  CustomMarkdownBody({
    data,
  }) : super(
          data: data,
          styleSheet: ss,
        );
}

class SpoilerButtonView extends StatelessWidget {
  final String entityTitle;
  final Widget hiddenEntity;

  const SpoilerButtonView(this.entityTitle, this.hiddenEntity);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.centerRight,
        child: TextButton(
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute<Null>(builder: (BuildContext context) {
                return Scaffold(
                    appBar: AppBar(
                      title: Text(entityTitle),
                    ),
                    body: SpoilerView(hiddenEntity));
              }));
            },
            child: Text(showDetails,
                style: TextStyle(
                    fontSize: 18.0,
                    decoration: TextDecoration.underline,
                    fontStyle: FontStyle.italic))));
  }
}

class SpoilerView extends StatefulWidget {
  final Widget hiddenEntity;

  SpoilerView(this.hiddenEntity);

  @override
  State<StatefulWidget> createState() {
    return SpoilerViewState(hiddenEntity);
  }
}

class SpoilerViewState extends AdaptivePageViewState {
  final Widget hiddenEntity;

  SpoilerViewState(this.hiddenEntity);

  @override
  Widget buildMobileLayout(height, width) {
    return Container(
        child: ListView(
      padding: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 5.0),
      children: <Widget>[hiddenEntity],
    ));
  }

  @override
  Widget buildTabletLayout(height, width) {
    var horizontalPadding = 0.15 * width;

    return Container(
      alignment: Alignment.center,
      child: ListView(
          padding: EdgeInsets.fromLTRB(
              horizontalPadding, 20.0, horizontalPadding, 5.0),
          children: <Widget>[hiddenEntity]),
    );
  }
}
