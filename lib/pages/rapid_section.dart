import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:ppp_flutter/assets/articles_html_resources.dart';
import 'package:ppp_flutter/assets/app_resources.dart';
import 'package:ppp_flutter/main.dart';
import 'package:ppp_flutter/views/adaptive_page.dart';

class RapidSection extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RapidSectionState();
  }
}

class RapidSectionState extends State<RapidSection> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        //buttons
        Container(
            padding: EdgeInsets.all(5.0),
            child: TextButton(
                onPressed: () {
                  _openSubSection(html_article_2_1_1);
                },
                child: HtmlWidget(
                  html_article_2_1_R,
                  textStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                      fontSize: 18.0),
                ))),
        Container(
            padding: EdgeInsets.all(5.0),
            child: TextButton(
                onPressed: () {
                  _openSubSection(html_article_2_1_2);
                },
                child: HtmlWidget(
                  html_article_2_1_A,
                  textStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                      fontSize: 18.0),
                ))),
        Container(
            padding: EdgeInsets.all(5.0),
            child: TextButton(
                onPressed: () {
                  _openSubSection(html_article_2_1_3);
                },
                child: HtmlWidget(
                  html_article_2_1_P,
                  textStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                      fontSize: 18.0),
                ))),
        Container(
            padding: EdgeInsets.all(5.0),
            child: TextButton(
                onPressed: () {
                  _openSubSection(html_article_2_1_4);
                },
                child: HtmlWidget(
                  html_article_2_1_I,
                  textStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                      fontSize: 18.0),
                ))),
        Container(
            padding: EdgeInsets.all(5.0),
            child: TextButton(
                onPressed: () {
                  _openSubSection(html_article_2_1_5);
                },
                child: HtmlWidget(
                  html_article_2_1_D,
                  textStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                      fontSize: 18.0),
                ))),
        Container(
          padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
          child: HtmlWidget(
            html_article_2_1_text,
            textStyle: TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.black,
                fontSize: 18.0),
          ),
        ),
      ],
    );
  }

  _openSubSection(String data) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => RapidSubSection(data)));
  }
}

class RapidSubSection extends StatefulWidget {
  final String data;

  RapidSubSection(this.data);

  @override
  State<StatefulWidget> createState() {
    return AdaptiveRapidSubSectionState(this.data);
  }
}

class AdaptiveRapidSubSectionState extends AdaptivePageViewState {
  final String data;

  AdaptiveRapidSubSectionState(this.data);

  @override
  Widget buildMobileLayout(width, height) {
    return Scaffold(
        appBar: AppBar(),
        body: Container(
          child: ListView(padding: EdgeInsets.all(15.0), children: [
            HtmlWidget(this.data,
                textStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                    fontSize: 18.0))
          ]),
        ));
    ;
  }

  @override
  Widget buildTabletLayout(width, height) {
    return Scaffold(
        appBar: AppBar(),
        body: Container(
          child: ListView(
            padding:
                EdgeInsets.fromLTRB(0.15 * width, 25.0, 0.15 * width, 10.0),
            children: [
              HtmlWidget(
                this.data,
                textStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                    fontSize: 18.0),
              )
            ],
          ),
        ));
  }
}
