import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:flutter/material.dart';




class SectionPage extends StatelessWidget {

  final String html;
  SectionPage(this.html);

  @override
  Widget build(BuildContext context) {
    return Container(child: HtmlWidget(html, textStyle: TextStyle(fontWeight: FontWeight.w400, color: Colors.black, fontSize: 18.0), ));
  }

}