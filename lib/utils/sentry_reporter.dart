import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

Future<Null> reportSentryException(
    SentryClient sentry, dynamic exception, dynamic stackTrace) async {
  print('Caught error: ${exception}');

  print('Send error to sentry...');
  SentryId sentryId;

  try {
    sentryId =
        await sentry.captureException(exception, stackTrace: stackTrace);
  } catch (exception) {
    debugPrint(exception.toString());
    return;
  }

  /*if (response.isSuccessful) {
    print("Success! ID: ${response.eventId}");
  } else {
    print(
        "Failed post report to sentry(${Sentry.dsnUri}) error: ${response.exception}");
  }*/
}
