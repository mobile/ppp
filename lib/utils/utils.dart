import 'dart:math';

import 'package:shared_preferences/shared_preferences.dart';

const TABLET_VIEW_THRESHHOLD = 600;
const String DIALOG_SHOWN_KEY = "dialogShown";

bool useTabletView(width, height) {
  final double smallestDimension = min(
    width,
    height,
  );

  return smallestDimension > TABLET_VIEW_THRESHHOLD;
}

Future<bool> dialogSeen() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.containsKey(DIALOG_SHOWN_KEY);
}

Future<void> setDialogShown() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setBool(DIALOG_SHOWN_KEY, true);
}
