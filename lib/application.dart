import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:ppp_flutter/assets/app_resources.dart';
import 'package:ppp_flutter/pages/section.dart';
import 'package:ppp_flutter/views/about_page.dart';
import 'package:ppp_flutter/views/adaptive_page.dart';
import 'package:url_launcher/url_launcher.dart';

import 'utils/utils.dart';

class SectionsGridView extends StatelessWidget {
  final List<Section> sections;

  const SectionsGridView(this.sections);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.extent(
        maxCrossAxisExtent: 220.0,
        children: sections.map((section) => GridViewItem(section)).toList(),
      ),
    );
  }
}

class GridViewItem extends StatelessWidget {
  final Section section;

  GridViewItem(this.section);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          if (section.subSections != null) {
            Navigator.of(context)
                .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
              return Scaffold(
                  appBar: AppBar(
                    title: Text(section.title),
                  ),
                  body: SectionsGridView(section.subSections));
            }));
          } else if (section.article != null) {
            Navigator.of(context)
                .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
              return Scaffold(
                  appBar: AppBar(
                    title: Text(section.title),
                  ),
                  body: AdaptiveSectionView(section));
            }));
          } else if (section.subPage != null) {
            Navigator.of(context)
                .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
              return Scaffold(
                appBar: AppBar(
                  title: Text(section.title),
                ),
                body: section.subPage,
              );
            }));
          } else if (section.url != null) {
            launch(section.url);
          }
        },
        child: Card(
            child: Container(
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Image(
                    image: AssetImage(section.imagePath),
                    width: 100.0,
                    height: 100.0,
                    fit: BoxFit.scaleDown,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  child: AutoSizeText(section.title,
                      style: TextStyle(fontSize: 16.0),
                      textAlign: TextAlign.center),
                ),
              )
            ],
          ),
        )));
  }
}

class AdaptiveSectionView extends StatefulWidget {
  Section section;

  AdaptiveSectionView(this.section);

  @override
  State<StatefulWidget> createState() {
    return _HandyLayoutState(section);
  }
}

class _HandyLayoutState extends AdaptivePageViewState {
  Section section;

  _HandyLayoutState(this.section);

  @override
  Widget buildMobileLayout(width, height) {
    if (section.inList) {
      return Container(
          child: ListView(
              padding: EdgeInsets.all(15.0), children: section.article));
    } else {
      return Container(child: section.article[0]);
    }
  }

  @override
  Widget buildTabletLayout(width, height) {
    var verticalPadding = 0.15 * width;

    if (section.inList) {
      return Container(
          alignment: Alignment.center,
          child: Container(
            child: ListView(
                padding: EdgeInsets.fromLTRB(
                    verticalPadding, 25.0, verticalPadding, 10.0),
                children: section.article),
          ));
    } else {
      return Container(alignment: Alignment.center, child: section.article[0]);
    }
  }
}

class MainWidgetPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainWidgetPageState();
}

class MainWidgetPageState extends State<MainWidgetPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => InfoScreenWidget()),
                  );
                })
          ],
        ),
        body: SectionsGridView(sections));
  }

  @override
  void initState() {
    super.initState();
    _checkForNotificationDialogue();
  }

  Future<void> _checkForNotificationDialogue() async {
    if (!await dialogSeen()) {
      setDialogShown();
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => AlertDialog(
                title: Text("Nově máme YouTube kanál"),
                actions: [
                  TextButton(
                      child: Text("OK"),
                      onPressed: () => Navigator.of(context).pop())
                ],
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                        "Přejít na stranku YouTube Vám pomůže ikonka dole. Najdete ji take v hlavním menu"),
                    IconButton(
                      iconSize: 48.0,
                      icon: Image.asset("assets/icons/11.png"),
                      onPressed: () => launch(youtubeLink),
                    )
                  ],
                ),
              ));
      debugPrint("Dialog showed first time");
    } else {
      debugPrint("Dialog is seen");
    }
  }
}
