const String appTitle = "První Psychická Pomoc";

const String aboutAppTitle = "O Aplikaci";
const String aboutAppDetails =
    "Aplikace První Psychická Pomoc je pomůckou, která ukazuje možnosti pomoci lidem v těžkých životních situacích. Navazuje na kurzy, které absolvují příslušníci bezpečnostních sborů, ale také například lidé z nestátních neziskových organizací. Aplikace je praktickým vodítkem, které obsahuje postupy pro efektivní poskytování první psychické pomoci. Aplikaci vytvořila pracovní skupina Sekce psychologie krizí, katastrof a traumatu při ČMPS ve spolupráci s týmem mobilních vývojářů sdružení CZ.NIC z.s.p.o.";
const String appLicense =
    "Aplikace První Psychická Pomoc je vyvíjena pod licencí Creative Commons 4.0. Pro více informací o licenci CC BY 4.0 navštivte";
const String licenseLink = "https://creativecommons.org/licenses/by/4.0/";
const String showDetails = "klikněte zde pro více podrobností";

const String aboutAppImageAsset = "assets/icons/01.png";
const String aboutAppNicLogoImageAsset = "assets/icons/logo_nic.png";
const String youtubeLink =
    "https://www.youtube.com/channel/UC6X6bltq0p8UrXVJFbrp70g";
