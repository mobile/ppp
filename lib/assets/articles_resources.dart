const String article_1 = """
# Cíl 
Cílem první psychické pomoci (z angl. First Psychological Aid, někdy překládáno též jako první psychologická pomoc) je stabilizovat člověka, který se dostal do náročné životní situace. Je možné ji přirovnat k první zdravotnické pomoci, která slouží k zajištění základních životních funkcí člověka, než je předán do další odborné péče. V první psychické pomoci se snažíme člověka, který momentálně není schopen uplatnit své mechanismy zvládání stresu, stabilizovat tak, aby dokázal nastalou situaci zvládnout sám nebo si našel alespoň někoho, kdo mu pomůže. 
# Různé přístupy
## PPP v systému Critical Incident Stress Management podle G. S. Everlyho a C. L. Parkerové
- strukturovaný přístup, který byl původně vytvořen pro záchranáře, hasiče a policisty v USA
- přístup zaměřený na redukci distresu a vyhledání potřeb k zajištění péče o psychické zdraví člověka
- může přispívat k prevenci výskytu posttraumatických symptomů
## PPP podle National Child Traumatic Stres Network (NCTSN) a National Centre for PTSD (NCPTSD) 
- standardizovaný přístup zaměřený na pomoc dětem, dospívajícím, dospělým a rodinám, které zažívají bezprostřední následky krizových situací, např. katastrof či terorismu
- zaměřen na redukci počáteční tísně způsobené traumatickou událostí
- podporuje krátkodobé a dlouhodobé zvládací a vyrovnávací mechanismy
## PPP podle Světové zdravotnické organizace (WHO)
- lidská, vstřícná podpora lidem, kteří trpí a potřebují pomoc
- je určena lidem, kteří byli v poslední době vystaveni vážné krizové situaci
- není vnucována lidem, kteří ji nechtějí
- měla by být snadno přístupná pro ty, kteří pomoc a podporu potřebují a chtějí přijmout
""";

const String article_2_1 = """
Jednotlivé kroky **RAPID** není nutné provádět přesně v daném pořadí (pouze D je vždy na konci), mohou se dokonce i opakovat. Není také nutné projít všechny kroky, ale vždy je nezbytné udělat krok D a minimálně jeden ze čtyř ostatních.
**Reflektující naslouchání** je základní dovednost, kterou používáme při poskytování jakékoliv podpory druhým. Reflektuje se:
- **Událost** (např. *„Vidím, že máš v ruce telefon, asi jsi s někým mluvila.“*), člověku v krizi to pomáhá orientovat se v situaci a navrátit do reality.
- **Reakce** (např. *„Sedíš tady už nějakou dobu na posteli a díváš se do kouta.“*), pomáhá navázat kontakt interventa s jedincem v krizi.
- **Obsah** - parafrázování, shrnování (např. *„Říkala jsi, že …“, „Jestli tomu dobře rozumím tak, ….“*), lidé zjišťují, že má někdo o ně zájem, že na to nemusí být sami.
- **Emoce** (např. *„No tak to je mazec!“*, *„Vidím, že tě to vzalo, asi bych byla taky naštvaná, kdyby mi tohle řekl.“*), reflexe emocí pomáhá jejich uvolnění. 
Zlaté pravidlo PPP: __„Když nevíš jak dál, reflektuj!“__
# Analýza a zajištění základních potřeb
Identifikace základních potřeb se opírá o **Maslowovu hierarchii potřeb**. Potřeby jsou seřazeny v pořadí, v jakém by měly být zjišťovány.
- **Zdravotní péče** – zjišťujeme zranění, bolest, mdloby, nevolnost atd., ihned řešíme ošetřením nebo zajištěním zdravotní péče.
- **Fyzické potřeby** – pokládáme otázky: *„Kdy jste naposledy jedl? …pil? …spal? Potřebujete na toaletu?“* Velmi vhodné je v této situaci také nabídnutí nápoje (voda, čaj). 
- **Potřeba bezpečí** – zajištění bezpečí jak přímého (odvedení z nebezpečného místa, snížení rizika pádu apod.), tak nepřímého (odvedení člověka do bezpečného prostředí – aby na něj ostatní nebo konkrétní osoby neviděli, aby mohl dát bezpečně průchod svým emocím – pláči, vzteku, smutku).
- **Potřeba fungovat** – zajištění možnosti podílet se na své stabilizaci a zase převzít aktivitu, nedělat úkony za klienta, např. telefonovat, zjišťovat informace, které je schopen si zajistit sám apod. Lidé mají v sobě zakotvenou schopnost fungovat v různých situacích a cílem psychologické první pomoci by mělo být podpoření této schopnosti.
# Prvořadé zaopatření
- Umístění znaků a symptomů o kontextu krize: *„Je toto jednání adekvátní dané situaci?“*
- Hodnocení, zda je chování člověka v dané situaci funkční nebo dysfunkční. Je důležité ověřit, zda není chování člověka ovlivněno aktuální atakou psychického onemocnění, protože v takovém případě nelze člověka plně stabilizovat, je nutné zajistit odbornou lékařskou péči.
- Sledování **projevů dysfunkce** (dysfunkce je známkou potřeby psychologické první pomoci):
    - Disociace, člověk má pocit, že daná situace se ho netýká, jakoby vše vnímal za sklem, necítí emoce.
    - Deprese, skleslost, hluboký smutek, lidé jsou plačtiví, mají pocit, že to nikdy neskončí.
    - Psychosomatické potíže se vyskytují velmi často. Mnohdy jsou jediným projevem, který si často ani nemusíme spojovat s těžkou životní situací. Patří sem bolesti zad, hlavy, kloubů apod. Také se zhorší projevy alergií, objevují se neznámé vyrážky. Příznaků na tělesné úrovni je mnoho a měla by jim být věnována náležitá pozornost. 
    - Vina je vždy příznakem dysfunkce a je velmi riziková, protože může vést až k sebevraždě.
    - Poruchy spánku vedou často k vyčerpání a následně ke snížení schopnosti odolávat stresu. Tím se zasažený jedinec ocitá v začarovaném kruhu, ze kterého není úniku. Nemá tak možnost danou situaci zvládnout.
    - Panika je spojena s velkou úzkostí, která způsobuje roztříštěnost v prožívání a chování člověka je často neúčelné a zmatené.
    - Tendence k násilí má svůj původ ve zvýšené hladině adrenalinu, člověk je doslova nabuzen a má potřebu vybít nahromaděnou energii, proto jsou násilné projevy při prožívání krize poměrně časté.  
    - Spoléhání (zneužívání) na medikaci. Předčasné podávání utěšujících prostředků může zablokovat uplatnění přirozených vyrovnávacích mechanismů, kterými disponuje každý člověk.
    - Nedostatek sociální opory může být reálný (absence podpůrné sociální sítě), ale lidé zasažení krizí často nevnímají své okolí jako podporující, přestože se jim ostatní snaží pomoci, jak nejlépe dovedou.
    - Hyperventilace se řeší řízeným dýcháním, dýcháním do pytlíku, do dlaní, stabilizačním chvatem „přes dlouhé kosti“, tj. pevným stisknutím dlouhých kostí na předloktí k sobě.
# Intervence – uvažování a chování
- *„Veď, vzdělávej!“* Nebojte se být direktivní (zejména v situacích ohrožení života) a člověka v krizi dostatečně edukujte. Poskytované informace by měly být jasné, stručné a srozumitelné. Musí být podány nematoucím způsobem a v situaci, kdy je schopen člověk v těžké situaci vnímat obsah sdělovaného. 
- *„Získejte pozornost.“* Ptejte se na jméno klienta, jeho příjmení, na to, co se stalo. Snažte se zaměřit na situaci „tady a teď“ a na budování vztahu. 
- *„Dýchejte.“* Správné, klidné dýchání může uklidnit interventa i klienta. Doporučují se jeden až dva hluboké nádechy, poté dýchání volně dle potřeby. 
- *„Změňte myšlení.“* Pokuste se změnit způsob uvažování klienta, „přetnout kruh“ jeho katastrofického a nelogického myšlení. Je-li to vhodné a možné (nikoliv za každou cenu), pokuste se nalézt něco pozitivního. 
# Dej dál - propusť nebo předej
- Pokud je jedinec stabilizovaný, není nutné ho předávat do nějaké další odborné péče.
- První psychologická pomoc by měla směřovat také k identifikaci a aktivizaci přirozených zdrojů opory, např.
    - Přátelé
    - Rodina
    - Společnost (komunita, sousedé…)
    - Práce (kolegové, nadřízený…)
- Jestliže není jedinec schopný zvládat situaci sám, je vhodné ho nasměrovat na adekvátní následnou péči ke kompetentním odborníkům.""";

const String article_2_2 = """
# Navaž kontakt
- Najdi odvahu zasaženou osobu oslovit – *„Jsem hasič...“, „Stalo se...“, „Jak se jmenujete?“*
- Ujisti, dodej pocit bezpečí – *„Nebojte se, jsem u Vás, budu potřebovat Vaši  spolupráci…“*
# Zjisti zdravotní stav
- Je zraněný? Ošetři nebo ošetření zprostředkuj! *„Slyšíte mě? Bolí Vás něco? Víte, co se stalo?“*
- Chraň soukromí - dbej na důstojnost.
- Chraň zasaženou osobu před zvědavci, případně před médii. 
# Informuj, co se děje, a naslouchej
- Podávej reálné informace. *„Jsem tu proto, ...“, Bude se dít…“, „Lékař je na cestě.“, „Teď uslyšíte ránu, protože...“* atd.
- Vyslechni. Podpoř.   
- Vyhni se frázím. Mluv jednoduše, v krátkých větách.
# Zjišťuj a zajišťuj základní potřeby 
- Zajisti základní potřeby - pět T: teplo, ticho, tekutiny, transport, tišící prostředky. I o dospělého pečuj jako o dítě.
- Pil? Byl na záchodě? 
- Je mu dost teplo? Nabídni deku či svetr. 
- Je někdo s ním? Postrádá někoho?
- Má se o co opřít? Posaď, podepři, opři, hledej opatrně tělesný kontakt.  
- Má se kde umýt? Zajisti.
# Hlídej bezpečí
- Odveď ho, je-li mu místo nebezpečné nebo nepříjemné. (Využij váš automobil nebo pomoc lidí v okolí.)
- Zabraň pohybu, je-li pro něj nebezpečný. Jinak se můžeš pohybovat s ním.
- Dbej na vlastní bezpečí.
- Spolupracuj s druhými na místě. (Se sousedy, s cestujícími z vozů, kteří zastavili, aby pomohli, atd.)
- Pomoz přemýšlet o dalším postupu. (Odvoz domů, náhradní ubytování, apod.)
- Řeš situaci tady a teď, příp. bezprostřední následující kroky, nehledej dlouhodobá řešení.
# Předej do péče blízkých osob nebo dalších služeb
- Pomoz je kontaktovat. Koho, ať si určí sám.
Můžeš využít:
    - místní zdroje - sousedy, starostu, místní instituce,
    - přes operační středisko kontakty na odborné služby - krizová centra, linky důvěry,
    - tel. konzultace s psychologem IZS (příp. si jej vyžádej na místo), týmy posttraumatické a psychologické péče.""";

const String article_2_3 = """
# Přibliž se
- Najdi odvahu, zasaženého oslovit (na projevení lidskosti nemusíš být odborník).
- Představ se.
# Podepři
- Zajisti bezpečí.
- Odveď zasaženého z exponovaného místa. Posaď ho, opři o zeď, podepři svým tělem, stabilizační pozicí. 
- Chraň lidskou důstojnost zasaženého.
# Připomínej realitu 
- Oslovuj (jménem, příjmením, titulem).
- Ptej se, zda ví, co se stalo, kde se nachází.
- Poskytuj potřebné informace, pomáhej zorientovat se v situaci.
- Zaměřte se na dýchání – i slovně: „klidný nádech- výdech“.
# Podpoř 
- Podpoř projevení emocí.
- Uklidni zasaženého, že jeho prožívání a projevy jsou v takových náročných situacích normální.
- Projev důvěru v jeho schopnosti situaci zvládnout.
# Pečuj
- Zjišťuj potřeby, nabízej vodu, přikrývku, kapesník, umožnění vykonat intimní potřebu.
- Naslouchej (neboj se i mlčet).
- Citlivě poskytuj tělesný kontakt.
# Předej
- Nemusíš zvládat vše sám. 
- Předej zdravotníkům, příbuzným, kamarádům, sousedům.
Pokuste se společně najít kontakt, např. v mobilu
""";

const String article_3_1_1 = """
# Co to je?
- Cílem je uklidnit a zorientovat emocionálně rozrušené nebo dezorientované osoby
- Silné emoce, strnulost a úzkost jsou očekávatelné reakce po neštěstí. Většina zasažených bude schopna se uklidnit sama
- Věnujte pozornost těm jedincům, jejichž reakce ovlivňují jejich schopnost fungovat
# Kdo ji potřebuje?
- Dezorientovaný: věnuje se nesmyslnému dezorganizovanému chování.
- Odpojený: strnulý, překvapivě nedotčený událostí.
- Zmatený: neschopný porozumět co se děje kolem něj, nechápe smysl.
- V panice: extrémně úzkostný, neschopen se vyrovnat s událostí, oči doširoka otevřené a těkající.
- Nekontrolovaně vzlykající, hyperventilující.
- Přespříliš zaměstnaný: neschopen přemýšlet o čemkoliv jiném.
- V popření: odmítající přijmout, že se událost stala.
- Ve stavu šoku: neschopen se pohnout, ztuhlý.
- Nepřítomně hledící („čumí do blba“), mající skelný zrak.
- Nereagující na verbální otázky nebo povely.
- Projevující se zuřivým hledáním.
- Zavalený starostmi.
# Jak postupovat?
- Před intervencí se na krátkou chvilku zastav a všimněte si sami sebe.
- Zůstaňte klidní, potichu a přítomní, spíše než abyste se pokoušeli mluvit přímo na dotyčného člověka.
- Stůjte poblíž, když děláte jiné věci, tak abyste byli k dispozici, když to bude osoba potřebovat.
- Nabídněte podporu a pomozte zasaženému se soustředit na vybrané zvládnutelné pocity, myšlenky a cíle.
- Poskytujte ověřené informace, které osobu orientují v prostředí.
- Vyjasněte dezinformace nebo nedorozumění o tom, co se děje, a proberte starosti týkající se bezpečnosti.
- Vysvětlete, že intenzivní emoce mohou přicházet a odcházet ve vlnách, a že někdy může pomoci použití uklidňovacích technik (dýchání, rozptýlení) nebo podpora přátel/rodiny.
- Jestliže tyto reakce pokračují a ovlivňují fungování jedince, můžete zvážit použití uzemnění (grounding). 
""";



const String article_3_1_2 = """
# Zasaženým lidem můžete nabídnout tento postup:
- „Pohodlně se posaďte, bez překřížených rukou a nohou.
- Pomalu, normálně dýchejte.
- Vyberte a pojmenujte 5 nestresujících věcí, které jsou kolem vás.
- Pomalu, normálně dýchejte.
- Identifikujte a pojmenujte 5 nestresujících zvuků, které slyšíte.
- Pomalu, normálně dýchejte.
- Vyberte a pojmenujte 5 nestresujících věcí, které můžete cítit.
- Pomalu, normálně dýchejte.“
- Pro děti
- Můžete jim říct, aby vyjmenovaly barvy, které kolem sebe vidí. Například položte tyto otázky:
- „Můžeš říct 5 barev, které vidíš z tohoto místa, kde sedíš?
- Vidíš něco modrého? Něco žlutého? Něco zeleného?“
- Vyhledejte lékaře, je-li to vhodné/nutné.""";

const String article_3_1_3 = """- Vyhledejte lékaře, je-li to vhodné/nutné.""";

const String article_3_2_1 = """
# Co to je?
Cílem je zvýšit okamžité a trvalé bezpečí, a poskytnout fyzickou a emocionální útěchu.
# Jak postupovat?
- Poskytněte poslední, přesné a aktuální informace. Pokuste se vyhnout sdělování informací, které jsou nepřesné nebo velmi znepokojující.
- Soustřeďte se na činnosti, které jsou aktivní, praktické (použijte dostupné zdroje) a známé (čerpající z předchozích zkušeností).
- Identifikujte a proberte starosti o bezpečí. Najděte odpovědné osoby, které mohou vyřešit starosti o bezpečí, jež jsou mimo vaši kontrolu.
- Najděte jednoduchý způsob, jak vytvořit příjemnější okolní prostředí. 
- Monitorujte média a různé sociální sítě, abyste zachytili nepravdivé informace a uvedli je na pravou míru. 
- Podporujte skupinové a sociální interakce k podpoře přirozeného vyrovnávání se s událostí.
- Chraňte zasažené před dalším vystavováním nepříjemným zkušenostem nebo připomínáním traumatu (např. sledovat záchranné práce nebo média).
- Buďte k dispozici dětem bez doprovodu. """;

const String article_3_2_2 = """
- Sehněte se na výšku jejich očí, abyste byli méně ohrožujícími.
- Představte se a navažte kontakt.
- Postarejte se o základní potřeby (jídlo, voda, zdravotní ošetření).
- Zeptejte se na identifikační údaje (jména, jména rodičů/pečujících osob a sourozenců, adresu domů, na školu).
- Uvědomte odpovědné osoby.
- Poskytněte jim informace, kdo se o ně postará a co bude následovat.
- Neslibujte, co nemůžete splnit, a nelžete jim.
- Ujistěte se, že o je děti postaráno.""";

const String article_3_2_3 = """- Poskytněte čas a podporu lidem, kteří pohřešují někoho blízkého. 
""";

const String article_3_2_4 = """
- Popište kroky, které byly podniknuty k nalezení pohřešované osoby.
- Dovolte jim, aby kladli otázky a informujte je, co vše se dělá, aby byla pohřešovaná osoba nalezena.
- Podpořte jejich aktivity při hledání, které je nevystaví nebezpečí (zavolat přátelům, vyplnit registrační údaje).
- Pomozte členům rodiny pochopit, že mohou mít různé reakce (někteří se vzdávají naděje, jiní doufají) a podpořte je, aby byli trpěliví, měli pochopení a respektovali tyto rozdíly.
- Připravte je na poskytování údajů úřadům.""";

const String article_3_2_5 = """- Postarejte se o lidi, kterým zemřel někdo blízký""";

const String article_3_2_6 = """
- Uvědomte si své limity při mluvení o smrti. Pokud je pro vás těžké mluvit o smrti, poproste o pomoc kolegu.
- Poskytněte bezpečný prostor k truchlení.
- Naslouchejte pozorně a empaticky.
- Nechte je, aby vám řekli, co potřebují.
- Mluvte na jejich úrovni, zvláště s dětmi.
- Pamatujte na to základní – zůstaňte klidní a nespěchejte.
- Připomeňte jim, že neexistuje jediná správná cesta truchlení a že různí členové rodiny mohou jednat a cítit se různě. Podpořte trpělivost, porozumění a respekt.
- Ujistěte truchlící, že jejich reakce jsou pochopitelné. 
- Očekávejte široké pole reakcí při truchlení, někteří lidé vyjadřují silné emoce a někteří je nedávají najevo.
- U dětí se mohou nálady rychle střídat. Projevy jsou různé, i když se chovají jako obvykle, neznamená to, že netruchlí.""";

const String article_3_2_7 = """  - Odpovídejte na dotazy, jak pomoci dětem a dospívajícím vyrovnat se se smrtí někoho blízkého""";

const String article_3_2_8 = """
- Ujistěte děti, že jsou milovány a že o ně bude postaráno.
- Vnímejte, jestli jsou děti připraveny mluvit o tom, co se stalo. 
- Nenuťte děti mluvit. Někdy mohou potřebovat aktivity k odvedení pozornosti (hraní, kreslení, poslouchání muziky), aby se uklidnily.
- Odpovídejte na jejich otázky krátce, jednoduše, poctivě a přiměřeně věku.
- Naslouchejte jim, neodsuzujte, nekritizujte.
- Odpovězte na jejich otázky o pohřbech, pohřbívání, modlitbě a jiných rituálech.
- Buďte připraveni odpovídat na jejich otázky znovu a znovu.
- Nebojte se říct, že neznáte odpověď na otázku.
- Pamatujte, že děti různého věku mají rozdílné reakce na smrt někoho blízkého.""";

const String article_3_2_9 = """  - Jak děti chápou v různém věku smrt""";

const String article_3_2_10 = """Dětské porozumění smrti se liší dle jejich věku a předchozí zkušenosti se smrtí a je silně ovlivněno rodinou, vírou a kulturními faktory. 
- # Předškolní děti
Nemusí rozumět, že smrt je trvalá a mohou věřit, že se osoba může vrátit. Potřebují pomoci s potvrzením fyzické reality, že je osoba mrtvá (zesnulý už nedýchá, nehýbe se, nic necítí a necítí se nepohodlně nebo necítí bolest). Mohou mít starosti, že se něco špatného může stát jinému členovi rodiny.
- # Děti školního věku
Rozumí fyzické realitě smrti, ale mohou personifikovat smrt jako monstrum nebo kostru. Touží, aby se jejich milovaná osoba vrátila, mohou zakoušet pocity přítomnosti „ducha“.
- # Dospívající
Chápou, že smrt je nevratný proces. Ztráta člena rodiny nebo přítele může spustit vztek a impulzivní rozhodnutí, jako je opuštění školy, útěk nebo zneužívání drog a alkoholu. Tyto problémy vyžadují okamžitou pozornost rodiny nebo školy.
""";

const String article_3_3_1 = """
# Co to je?
Poskytnutí praktické pomoci zasaženým při řešení jejich okamžitých potřeb a obav.
# Jak postupovat?
- Rozpoznejte nejnaléhavější potřeby
- Vyjasněte si potřeby zasažených""";
const String article_3_3_2 = """
- # Dospělý/Pečovatel
„Z toho, co jste mi řekla, paní Nováková, jsem pochopil, že právě teď je vaším hlavním cílem najít manžela a ujistit se, že je v pořádku. Musíme se zaměřit na to, abychom vám pomohli se s ním dostat do kontaktu. Pojďme vymyslet plán, jak budeme postupovat při získávání informací.“
- # Dospívající/Dítě
„Mám pocit, že právě teď se hodně obáváš několika různých věcí. Co se stane s vaším domem, kdy přijde tvůj táta a co se bude dít dál. Všechny tyto věci jsou důležité, ale pojďme přemýšlet o tom, co je nejdůležitější právě v tuto chvíli a vymyslet nějaký plán.“""";
const String article_3_3_3 = """- Prodiskutujte plán, který je praktický a zároveň proveditelný.
- Při řešení problému se zaměřte na návrhy a rozhodnutí zasažených""";
const String article_3_3_4 = """# Jednání při řešení potřeb zasažených
- Pomozte zasaženým se zahájením jejich plánů, protože stres může mít negativní dopad na poznávací schopnosti a schopnosti řešit problémy. Je možné, že budou potřebovat vaši pomoc při plánování schůzek, s kompletováním administrativy a dalším.
- Pomozte zasaženým uvědomit si jejich potřeby a rozdělte jejich realizaci do menších kroků. Budou mít lepší pocit, že se dokáží s daným problémem vypořádat a mít prospěch z vědomí, že mohou zajistit sami sebe.
- Pamatujte, že každý zasažený prožívá situaci jinak. Někteří lidé mohou být aktivně samostatní, zatímco jiní se budou více spoléhat na informace a instrukce od vás. Přizpůsobte váš přístup potřebám zasažených a dané situaci.""";
const String article_3_3_5 = """- Před pomocí zasaženým si uvědomte, jaké služby jsou pro ně dostupné. Poté budete připraveni zajistit užitečnou praktickou asistenci, při které uplatníte základní prostředky, jako je jídlo, oblečení, bydlení, zdravotnická péče, finanční pomoc a další služby.
- Ujistěte se, že vaše informace jsou přesné a aktuální. Znalostí dostupných poskytovaných služeb, pomůžete zasaženým stanovit reálná očekávání. """;


const String article_3_4_1 = """
# Co to je?
Zahájení komunikace se zasaženými nenásilným, nevtíravým, citlivým způsobem.
# Jak postupovat?
- Nejdříve reagujte na ty, kteří vás vyhledají.
- Pokud se na vás současně obrátí více lidí, navažte kontakt s tolika lidmi, s kolika jste schopni.
- Projevení zájmu a starostlivosti může být nápomocné pro lidi, kteří se cítí mizerně nebo zmateně.
- Představte se jménem, uveďte svou funkci a krátce popište vaši úlohu.""";
const String article_3_4_2 = """Například:
*„Dobrý den. Jmenuji se _______. Jsem záchranář. Snažím se zjistit, jak se tady lidem vede, jestli jim můžu nějak pomoci. Nevadilo by vám, kdybych si s vámi na chvíli popovídal? Mohu se zeptat, jak se jmenujete?
Paní Nováková, než začneme s povídáním, nepotřebujete právě teď něco? Například trochu vody nebo přikrývku?“*""";
const String article_3_4_3 = """- Pokud jedinec odmítne vaši nabídku pomoci, respektujte jeho/její rozhodnutí a informujte, na koho se může později obrátit.
- Nepředpokládejte, že s vámi lidé budou hned mluvit a reagovat na vás pozitivně. Někomu může trvat déle, než bude cítit dostatek bezpečí, jistoty a důvěry.
- Zeptejte se na okamžité potřeby.
- Respektujte osobní a kulturní hranice týkající se fyzického doteku, osobního prostoru a očního kontaktu.
- Pokuste se zajistit trochu soukromí a věnujte zasaženému vaši plnou pozornost.""";
const String article_3_5_1 = """
# Co to je?
Shromažďování a vyhodnocování informací začíná okamžitě po navázání kontaktu a pokračuje v průběhu první psychické pomoci. Vaše schopnost shromažďovat informace bude omezena časem, potřebami zasažených, prioritami a dalšími faktory.
# Jak postupovat?
- Shromážděte dostatek informací, abyste byli schopní reagovat na okamžité potřeby a obavy zasažených.
- Tento proces je neformální a probíhá během celé komunikace se zasaženým. Vyhněte se tomu, aby měl člověk pocit, že ho vyslýcháte. 
- Držte se toho, co vám člověk říká. Vyhněte se požadování podrobných popisů prožité události, které by mohly vyvolat dodatečnou tíseň. 
- Je důležité ptát se na otázky, které vám mohou pomoci se rozhodnout:
    - Jaké základní kroky PPP jsou na místě
    - Zda je třeba zasaženého okamžitě předat odborníkovi
    - Jestli jsou potřeba další služby """;
const String article_3_6_1 = """
# Co to je?
Pomoc navázat kontakt s blízkými osobami a dalšími zdroji podpory zahrnující členy rodiny, přátele a sousedy.
# Jak postupovat
- Pomozte zasaženým se spojit s rodinou, přáteli a dalšími milovanými osobami (osobně, telefonicky, emailem, přes sociální sítě).
- Pobídněte zasažené k vytvoření nových kontaktů s bezprostředně dostupnými podporujícími osobami. 
- Zlepšete přístup ke komunitním zdrojům nebo dalším zdrojům podpory, které mohou usnadnit zotavení (sociální služby, služby zabývající se závislostí, dětská péče, církevní podpora).
- Podejte pomocnou ruku těm, kteří jsou uzavření do sebe. """;
const String article_3_6_2 = """# Pomáhání těm, co jsou uzavření do sebe
- Pokud mají sebevražedné sklony, okamžitě vyhledejte krizového interventa, případně lékařskou pomoc.
- Pokud se zdráhají vyhledat pomoc, zjistěte proč. Možnými důvody mohou být: jsou zdrcení; neví, co potřebují; cítí se zahanbeně; obávají se, že zatěžují ostatní; pochybují, že podpora je k dispozici nebo mají strach z reakce ostatních, když požádají o pomoc; aktuálně pomoc nechtějí či nepotřebují
- Pomozte jim identifikovat způsob podpory, který by mohl být nejvíce nápomocný.
- Pomozte jim vymyslet plán, jak se k této podpoře dostat, nebo jak se spojit s ostatními. 
- Proberte důležitost sociální podpory ve vypořádávání se s problémem.
- Pamatujte, že někteří lidé navazovat kontakty na daném místě nechtějí. Zaměřte se tedy na kontakty, se kterými se mohou spojit ve svém obvyklém prostředí.""";
const String article_3_6_3 = """- Podpořte zasažené při vyhledání pomoci i při tom, když chtějí pomoci druhým. Svým příkladem můžete předvést pozitivní podporující reakce.""";
const String article_3_6_4 = """Demonstrujte pozitivní a nápomocnou komunikaci. Předveďte podporující reakce pomocí následujících typů vyjádření:
- ## Reflektující
*„To, co mi říkáte, zní jako…“ nebo „Z toho, co říkáte, mi připadá, jako byste byl…“*
- ## Objasňující
*„Mám pravdu, když řeknu…?“ nebo „Řekněte, jestli se mýlím, ale zní to jako byste říkal…“*
- ## Podporující 
*„Žádný div, že se cítíte…“ nebo „Opravdu mě mrzí, že je to pro vás tak těžké období.“*
- ## Posilující
*„Co jste dělal v minulosti, když jste byl ve složité situaci, abyste se cítil lépe?“*""";


const String article_3_7_1 = """
# Co to je?
Poskytování informací o stresových reakcích a zvládání za účelem snížit úzkost a podporovat dobré zvládání.
# Jak postupovat
- Poskytněte základní informace o běžných stresových reakcích na traumatické události. """;

const String article_3_7_2 = """## Běžné stresové reakce jsou:
- ## Vtíravé reakce
Do mysli se vrací traumatický zážitek. To zahrnuje vtíravé myšlenky, představy události nebo sny o tom, co se stalo. Také to mohou být znepokojující emoční nebo fyzické reakce při vzpomínce na traumatický zážitek.
- ## Vyhýbání se
Zasažení se pokouší oprostit nebo se chránit před úzkostí. Mohou se vyhýbat o traumatické události mluvit, myslet na ni nebo potlačují pocity s ní spojené. Vyhýbají se čemukoli, co by událost připomínalo, třeba určitým lidem nebo místům.
- ## Fyzické napětí
Fyzické změny mohou tělo nutit reagovat, jako by nebezpečí bylo stále aktuální, i když už reálně pominulo. Zasažení neustále dávají pozor, snadno se vylekají nebo jsou nervózní, podráždění nebo rozmrzelí, mívají výbuchy vzteku nebo pláče, obtížně spí a těžko se soustředí.
- ## Zármutek
Reakce po smrti milované osoby nebo velké ztrátě (vztahu, role, majetku..). Tyto ztráty mohou vést k pocitům smutku, zlosti, viny, lítosti. Touží, aby se situace vrátila zpět před událost.
- ## Traumatický zármutek
Reakce, které nastanou, když  byli zasažení svědky traumatické smrti milované osoby. Mohou se zaměřovat na okolnosti smrti, přemýšlet, jak se smrti dalo předejít, jaké měly být poslední okamžiky a kdo je na vině. Tyto reakce mohou způsobovat komplikované truchlení.
- ## Deprese
Mezi reakce patří depresivní nebo podrážděná nálada, ztráta chuti, problémy se spánkem, vyčerpání, pocity bezcennosti nebo viny a někdy myšlenky na sebevraždu. Deprese bývá spojená se zármutkem a souvisí s nesnázemi po události.
- ## Fyzické reakce
Tyto reakce zahrnují tělesná strádání, jako jsou bolesti hlavy, závratě, bolesti břicha, bolesti svalů, zrychlený srdeční tep, tíha na hrudi, hyperventilaci a střevní problémy. Zasažení mohou prožívat tyto typy reakcí, i když předtím neměli žádné zásadní fyzické zranění nebo nemoc.""";
const String article_3_7_3 = """- Mluvte se zasaženými o připomínkách traumatu a jak se s nimi lze vypořádat.""";
const String article_3_7_4 = """Připomínky traumatu, ztráty nebo změny, stejně jako trvající tíživá životní situace, mohou vyvolávat znepokojující myšlenky a pocity dlouho po události.
- ## Připomínky traumatu
Situace, která vyvolává znepokojující myšlenky a pocity spojené s událostí. Mohou to být zrakové vjemy, zvuky, místa, vůně či pachy, určití lidé, určitá denní doba, situace, nebo dokonce pocity. Kupříkladu po tornádu to může být zvuk větru, deště, helikoptéry, křik a další podněty, které má člověk spojené s událostí.
- ## Připomínky ztráty
Situace, kvůli kterým může vytanout na mysli nepřítomnost milované osoby nebo ztraceného majetku a přinést silné pocit, jako je smutek, touha nebo nervozita. (fotografie apod.)
- ## Připomínky změny
Situace, které lidem připomínají, jak se jejich život změnil následkem katastrofy. Mohou to být osoby, místa, věci, aktivity, nebo tíživá životní situace. Kupříkladu probuzení se ráno v odlišné posteli, chození do jiné školy, nebo bytí na jiném místě.""";
const String article_3_7_5 = """- Zdůrazněte, že tyto stresové reakce jsou běžné a pochopitelné. Nicméně pokud přetrvávají déle než 4 až 6 týdnů, nebo se zhoršují a oslabují fungování, člověk by měl vyhledat další pomoc.
- Zjistěte, jaké způsoby vypořádávání použili v minulosti a pomozte jim zvážit, zda jsou vhodné pro nynější situaci.
- Hledejte společně další strategie zvládání, jak se s problémem vypořádat.""";
const String article_3_7_6 = """- Mluvte s někým nebo stačí s někým jen být.
- Dělejte něco, co vás baví. 
- Dopřejte si adekvátní odpočinek a jezte zdravá jídla.
- Pokuste se udržovat obvyklý rozvrh.
- Dávejte si pravidelné přestávky.
- Používejte humor.
- Naplánujte si příjemné aktivity.
- Zaměřte se na něco praktického, co můžete dělat právě teď.
- Používejte relaxační metody.
- Účastněte se podpůrných skupin.
- Můžete si psát deník.
- Vyhledejte poradenské služby, pokud to potřebujete.
- Vyhněte se alkoholu, zneužívání látek, vzteku a obviňování, návykovému chování, extrémnímu stranění se a izolaci od ostatních a riskantnímu chování.""";
const String article_3_7_7 = """- Učte jednoduché relaxační techniky""";
const String article_3_7_8 = """Učení jednoduchých relaxačních technik:
- Dechová cvičení mohou pomoci mírnit pocity stresu a napětí.
- Pravidelné cvičení může zlepšit spánek, stravování a psychický stav.
- **Projděte se zasaženými následující kroky:**
    - Pomalu se nadechněte (tisíc jedna, tisíc dva, tisíc tři) skrz váš nos a pohodlně naplňte své plíce a nadechněte se až do břicha.
    - Tiše a jemně řeknete sami sobě, „Mé tělo je naplněno klidem.“ 
    - Pomalu vydechněte (tisíc jedna, tisíc dva, tisíc tři) skrz vaše ústa  všechen vzduch z plic.
    - Tiše a jemně řekněte sami sobě, „Mé tělo se zbavuje napětí.“
    - Tohle pětkrát pomalu a pohodlně zopakujte.
    - Toto cvičení provádějte tolikrát denně, kolikrát je potřeba.""";
const String article_3_7_9 = """- Pomozte rodinám se s událostí vypořádat.""";
const String article_3_7_10 = """Pomoc rodinám při vypořádávání se s problémem:
- Vysvětlete, že každý člen rodiny bude mít rozdílné reakce a cestu k zotavení. Podpořte je, aby byli trpěliví, chápající a respektující k těmto rozdílům.
- Udržujte zaběhlé zvyklosti v rodině  (čas jídla, čas jít spát, čas probouzení, čas čtení a čas na hraní).
- Upevněte roli rodičů a zodpovězte otázky, které mají ohledně jejich dětí.
- Podpořte členy rodiny v aktivitách, které jim mohou  pomoci lépe pochopit druhého a tím ho i podpořit.""";
const String article_3_7_11 = """- Pracujte se hněvem.""";
const String article_3_7_12 = """Řešení problémů s hněvem:
- Zeptejte se, co člověka trápí.
- Vysvětlete, že rozumíte, proč jsou naštvaní, a že pocity hněvu a frustrace jsou po katastrofě běžné.
- Požádejte zasažené, aby určili cíle, kterých chtějí dosáhnout.
- Pomozte jim porozumět, že zadržování vzteku je může zranit a že jim zvládnutí vzteku může pomoci dosáhnout jejich cílů. 
- Některé způsoby ovládnutí vzteku zahrnují:
    - Vezměte si „time-out“ nebo „vychladněte“ (odejděte a zklidněte se)
    - Promluvte si s kamarádem/kamarádkou o tom, co vás rozčiluje.
    - Upusťte páru fyzickým cvičením.
    - Pište si deník.
    - Připomeňte si, že nepřiměřený vztek vám nepomůže dosáhnout vašich cílů a že může poškodit důležité vztahy.
    - Rozptylte se pozitivními aktivitami.
    - Zkuste se podívat na svou situaci v jiném světle.
    - Rodičům/opatrovatelům: mějte po ruce nějakou dospělou osobu na dočasný dohled nad aktivitami vašich dětí, zejména když se cítíte naštvaně nebo podrážděně.
    - Navrhněte dětem nebo dospívajícím, aby se zapojili do aktivit, které jim mohou pomoci vyjádřit jejich pocity, jako je kreslení obrázků, psaní do deníku, sehrávání situací s hračkami a skládání písniček.
    - Pomozte dětem a dospívajícím přehrát si situaci, která je štve nebo deptá.
- Pokud se agresivní projevy stanou nekontrolovatelnými nebo jsou násilné, vyhledejte odbornou pomoc, případně kontaktujte policii.
""";

const String article_3_7_13 = """- Věnujte se vysoce negativním emocím.""";
const String article_3_7_14 = """Práce s negativními emocemi (sebeobviňování, pocit viny)
- Postupujte následovně:
    - Pomozte jim vidět situaci z jiného úhlu pohledu.
    - Jaké jsou další způsoby, jak o situaci uvažovat?
    - Jak by pravděpodobně reagovali, kdyby jejich dobrý/á kamarád/kamarádka o sobě takto mluvili? Co by mu/jí řekli? Můžou říct to samé sami sobě?
- Pomozte objasnit nedorozumění, fámy a zkreslení, které zvyšují tíseň, neodůvodněný pocit viny nebo studu.
- Děti a dospívající mají tendenci obviňovat sami sebe. Připomeňte jim, že nejsou na vině, a to i v případě, že o tom nemluví.
- I když cítí pocit viny, nemusí to znamenat, že situaci zavinili.
- Při události, kdy je osoba nějakým způsobem zodpovědná a vyrovnává se s pocitem viny, by měla být odkázána na odborné poradenství. Tyto situace už jdou mimo rámec krátkodobých zásahů, jako je PPP.""";
const String article_3_7_15 = """- Pomozte řešit problémy se spánkem.""";
const String article_3_7_16 = """Podporujte následující zdravé návyky.
- Choďte spát a vstávejte každý den ve stejný čas.
- Omezte požívání alkoholu, alkohol narušuje spánek.
- Odpoledne a večer omezte požívání kofeinových nápojů.
- Zvyšte pravidelné cvičení, ale ne těsně před spaním.
- Odpočiňte si před tím, než jdete spát, dělejte něčeho uklidňujícího.
- Omezte zdřímnutí přes den na 15 minut a omezte zdřímnutí po 16. hodině.
- Poraďte, že sdílet obavy a dostat od ostatních podporu může spánek v průběhu času zlepšit.
- Připomeňte rodičům, že je běžné, když děti chtějí v noci zůstat rodičům nablízku a dokonce s nimi chtějí spát v posteli. Dočasné změny ve spacích návycích jsou v pořádku, pokud se rodiče s dětmi dohodnou, že se po čase navrátí k běžným spacím návykům.""";
const String article_3_7_17 = """- Řešte užívání alkoholu a návykových látek.""";
const String article_3_7_18 = """Řešení užívání alkoholu a návykových látek:
- Proberte s nimi, jaká jsou pozitiva a negativa v užívání alkoholu a drog pro vyrovnání se s danou situací.
- Snažte se je motivovat, aby tyto strategie opustili. Navrhněte jiné možnosti.""";

const String article_4_1 = """
Zrakové postižení má mnoho podob. Může jít o lidi nevidomé od narození, osleplé později, lidi s poruchami zraku následkem nehody, lidi, kteří při nehodě ztratili brýle, slabozraké, lidi se zbytky zraku různého charakteru nebo o velmi staré lidé s fyziologickou ztrátou zraku. Charakter potřebné pomoci se u těchto lidí může odlišovat, stejně jako jejich schopnosti vnímání, orientace a sebeobsluhy. Psychický rozvoj lidí se zrakovým postižením je totožný s rozvojem u vidících lidí (dochází ke shodnému vývoji myšlení, emocí i sociálních schopností).
# Zásady pro krizovou asistenci a komunikaci s nevidomými
- Postupujeme podle zásad první psychické pomoci.
- Dbáme na zachování jeho lidské důstojnosti a úcty. 
- Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám. 
- Podporujeme vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence. 
- Zeptáme se těchto lidí, zda a jak vidí. Přizpůsobíme se jejich schopnostem.
- Při evakuaci ponecháváme potřebné kompenzační pomůcky (mobily, čtecí lupy, notebooky, diktafon apod.)
- Neoddělujeme nevidomého od vodícího psa. Vodícího psa evakuujeme společně s nevidomým. Na vodícího psa nevoláme, nehladíme ho, nekrmíme, nerozptylujeme.
- S věcmi nevidomého nebo slabozrakého nemanipulujeme bez jeho vědomí (má prostorovou představu, kam co dal). Při změnách přemístění věcí vždy jedince informujeme.
- Nelitujeme, respektujeme odlišnosti ve vnímání a komunikaci.
- Při komunikaci využíváme všechny způsoby vnímání (sluch, hmat, zbytky zraku).
- Při oslovení nevidomého se lehce dotkneme jeho paže (upoutáme pozornost). Oslovujeme jej jménem. Důležité je se představit a říci svou roli a úkoly. Podání ruky nahrazuje vizuální kontakt, při představení řekneme „Podávám vám ruku“. 
- Mluvíme přímo na nevidomého, nikoliv na jeho průvodce. (Nevidí, ale rozhoduje za sebe sám.) Pokud je to možné, udržujte oční kontakt s osobou s poruchou zraku, pokud mluvíme bokem, pozná to.
- Při komunikaci bez obav používáme obraty jako: „vidět, kouknout se, mrknout, prohlédnout si“. 
- Zjistíme, zda má kompenzační pomůcky. 
- Při vedení nevidomého do neznámých prostor slovně popisujeme okolí, sanitku, místnost pro ukrytí a její vybavení. Ukážeme, kde se nachází toaleta, splachovadlo, papír, umyvadlo. Ujistíme se, že je toaleta čistá. 
- Aktivně informujeme o situaci. Popisujeme viděné aspekty. Vysvětlujeme postupy.
- Ptáme se, zda je osoba připravena na úkon (např. aplikace injekce, ošetření).
- Nezbytné jsou informace o tom, co bude (např. „Teď to bude tlačit, teď to bude pálit“).
- Pozor na emotivní zvolání bez vysvětlení (např. „ježišmarja“), mohou zmást a zvyšovat úzkost.
- Nepoužíváme výrazy „támhle“, „tady“, ale spíše „hned za vámi“, „dva metry před vámi“ apod.
- Neznámé pomůcky a vybavení necháme nevidomého osahat (vidí hmatem), vše vysvětlíme a popíšeme. 
- Veškeré činnosti a úkony slovně popisujeme (např.: „Tady je dýchací maska, sáhněte na ni, teď vám ji nasadím na hlavu…Kdyby vás to tlačilo, řekněte…“). 
- V dýchací technice nemusí být záchranáře slyšet.  Aby nás nevidomý rozpoznal, přiložíme jeho ruce na helmu apod. Pomocí rukou naznačujeme úmysl, např. nasadit vyváděcí masku.
- V neznámém prostoru přesně popisujeme (únikovou) cestu a případná nebezpečí. Kde není volný průchod pozor na překážky a předměty na zemi, na předměty ve výšce hlavy (lešení, trosky, dopisní schránka na zdi domu, změny v povrchu chodníku apod.). Např. „Tři kroky dopředu, teď sehněte hlavu, je tam tyč. Teď velký krok, teď jdu před vás… Teď úkrok do leva před námi leží prkna, a už můžeme jít rovně“. 
- Při doprovázení se domluvíme na způsobu. Při vedení např. nabídneme paži a necháme jej se zavěsit – zpravidla se lehce dotýká našeho lokte či spodní hrany předloktí, to jej vede. Průvodce je zpravidla vpředu.  Nevidomého nikdy netlačíme před sebou ani netáhneme za ruku za sebou. U vozidla jej stavíme k otevřeným dveřím nebo dáváme ruku na kliku. Při nástupu do vozidla upozorníme na výšku podlahy a položíme jeho ruku na horní hranu dveří.  
- Při procházení úzkým průchodem jde průvodce jako první.
- Při usazení nevidomého položíme ruku nevidomého na opěradlo, necháme ho se samostatně posadit. Nestrkáme ho, nenecháme ho tápat v prostoru.
- Na podávaný předmět položíme jeho ruku, popisujeme, kde a co leží.
- Při chůzi po schodech dáváme ruku nevidomého na zábradlí, druhou rukou se do nás může zavěsit. Informujeme o směru chůze nahoru/dolů. Upozorníme na první a poslední schod. Upozorníme na obrubník chodníku či překážky. Upozorníme na věci na stole, které by mohl shodit.
- Nevidomého neopouštíme bez upozornění, pokud se potřebujeme vzdálit, zanecháme ho u pevné opory a informujeme ho, na jak dlouho odcházíme (např. „Potřebuji na chvíli odejít, tady je zábradlí, počkejte tu na mně, za 5 minut se vrátím“). 
- Nikdy nechodíme za nevidomým jako „hlídač“, bez komunikace či nabídky pomoci.""";
const String article_4_2 = """
Sluchové postižení má mnoho podob. Může jít o lidi neslyšící od narození, ohluchlé až po rozvoji řeči, osoby se zbytky sluchu, ohluchlé ve stáří, lidi se ztrátou sluchu dočasnou po úraze, lidi, kteří ztratili při nehodě naslouchadla apod. Pouze někteří sluchově postižení ovládají znakový jazyk, někteří umí odezírat, někteří používají kompenzační pomůcky (naslouchadla, kochleární implantát, přepis řeči). Někteří ohluchlí mohou žít po dlouhou dobu v izolaci. 
Charakter potřebné pomoci se u těchto lidí může odlišovat, stejně jako jejich schopnosti vnímání, orientace a sebeobsluhy. Psychický rozvoj lidí ohluchlých před rozvojem řeči se může lišit od průměrné populace (méně je rozvinuta abstraktní složka myšlení, odlišný je smysl pro humor, odlišnosti mohou být v chápání sociálních interakcí).  
Velmi specifická situace je u lidí hluchoslepých nebo u lidí s částečnou ztrátou jak zraku, tak sluchu.
# Zásady pro krizovou asistenci a komunikaci se sluchovým postižením
- Postupujeme podle zásad první psychické pomoci.
- Ke stabilizaci osob pomáhá dodávání informací (včasných, přiměřených a pravdivých) a komunikace s blízkými. Zprostředkujeme kontakt s blízkou osobou a případně s tlumočníkem znakového jazyka.
- Při komunikaci s člověkem se sluchovým postižením vždy dbáme na zachování jeho lidské důstojnosti a úcty. 
- Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám. 
- Usilujeme o vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence. 
- Zeptáme se, zda a jak slyší. Přizpůsobíme se jejich schopnostem a komunikačním možnostem (tlumočník, zajištění přepisu slov do textu, bloky a tužky, kreslení obrázků).
- Při varování používáme jiné komunikační cesty než je jen zvuk.
- Při evakuaci ponecháváme potřebné kompenzační pomůcky (naslouchadla, kochleární implantát, mobily, notebooky apod.).
- Nelitujeme, respektujeme odlišnosti ve vnímání a komunikaci.
- Při komunikaci využíváme všechny způsoby vnímání (zrak, hmat, zbytky sluchu).
- Při oslovení neslyšícího se lehce dotkneme jeho paže (nebo jinak upoutáme pozornost).
- Na neslyšícího mluvíme tváří v tvář, na nedoslýchavého z té strany, kde má zachovány zbytky sluchu. 
- Používáme krátké věty. Zřetelně artikulujeme, nestoupáme si  proti světlu. Ne všichni neslyšící umí dobře odezírat. Nekřičíme. Ověříme si, zda nám osoba správně porozuměla.
- Používáme všechny způsoby komunikace (gesta, posunky, psaní na papír, předtištěné obrázky, piktogramy, předtištěné věty, textové zprávy, notebook, překladač v mobilu, znaky na kartičkách, některá slova znakového jazyka) např. „Jsem zdravotník. Jste zraněn? Bolí vás něco? Ukažte na obrázku, kde vás bolí…“ Pokud není k dispozici tlumočník, hlavní je tužka a papír. Postup a jiné informace píšeme v jednoduchých větách na papír.
- I když neumíme znakový jazyk, můžeme gesta použít intuitivně (pantomima). Demonstrujeme např. nasazení pomůcek na sobě. Naznačíme následný zdravotnický úkon.
- Pokud to lze, zajistíme tlumočníka (lze tlumočit např. i přes Skype, lze použít on-li službu www.appn.cz, www.tkcn.cz )
- Mluvíme vždy přímo na neslyšícího, nikoliv na jeho tlumočníka. (Neslyší, ale rozhoduje za sebe sám.) Tlumočník stojí vedle nás čelem k neslyšícímu.
- Aktivně informujeme o situaci. Vysvětlujeme postupy. Veškeré činnosti a úkony neverbálně naznačujeme nebo píšeme.
- Neptáme se, zda porozuměl, ale raději „Co jste mi rozuměl?“.
- Pozor neexistuje univerzální znakový jazyk, každý národ má vlastní.
- Velkou úzkost může způsobit u neslyšícího nemožnost použít ke komunikaci ruce (zranění, spoutání).
- Pozor na nebezpečné situace, kdy neslyšící nemá zvukové informace - například o vozidlech či strojích mimo zorný úhel. """;
const String article_4_3 = """

Psychické potíže mají mnoho podob. Může jít o lidi s vrozeným postižením intelektu různé hloubky, osoby se stařeckou demencí, osoby trpící posttraumatickou stresovou poruchou, hlubokou depresí, fobiemi, intoxikované osoby, osoby s potížemi autistického spektra či trpící psychózou apod. Rozumové schopnosti,  sociální schopnosti a emoční projevy mohou být u jednotlivců velmi individuální i proměnlivé. Mimořádná událost může u některých lidí vést k dekompenzaci psychického stavu, jiní dovedou jednat velmi racionálně a organizovaně. 
U některých lidí dochází ke kombinacím psychického a fyzického či smyslového postižení.
Charakter potřebné pomoci se u těchto lidí může odlišovat, stejně jako jejich schopnosti myšlení, vnímání, orientace a sebeobsluhy.  Mnohdy pomůže větší trpělivost, vnímavost a ohleduplný přístup.  
# Zásady pro krizovou asistenci a komunikaci s lidmi s psychickými potížemi
- Postupujeme podle zásad první psychické pomoci.
- Při komunikaci s člověkem s psychickými potížemi vždy dbáme na zachování jeho lidské důstojnosti a úcty. 
- Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám. 
- Usilujeme o vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence. 
- Přizpůsobíme se jejich schopnostem a komunikačním možnostem.
- Nelitujeme, respektujeme odlišnosti ve vnímání, myšlení a komunikaci.
- Bereme na vědomí, že se může jednat o lidi s postižením intelektu v různé intenzitě, jsou individualitami. Pomaleji chápou. Na změny potřebují více času. Někteří nemluví nebo mají další zdravotní problémy. Někteří mohou dobře rozumět, jiní mohou být mentálně na úrovni velmi malého dítěte.
- Zásah do běžných stereotypů je může rozrušit, proto jednejme klidně, citlivě a trpělivě. Neznámé situace a náhlé změny prožívají většinou velmi citlivě.
- Podobně jako u dětí uniformy a pomůcky mohou vystrašit.
- Potřebují klidné a neměnné prostředí (ať už v rodině nebo pobytovém zařízení). Vlivem stresu se mohou i ve známém prostředí pohybovat zmateně. Proto je vždy doprovázíme nebo i vedeme za ruku.
- Pro zvýšení pocitu bezpečí vedeme osoby s mentálním postižením pokud možno pohromadě. Neoddělujeme je od známých osob.
- Při mimořádných situacích mohou být vystrašení a zmatení. 
- Mluvme na ně pomalu a jednoduše. Ty co nemluví, chytněme za ruku.
- K uklidnění může přispět vzít si sebou některé své osobní předměty nebo hračku.
- Mentálně postižení lidé nemusí vždy rozumět našim informacím a pokynům, pak se mohou chovat neočekávaným způsobem. Vždy si ověříme, zda nám porozuměli a pochopili, co mají dělat.
- Mentální postižení může být kombinováno s postižením tělesným a smyslovým. Je třeba i toto zohledňovat.
- Pokud je musíme evakuovat do neznámých prostor, ujišťujeme je zvýšeně, že budou v bezpečí a že na novém místě budou mít vše, co potřebují. Nápodoba hraje roli.
- Pomáhá fyzická opora, dotyk, jednoduché vysvětlování, vlídný přístup, pozitivní emoce.
- Mluvme v krátkých jednoduchých větách a konkrétně, ale není vždy vhodné mluvit s těmito lidmi jako s malými dětmi. 
- Nepoužívejme odborné termíny a abstraktní pojmy. Tito lidé většinou nerozumí ironii, nadsázce a dvojsmyslům.
- I když se někteří nedovedou vyjadřovat slovy, mluvené řeči alespoň částečně rozumí. Dobře reagují na neverbální komunikaci.
- Využívejme přívětivé a povzbudivé mimiky, znázorňujících gest a pohybů.
- Zvýšeně dodáváme pocit bezpečí, nenecháváme je samotné, udržujeme s nimi oční kontakt.
""";
const String article_4_4 = """
Pohybová postižení mají mnoho podob. Může jít o lidi s vrozeným postižením motoriky končetin a trupu, osoby s postižením pohybu po úrazu, lidi s omezenou hybností danou věkem, lidi s různou úrovní sebeobsluhy a různými kompenzačními prostředky (protézy, berle, chodítka, invalidní vozík). U některých se může objevovat současné postižení mluvidel a mimických svalů. Jen u některých lidí dochází ke kombinacím psychického a fyzického či smyslového postižení.
Charakter potřebné pomoci se u těchto lidí odlišuje, stejně jako jejich schopnosti sebeobsluhy.  Mnohdy pomůže zeptat se daného jedince, jak napomoci při transportu a jaké jsou jeho specifické potřeby.  
# Zásady pro krizovou asistenci a komunikaci s lidmi s pohybovým postižením
- Postupujeme podle zásad první psychické pomoci.
- Při komunikaci s člověkem s pohybovým postižením vždy dbáme na zachování jeho lidské důstojnosti a úcty. 
- Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám. 
- Usilujeme o vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence. 
- Přizpůsobíme se jeho schopnostem.
- Nelitujeme, respektujeme odlišnosti v pohybu. Často se se svým postižením již naučili žít.
- Přistupujeme k jednotlivci jako k jakékoli jiné osobě.
- U některých tělesných postižení je zasažena i motorika mluvidel a obličeje. Tělesně postižený se může jevit jako postižený mentálně, i když není. Ověříme si stav postižení a vždy zachováváme jeho lidskou důstojnost.
- Ptáme se, co jedinec dokáže zvládnout sám a s čím můžeme pomoci.
- Necháme si od něj poradit, jak s ním zacházet.
- Pozor na zvýšenou lámavost kostí nebo svalovou ochablost – při nevhodném zacházení je riziko zranění.
- Někteří neudrží tělo bez opory.
- Při pomoci s vozíkem postupujte podle rad vozíčkáře.
- Pokud nelze evakuovat po schodech, může být transport vozíčkáře prováděn na nosítkách. Některé osoby lze evakuovat vsedě, jiné jen vleže.
- Popisujeme situaci a vysvětlujeme kroky transportu.
- S osobou evakuujeme i její vozík či jiné kompenzační pomůcky.
- Pokud pomůcky chybí, můžeme využít improvizované transportní prostředky.
- Manipulace s vozíkem – složení/rozložení: zajistíme brzdy, zvedneme podnožku, tahem za látku sedadla vozík složíme. Tlakem na sedadlo rozložíme. Pozor na zranění prstů při manipulaci. 
- Při transportu ze schodů: jedna osoba uchopí vozík za zadní madla a nakloní jej dozadu na velká kola, druhá osoba přidržuje vozík dole za pevnou část konstrukce u předních koleček. Lehkou osobu můžeme snést i s vozíkem.
- Podobně se postupuje při cestě do schodů (vozíčkář je vyvážen pozadu). 
- Pokud má vozíčkář dostatečnou sílu v rukou, může mu na schodišti pomáhat jen jedna osoba. Před sjížděním/vyjížděním schodiště je nutné naklonit vozík na zadní kola, aby osoba nevypadla.
- Na nerovném, kamenitém či písčitém terénu vezeme vozík s osobou pozpátku. (Při tlačení dopředu se kola boří).
- Chceme-li osobu vysadit z vozíku, musíme se zeptat, jakým způsobem ji můžeme uchopit.
- Před usazením na vozík jej zabrzdíme a odkloníme postranní opěrku.
- Osobu používající berle nezvedáme ze země za ruce, ale uchopíme zezadu v pase.
- Při přenášení tělesně postiženého podpíráme jeho páteř, ne každý má dostatečnou sílu v rukou, aby se nás mohl přidržovat. Stále hlídáme polohu ochrnutých a nehybných končetin. (Nemusí sám cítit zavadění či zranění).
- Elektrické vozíky je lépe přenášet bez vozíčkáře (váží 100 kg a víc). Motory lze vzadu odpojit a vozík lze odtlačit mechanicky.
- Při přesunu z elektrického na mechanický vozík se odmontuje bočnice.
- Je třeba ověřit potřebnou šíři dveří WC.""";
const String article_4_5 = """
Při práci s cizinci při mimořádných událostech je klíčovou kompetencí jazyková komunikace. Dále hrají roli také kulturní odlišnosti. Ty se týkají komunikace, stravování, odívání, ubytování, náboženských rituálů, nakládání se zemřelými, pohřebních potřeb a zvyklostí apod. Specifické potřeby mají také zástupci některých náboženských menšin.
Pokud nemáme potřebné interkulturní znalosti, obrátíme se na zástupce daného zastupitelského úřadu či tlumočníka, nebo se i přímo ptáme zástupců zasažených osob, co je v jejich zemi důležité, co potřebují, na co nemáme zapomenout apod.""";

const String article_5_1 = """
- nevšímat si zasažených, chovat se, jako by neexistovali, 
- spěchat, být netrpělivý, vyvíjet nátlak, 
- v rozhovoru vyslýchat, kritizovat, soudit, kázat, 
- lhát, překrucovat informace
- vnucovat jim vlastní rady, zkušenosti,
- zlehčovat jejich ztrátu, prožitou událost,
- dávat planou útěchu a naději,
- slibovat, co nemůžeme splnit,
- neustále mluvit, zaplňovat ticho,
- dělat za zasažené to, co by mohli dělat sami,
- pomáhat, když se na to necítíme
- užívat alkohol či drogy.
""";

const String article_6_1 = """
# Reakce v chování dětí
- lepí se na rodiče nebo jiné známé dospělé lidi
- bezmoc a pasivita  
- znovu se počůrává nebo si znovu cucá palec 
- bojí se tmy
- nechce spát samo
- zvýšeně pláče 
# Tělesné projevy u dětí
- ztráta chuti k jídlu 
- bolesti bříška 
- nevolnost
- problémy se spaním, noční můry 
- potíže v řeči
- tiky
# Reakce v prožívání dětí
- úzkost 
- strach téměř ze všeho
- podrážděnost 
- výbuchy zlosti
- smutek 
- staženost, ponoření se do sebe 
# Možnost postupu
- slovně ujišťujte a tělesně utišujte, pochovejte 
- opakovaně objasňujte neporozumění a mylné představy
- při ukládání do postýlky se mazlete
- pomáhejte pojmenovávat emoce
- vyhněte se zbytečným odloučením 
- dočasně dovolte dítěti, aby spalo v ložnici rodičů
- připomínky události zbavujte tajemna a dohadů
- povzbuzujte projevy týkající se ztrát (např. smrti včetně smrti domácích zvířat, ztrát hraček)
- hlídejte, nakolik je dítě vystaveno působení médií
- povzbuzujte vyjádření prostřednictvím hry """;

const String article_6_2 = """
# Reakce v chování dětí
- zhoršil/a  se v učení 
- chodí za školu
- doma nebo ve škole druhé napadá    
- chová se přespříliš aktivně, nebo otupěle - hloupě 
- kňourá, lepí se, chová se jako menší dítě
- častěji soupeří s mladšími sourozenci o pozornost rodičů   
- přehrávání traumatu, hra na traumatické téma
# Tělesné projevy u dětí
- změny chuti k jídlu 
- bolesti hlavy
- bolesti břicha 
- poruchy spánku, noční můry  
- tělesné stesky
# Reakce v prožívání dětí
- strach z pocitů 
- odvracení se od přátel a obvyklých činností  
- připomínky události vyvolávají strach a obavy
- výbuchy zlosti
- zaujetí zločinem a zločinci, bezpečím a smrtí 
- výčitky svědomí
- vina 
# Možnosti postupu
- věnujte dítěti více pozornosti a zájmu 
- dočasně zmírněte požadavky na výkon ve škole i doma
- vymezte citlivě ale pevně hranice pro předvádění se a podobné chování
- zaměstnávejte dítě činnostmi dobrými pro rehabilitaci a obnovu a - - strukturujte mu čas nenáročnými domácími pracemi   
- povzbuzujte vyjadřování pocitů a myšlenek slovem i hrou
- naslouchejte, když bude dítě opakovaně vyprávět o traumatizující události 
- objasňujte dítěti jeho mylné představy a zkreslení
- rozpoznejte připomínky události a pomáhejte mu s nimi 
- vystavte školní program zaměřený na vrstevnickou oporu, na činnosti, při nichž je možné se projevit a vyjádřit, na osvětu o traumatu a zločinu, na krizovou připravenost a plánování, na vytipování ohrožených dětí 
""";

const String article_6_3 = """
# Reakce v chování dospívajících
- zhoršil/a se v učení   
- doma nebo ve škole se chová vzdorovitě, revoltuje 
- znovu se chová nezodpovědně, ač už tomu bylo jinak 
- chová se neklidně nebo apaticky, projevuje úbytek energie
- dostává se do rozporu se zákonem  
- hazarduje 
- společensky se stahuje, uzavírá do sebe 
- prudce mění vztahy a prudce se mění ve vztazích     
- pije alkohol nebo užívá nezákonné drogy
# Tělesné projevy u dospívajících
- změny chuti k jídlu 
- bolesti hlavy
- problémy v oblasti trávicí soustavy  
- vyrážky  
- stesky na nejasné bolesti a pobolívání 
- poruchy spánku
# Reakce a prižívání dospívajících
- ztráta zájmu o vrstevníky, koníčky a zájmové činnosti 
- smutek nebo deprese
- úzkost a obavy o bezpečí 
- odpor vůči autoritě
- pocity neschopnosti a bezmoci
- vina, výčitky svědomí, stud a plachost   
- touha po odplatě
# Možnosti postupu
- věnujte dospívající/mu více pozornosti a zájmu 
- dočasně zmírněte požadavky na výkon ve škole i doma
- povzbuzujte debaty o prožitcích traumatu s vrstevníky a důležitými dospělými
- netrvejte na tom, aby se o svých pocitech bavil/a s rodiči 
- vypořádejte se s tendencemi k lehkomyslnosti a bezohlednosti, pojmenujte je
- propojujte chování a prožívání s událostí  
- podporujte cvičení a další tělesné činnosti 
- povzbuzujte návrat ke společenským činnostem, k atletice, klubům atp. 
-  účast na činnostech v obci a na školních akcích 
- vytvářejte školní programy zaměřené na vrstevnickou oporu a vytipování ohrožených mladistvých, podpůrné skupiny pro ohrožené studenty, telefonické kontaktní linky v tísni, nízkoprahová kontaktní střediska  
""";

const String article_6_4 = """
# Reakce v chování dospělých 
- špatně spí 
- vyhýbá se připomínkám události
- je přespříliš aktivní
- ochraňuje blízké    
- snadno se rozpláče
- zlostně vybuchuje 
- častěji než dříve se dostává do konfliktů s rodinou 
- reaguje se zvýšenou ostražitostí a bdělostí 
- izoluje se, stahuje, mlčí, vypíná  
- častěji než dříve pije alkohol nebo užívá nezákonné drogy
# Tělesné projevy u dospělých
- nevolnost  
- bolesti hlavy
- únava, vyčerpání 
- nepohoda v oblasti trávicí soustavy  
- změny chuti k jídlu 
- tělesné stesky
- zhoršení chronických stavů 
# Reakce v prožívání dospělých
- šok, dezorientace, otupělost
- deprese, smutek
- žal 
- podrážděnost, zlost
- úzkost, strach
- zoufalství, bezmocnost
- vina, pochybnosti o sobě
- výkyvy nálad
# Možnosti postupu
- ochraňujte, nasměrujte a propojujte 
- zajistěte přístup k neodkladné lékařské péči 
- podporujte nasloucháním a poskytněte příležitost mluvit o prožitcích a ztrátách
- poskytujte časté a aktualizované zprávy o záchranných pracích, o obnově a zotavování 
-  dodejte kontakty atp. pro odpovědi na otázky    
- pomáhejte s určováním, co má přednost, a s řešením problémů 
- pomáhejte rodině usnadňovat komunikaci a účelně fungovat 
- poskytujte rady rodinám a informace o traumatické zátěži a o vyrovnávání se s ní, o reakcích dětí  
- poskytujte informace o policejním vyšetřování a trestním řízení, o roli zdravotnických záchranářů atp. 
- poskytujte služby pro oběti trestných činů 
- posuzujte stav a tam, kde je třeba, doporučte navazující pomoc  
- poskytujte informace o možných navazujících službách a dalších zdrojích
""";

const String article_6_5 = """
# Reakce v chování seniorů   
- stahuje se a izoluje 
- neochotně opouští domov
- objevují se problémy s pohybem   
- problematicky se přizpůsobuje, je-li přestěhován/a  
# Tělesné projevy u seniorů
- zhoršení chronických nemocí  
- poruchy spánku
- problémy s pamětí  
- tělesné příznaky 
- zvýšená citlivost a náchylnost k přehřátí a podchlazení 
- tělesná a smyslová omezení (zrak, sluch) ztěžují zotavování   
# Reakce v prožívání seniorů
- deprese
- zoufání nad ztrátami
- apatie 
- zmatenost, dezorientace
- podezíravost
- neklid, zlost
- obavy z umístění do ústavu  
- úzkost z neznámého prostředí 
- rozpaky z přijímání podpor a výpomocí  
# Možnosti postupu
- v rozhovoru důrazně a vytrvale ujišťujte 
- pro orientaci poskytujte informace
- postarejte se, aby bylo pamatováno na tělesné potřeby (voda, jídlo, teplo) 
- používejte vícero posuzovacích metod, neboť problémy bývají podhodnoceny
- pomáhejte při obnově kontaktů s rodinou a dalšími opěrnými systémy
- pomozte získat zdravotní a finanční pomoc 
- povzbuzujte rozhovor o traumatické zkušenosti, o ztrátách a podporujte vyjádření pocitů 
- poskytujte pomoc určenou obětem trestných činů
""";
const String article_7_1 = """
Zátěžová situace je často pro jedince:
- nečitelná,
- neřešitelná,
- nezvádnutelná, protože chybějí prostředky k řešení,
- ohrožující,

proto psychika aktivuje obranné mechanismy.
Obranný mechanismus je schopnost mysli se různými způsoby nevědomě bránit proti uvědomění nepříjemných emocí, fantazií, traumat, impulzů. Překrucuje realitu nebo ji popírá.
Při zátěži člověk může použít jeden nebo více z asi 40 popsaných obranných mechanismů, přičemž neví, že ho používá, neuvědomuje si ho. Krátkodobě plní pro jedince i skupinu významnou funkci, také proto je z použití neobviňujeme, nekritizujeme, nehodnotíme, nesnažíme se jich za každou cenu zbavit. Přistupujeme citlivě a laskavě, přitom pevně. Obranné mechanismy se vyskytují jak na straně zasažených, tak i pomáhajících. V zátěžové situaci nám pomáhají přežít, pokud přetrvávají dlouho, snižují kvalitu života. 

# Obranné mechanismy:

## Disociace 
Mnoho druhů projevů od poruch paměti a vnímání až k necítění částí těla, ztuhnutí nebo křečím, psychika se rozštěpí na části, které za normálních okolností jsou propojené.
### Co dělat?
Bdělá přítomnost, maximální trpělivost, klid, jemný přístup, podpora těla a probíhajícího procesu.
### Co nedělat?
Nezakazujeme projevy disociace, neoznačujeme zasažené jako nespolupracující.

## Derealizace
Zasažený prožívá minimum emocí, připadá si jako vytržený ze života, ze současného dění, pocit „jako ve filmu“
### Co dělat?
Popisujeme, co se děje a co pravděpodobně by zasažený mohl prožívat, pocit neskutečna označujeme jako normální v té situaci.

## Vyhýbání se
Odvádění řeči jinam, nápadné nehledění na zdroj ohrožení, odcházení jinam
### Co dělat?
Ujišťujeme, že je běžné v takové situaci snažit se tomu vyhnout, respektujeme.

## Depersonalizace
Vlastní já, vlastní tělo se může zdát být cizí, neskutečné, ne vlastní
### Co dělat?
Normalizujeme pocity.

## Popření
Jednoduše popírá, co je zjevné: „Ne, to není pravda. Spletli jste se. Jedná se o někoho jiného.“
### Co dělat?
Mluvíme o realitě jak je, a nepřekrucujeme ji: „Ano, opravdu zemřel. Je mi to líto, nedá se dělat víc. Udělali jsme vše, co bylo možné. V této chvíli je po smrti.“
### Co nedělat?
Nepřejímáme obranné mechanismy zasažených, nepoužíváme slova jako zesnul, odešel apod., jsou výrazem popření a  nenabízíme falešné naděje.

## Racionalizace
Rozumově vysvětluje něco nepřijatelného: *„Už ho nic nebolí. Je to tak lepší. Jen by se trápil. Co je to vlastně za život v tomto krutém světě? Teď je mu lépe.“*
### Co dělat?
Uznáme, že ho současná ztráta, bolest či trápení může bolet víc, než si kdokoliv může vůbec představit.
### Co nedělat?
Nepotvrzujeme ani nevyvracíme, nehádáme se se zasaženými.

## Obrana ve vnímání
Některé věci prostě zasažený přehlíží, neslyší, necítí. Tím se chrání před pro něj v tuto chvíli nebezpečnými informacemi.
### Co dělat?
Jsme trpěliví, vydržíme to s ním. Některé informace musíme opakovat několikrát.
### Co nedělat?
Nevyčítáme, kolikrát mu to ještě musíme říkat, nebereme to osobně jako útok či nespolupráci.

## Regrese
Zasažený se začne chovat jako mladší, případně jako dítě. Například staří lidé a děti mohou usnout, starší děti si začnou cucat paleček, počůrají se, kňourají, šišlají. Chtějí být opečováváni jako batolata. Zbavují se odpovědnosti.
### Co dělat?
Jsme maximálně laskaví, vyjadřujeme pochopení, projevujeme klid, slovy, hlasem i beze slov. Současně jim vracíme jejich kompetence a zodpovědnost.
### Co nedělat?
Nezesměšňujeme zasažené nebo nezakazujeme projevy regrese, netlačíme do zralejšího chování. Nerozhodujeme za ně, neděláme za ně, na co stačí.

## Agrese
Zasažený vykazuje tendence ublížit sobě nebo druhým, rozbít, ničit, nadávat, slovně napadat, pomlouvat.
### Co dělat?
Pojmenujeme, co vše ho může hněvat, normalizujeme hněv jako reakci na zátěž, oddělujeme od násilného chování, snažíme se zabránit zranění či ničení, chráníme se. 
Nabízíme sociálně přijatelné způsoby vybití""";

const String article_7_2 = """
- cviky proti zdi (limit), u lidí s nahromaděnou energií, neklid
    - tlačení nohama v leže na zádech proti zdi (doplnit obrázek)
    - tlačení rukama proti zdi (doplnit obrázek)
    - tlačení do futer od dveří (doplnit obrázek)
    - tlačení dlaněmi proti sobě, s nádechem tlačíme, zadržíme dech a zvýšíme úsilí na maximum možného a s výdechem povolíme

- Všechny cviky komentujeme
    - *„Vidím že máte strašně moc energie. Vím o něčem co pomáhá lidem co se cítí takhle. Chcete to zkusit?“*
    - *„Kolik té energie je bezpečné, abyste vyjádřil ven?“*
    - *„10%.....30%...“*
    - *„když vám to půjde, přidejte zvuk…“*

- Doporučení
    - Vždy dělat cviky s ním. *„to je v pořádku že…“ „je to rychlé a bezpečné“*
    - ženy: 
        - opřít se zádama o zeď a tlačit
    - muži/ženy: 
        - jednou rukou (oboustranně)
        - dvěma rukama
    - puberťáci: 
        - nohou

- Kdy to nedělat?
    - nikdy ne v mánii!
    - Kontraindikace jen vysoký nitrooční tlak
    - mnohem lepší než mlátit pěstmi – ublížení si, navíc lidé mívaji často blbé pocity že něco ničí nebo někomu ubližují (pak stud, nenávist atd.) Navíc je to příliš prudká abreakce, kdy lidi jdou do afektu a nemají to pod kontrolou což je špatně! Naopak je třeba mít limit.
    - Limit dostává i tím, že se zeď pod jeho energií fakt nezhroutí.""";
const String article_7_3 = """### Co nedělat?
Nesnažíme se ho zavřít někam, nereagujeme na jeho agresi svou agresí, nesnažíme se ho potrestat, nezesilujeme nebo neprovokujeme jeho hněv.
## **Projekce**
Připisování vlastních nepřijatelných vlastností nebo emocí druhým lidem *„Vy jste smutný, naštvaný….“*
### Co dělat?
Popíšeme, co cítíme, nabídneme, že i zasažený se tak může cítit, může se na nás hněvat
### Co nedělat?
Nepopíráme a nepotlačujeme své pocity, nevysvětlujeme pouze racionálně. 
""";
const String article_8_1 = """
- intervence pro malou skupinu
- pro homogenní skupinu
- ukončení situace
- všichni byli vystaveni traumatu přibližně stejně
- trvání 20 – 45 min.
- slouží k „začištění“ události

# Úvod
- představení týmu a uvedení pravidel
- důraz na důvěrnost
- nikdo nebude nucen mluvit
- mluvte o své vlastní zkušenosti
- tým bude nejdříve poslouchat – informovat bude později
- zkušenost každého jedince je důležitá
- účelem není hledat viníka či někoho obviňovat
- účelem není kritika či vyšetřování
- dbejte na dodržování pravidel

# Explorace (Fakta) – ptejte se….
- Co se z Vašeho pohledu stalo?
- Kdo dorazil první? Co se stalo? Co bylo dále?
- Byl jste přímo v kontaktu se zasaženými?
- Co pro Vás bylo podstatné/významné?
- Co ve Vás přetrvává?
- Něco dalšího?

# Informace
- informujte o možných reakcích
- normalizujte jejich projevy
- nabídněte doporučení, co obvykle pomáhá 
- varujte před alkoholem, drogami, tučným jídlem, nevhodným - jídlem, kofeinem a nikotinem
- podpořte smysluplné aktivity
- shrňte důležité informace a dejte prostor pro dotazy
- poskytněte letáky/kontakty""";

const String article_8_2 = """
- 30 minutová intervence pro velkou skupinu
- pro zasahující/homogenní skupinu
- po katastrofách/událostech velkého rozsahu
- provádí se pouze jedenkrát, po prvotním vystavení události
- provádí se po ukončení záchranných a likvidačních prací
- pro skupiny, které byly přibližně stejnou měrou vystaveny traumatu
- začíná se stručným úvodem interventa
- poskytují se informace/zdůrazňují se instrukce pro vyrovnání se se stresem
- ptáme se na dotazy/komentáře		
- při demobilizaci mluví zasahující příslušníci zřídka 
- informační část trvá max. 10 minut
- 20 minut je vyhrazeno na odpočinek a jídlo
- zasahující mohou jít domů či jít plnit své další povinnosti
- zasahující příslušníci by se neměli vrátit na místo události nejméně dalších 6 hodin""";

const String article_8_3 = """
- intervence pro velkou skupinu
- 30 – 45 minut
- pro lidi, kteří nejsou zasahující
- vyžaduje myšlení a plánování
- používán před, během nebo po krizové události
- opakuje se podle změny situace
- vyžaduje homogenní skupinu
- skupiny se stejnou měrou vystavení události
- nehrozí žádné bezprostřední nebezpečí
- shromažďují se dohromady specifické skupiny
- důvěryhodný představitel prezentuje fakta
- stručná řízená diskuze/otázky a odpovědi
- podání informací/instrukcí/zdůraznění dovedností, které pomáhají 
- otevření možnosti následného setkání všech zúčastněných
""";
const String article_9_1 = """
- Věnuji pozornost dechu. Dýchám hluboce a pomalu. Uvědomuji si nádechy a výdechy.
- Zaměřím se na to, co cítím a jaké mne napadají myšlenky. Neodháním je. Uvědomím si, že prožívám mnoho různých pocitů, např. smutek, radost, vztek, úžas, pocit neskutečna, uspokojení, neklidu, rozčarování… Přichází například i pocit hněvu a bezmoci z toho, že jsem nemohl udělat více nebo že jsem nemohl udělat vůbec nic. Pocity přijímám, patří k silnému zážitku. Neutápím se v nich, registruji je. Současně si uvědomuji, že jsem udělal vše, co bylo momentálně v mých silách a ostatní také.
- Zrekapituluji pro sebe, co jsem měl chuť udělat, ale nemohl jsem. („Řekl bych třeba necitlivému kolegovi, ať je raději ticho nebo bych ho poslal někam pryč.“, „Měl jsem chuť dát tomu opilému řidiči pěstí, kopnout ho.“, „Nejraději bych utekl někam daleko, pryč od toho všeho.“)
- Uvědomím si, že to, co jsem prožil, není běžná lidská zkušenost a mám právo být po určitou dobu také trochu „mimo“, než se s tím- to prožitkem vyrovnám.
- Nezůstanu se svým zážitkem sám, mluvím o něm se svými blízkými, kolegy v práci apod. co nejdříve po zásahu. Případně si vyžádám intervenci psychologa jen pro sebe nebo pro celou skupinu, která může být zasažena.
- Pokud máte potřebu, kontaktujte své blízké, abyste se ujistili, že je u vás doma vše v pořádku
- Připomenu si, co mi jindy pomohlo v nějaké tíživé situaci. Pomohlo by mi to také nyní? Nebo potřebuji něco jiného?
- Pokud mi obvykle pomáhá sprcha, koupel, hudba, film, pohyb, práce, dopřeji si je.
- Snažím se co nejdříve zapojit do normálního života, dělám běžné věci v obvyklou dobu, piji nápoje bez alkoholu, věnuji pozornost tomu, co jím, zachovávám hygienu, zacvičím si, uklidím po sobě, jdu spát.

Pokud u sebe od události pozoruji příznaky: narušený spánek nebo nespavost, děsivé sny, tendenci uchylovat se k alkoholu a zneužívat léky, vyhýbání se lidem, nepřiměřenou slovní a fyzickou agresivi- tu, necitlivost a cynismus, neobvyklé nálady, nevysvětlitelný hněv a smutek, pak potřebuji odbornou pomoc a je dobrá ji vyhledat. 
""";