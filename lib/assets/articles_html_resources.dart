const html_article_1 = """
<h2>Cíl</h2>
<div style="text-align: justify">Cílem první psychické pomoci (zkráceně PPP; odvozeno z angl. First Psychological Aid, někdy překládáno též jako první psychologická pomoc) je stabilizovat člověka, který se dostal do náročné životní situace. Je možné ji přirovnat k první zdravotnické pomoci, která slouží k zajištění základních životních funkcí člověka, než je předán do další odborné péče. V první psychické pomoci se snažíme člověka, který momentálně není schopen uplatnit své mechanismy zvládání stresu, stabilizovat tak, aby dokázal nastalou situaci zvládnout sám nebo si našel alespoň někoho, kdo mu pomůže.</div>
<h2>Různé přístupy</h2>
<h3><u>PPP v systému Critical Incident Stress Management podle G. S. Everlyho a C. L. Parkerové</u></h3>
<ul>
<li><div style="text-align: justify">strukturovaný přístup, který byl původně vytvořen pro záchranáře, hasiče a policisty v USA</div></div></li>
<li><div style="text-align: justify">přístup zaměřený na redukci distresu a vyhledání potřeb k zajištění péče o psychické zdraví člověka</div></div></li>
<li><div style="text-align: justify">může přispívat k prevenci výskytu posttraumatických symptomů</div></div></li></ul>
<h3><u>PPP podle National Child Traumatic Stres Network (NCTSN) a National Centre for PTSD (NCPTSD)</u></h3>
<ul>
<li><div style="text-align: justify">standardizovaný přístup zaměřený na pomoc dětem, dospívajícím, dospělým a rodinám, které zažívají bezprostřední následky krizových situací, např. katastrof či terorismu</div></div></div></li>
<li><div style="text-align: justify">zaměřen na redukci počáteční tísně způsobené traumatickou událostí</div></li>
<li><div style="text-align: justify">podporuje krátkodobé a dlouhodobé zvládací a vyrovnávací mechanismy</div></li></ul>
<h3><u>PPP podle Světové zdravotnické organizace (WHO)</u></h3>
<ul>
<li><div style="text-align: justify">lidská, vstřícná podpora lidem, kteří trpí a potřebují pomoc</div></li>
<li><div style="text-align: justify">je určena lidem, kteří byli v poslední době vystaveni vážné krizové situaci</div></li>
<li><div style="text-align: justify">není vnucována lidem, kteří ji nechtějí</div></li>
<li><div style="text-align: justify">měla by být snadno přístupná pro ty, kteří pomoc a podporu potřebují a chtějí přijmout</div></li>
</ul>
""";

const html_article_2_1_R = """
<strong><span style="text-decoration: underline"><span style="color: #ff0000">R</span>eflektující naslouchání</span></strong>
""";
const html_article_2_1_A = """
<strong><span style="text-decoration: underline"><span style="color: #ff0000">A</span>nalýza a zajištění základních potřeb</span></strong>
""";
const html_article_2_1_P = """
<strong><span style="text-decoration: underline"><span style="color: #ff0000">P</span>rvořadé zaopatření</span></strong>
""";
const html_article_2_1_I = """
<strong><span style="text-decoration: underline"><span style="color: #ff0000">I</span>ntervence – uvažování a chování</span></strong>
""";
const html_article_2_1_D = """
<strong><span style="text-decoration: underline"><span style="color: #ff0000">D</span>ej dál – propusť nebo předej</span></strong>
""";
const html_article_2_1_text =
    """<div style="text-align: justify"><p>Jednotlivé kroky <strong>RAPID</strong> není nutné provádět přesně v daném pořadí (pouze D je vždy na konci), mohou se dokonce i opakovat. Není také nutné projít všechny kroky, ale vždy je nezbytné udělat krok D a minimálně jeden ze čtyř ostatních.</p></div>""";

const html_article_2_1_1 =
    """<h2><span style="color: #ff0000">R</span>eflektující naslouchání</h2>
    <div style="text-align: justify"><p><strong>Reflektující naslouchání</strong> je základní dovednost, kterou používáme při poskytování jakékoliv podpory druhým. Reflektuje se:</p>
    <ul>
    <li><div style="text-align: justify"><strong>Událost</strong> (např. <em>„Vidím, že máte v ruce telefon, asi jste s někým mluvil.“</em>), člověku v krizi to pomáhá orientovat se v situaci a navrátit do reality.</div></li>
    <li><div style="text-align: justify"><strong>Reakce</strong> (např. <em>„Sedíte tady už nějakou dobu na posteli a díváte se do kouta.“</em>), pomáhá navázat kontakt interventa s jedincem v krizi.</div></li>
    <li><div style="text-align: justify"><strong>Obsah</strong> - parafrázování, shrnování (např. <em>„Říkal jste, že …“, „Jestli tomu dobře rozumím tak, ….“</em>), lidé zjišťují, že má někdo o ně zájem, že na to nemusí být sami.</div></li>
    <li><div style="text-align: justify"><strong>Emoce</strong> (např. <em>„No tak to je mazec!“</em>, <em>„Vidím, že Vás to vzalo, asi bych byl taky naštvaný, kdyby mi tohle řekl.“</em>), reflexe emocí pomáhá jejich uvolnění. 
    </ul>
    Zlaté pravidlo PPP: <strong>„Když nevíš jak dál, reflektuj!“</strong>""";

const html_article_2_1_2 =
    """<h2><span style="color: #ff0000">A</span>nalýza a zajištění základních potřeb</h2>
    <div style="text-align: justify">Identifikace základních potřeb se opírá o <strong>Maslowovu hierarchii potřeb</strong>. Potřeby jsou seřazeny v pořadí, v jakém by měly být zjišťovány.</div>
    <ul><li><div style="text-align: justify"><strong>Zdravotní péče</strong> – zjišťujeme zranění, bolest, mdloby, nevolnost atd., ihned řešíme ošetřením nebo zajištěním zdravotní péče.</div></li>
    <li><div style="text-align: justify"><strong>Fyzické potřeby</strong> – pokládáme otázky: <em>„Kdy jste naposledy jedl? …pil? …spal? Potřebujete na toaletu?“</em> Velmi vhodné je v této situaci také nabídnutí nápoje (voda, čaj). </div></li>
    <li><div style="text-align: justify"><strong>Potřeba bezpečí</strong> – zajištění bezpečí jak přímého (odvedení z nebezpečného místa, snížení rizika pádu apod.), tak nepřímého (odvedení člověka do bezpečného prostředí – aby na něj ostatní nebo konkrétní osoby neviděli, aby mohl dát bezpečně průchod svým emocím – pláči, vzteku, smutku).</div></li>
    <li><div style="text-align: justify"><strong>Potřeba fungovat</strong> – zajištění možnosti podílet se na své stabilizaci a zase převzít aktivitu, nedělat úkony za klienta, např. telefonovat, zjišťovat informace, které je schopen si zajistit sám apod. Lidé mají v sobě zakotvenou schopnost fungovat v různých situacích a cílem PPP by mělo být podpoření této schopnosti.</div></li>
    </ul>""";

const html_article_2_1_3 =
    """<h2><span style="color: #ff0000">P</span>rvořadé zaopatření</h2>
    <ul>
    <li><div style="text-align: justify">Umístění znaků a symptomů o kontextu krize: <em>„Je toto jednání adekvátní dané situaci?“</em></div></li>
    <li><div style="text-align: justify">Hodnocení, zda je chování člověka v dané situaci funkční nebo dysfunkční. Je důležité ověřit, zda není chování člověka ovlivněno aktuální atakou psychického onemocnění, protože v takovém případě nelze člověka plně stabilizovat, je nutné zajistit odbornou lékařskou péči.</div></div></li>
    <li><div style="text-align: justify">Sledování <strong>projevů dysfunkce</strong> (dysfunkce je známkou potřeby psychologické první pomoci):<ul>
    <li><div style="text-align: justify">Disociace, člověk má pocit, že daná situace se ho netýká, jakoby vše vnímal za sklem, necítí emoce.</div></li>
    <li><div style="text-align: justify">Deprese, skleslost, hluboký smutek, lidé jsou plačtiví, mají pocit, že to nikdy neskončí.</div></li>
    <li><div style="text-align: justify">Psychosomatické potíže se vyskytují velmi často. Mnohdy jsou jediným projevem, který si často ani nemusíme spojovat s těžkou životní situací. Patří sem bolesti zad, hlavy, kloubů apod. Také se zhorší projevy alergií, objevují se neznámé vyrážky. Příznaků na tělesné úrovni je mnoho a měla by jim být věnována náležitá pozornost.</div></li>
    <li><div style="text-align: justify">Vina je vždy příznakem dysfunkce a je velmi riziková, protože může vést až k sebevraždě.</div></li>
    <li><div style="text-align: justify">Poruchy spánku vedou často k vyčerpání a následně ke snížení schopnosti odolávat stresu. Tím se zasažený jedinec ocitá v začarovaném kruhu, ze kterého není úniku. Má omezenou možnost danou situaci zvládnout.</div></li>
    <li><div style="text-align: justify">Panika je spojena s velkou úzkostí, která způsobuje roztříštěnost v prožívání, chování člověka je často neúčelné a zmatené.</div></li>
    <li><div style="text-align: justify">Tendence k násilí má svůj původ ve zvýšené hladině adrenalinu, člověk je doslova nabuzen a má potřebu vybít nahromaděnou energii, proto jsou násilné projevy při prožívání krize poměrně časté. Pomoz nalézt vhodný a přijatelný způsob, jak nahromaděnou energii ventilovat.</div></li>
    <li><div style="text-align: justify">Spoléhání (zneužívání) na medikaci. Předčasné podávání utišujících prostředků může zablokovat uplatnění přirozených vyrovnávacích mechanismů, kterými disponuje každý člověk. Snaž se jejich podání v této fázi zamezit.</div></li>
    <li><div style="text-align: justify">Nedostatek sociální opory může být reálný (absence podpůrné sociální sítě), nebo domnělý. Lidé zasažení krizí často nevnímají své okolí jako podporující, přestože se jim ostatní snaží pomoci, jak nejlépe dovedou.</div></li>
    <li><div style="text-align: justify">Hyperventilace se řeší řízeným dýcháním, dýcháním do pytlíku, do dlaní, stabilizačním chvatem „přes dlouhé kosti“, tj. pevným stisknutím dlouhých kostí na předloktí k sobě.</div></li>
    </ul>    
    </ul>""";

const html_article_2_1_4 =
    """<h2><span style="color: #ff0000">I</span>ntervence – uvažování a chování</h2>
    <ul>
    <li><div style="text-align: justify"><i>„Veď, vzdělávej!“ </i><span style="font-style: normal"> Neboj se být direktivní (zejména v situacích ohrožení života) a člověka v krizi dostatečně edukuj. Poskytované informace by měly být jasné, stručné a srozumitelné. Musí být podány nematoucím způsobem a v situaci, kdy je schopen člověk v těžké situaci vnímat obsah sdělovaného.</span></div></li>
    <li><div style="text-align: justify"><i>„Získej pozornost.“</i><span style="font-style: normal"> Ptej se na jméno klienta, jeho příjmení, na to, co se stalo. Snaž se zaměřit na situaci „tady a teď“ a na budování vztahu.</span></div></li>
    <li><div style="text-align: justify"><i>„Dýchejte.“</i><span style="font-style: normal"> Správné, klidné dýchání může uklidnit interventa i klienta. Doporučují se jeden až dva hluboké nádechy a výdechy, poté dýchání volně dle potřeby.</span></div></li>
    <li><div style="text-align: justify"><i>„Změň myšlení.“</i><span style="font-style: normal"> Pokus se změnit způsob uvažování klienta, „přetnout kruh“ jeho katastrofického a nelogického myšlení. Je-li to vhodné a možné (nikoliv za každou cenu), pokus se nalézt něco pozitivního.</span></div></li></ul>""";

const html_article_2_1_5 =
    """<h2><span style="color: #ff0000">D</span>ej dál - propusť nebo předej</h2>
    <ul>
    <li><div style="text-align: justify">Pokud je jedinec stabilizovaný, není nutné ho předávat do nějaké další odborné péče.</div></li>
    <li><div style="text-align: justify">PPP by měla směřovat také k identifikaci a aktivizaci přirozených zdrojů opory, např.<ul>
    <li><div style="text-align: justify">přátelé</div></li>
    <li><div style="text-align: justify">rodina</div></li>
    <li><div style="text-align: justify">společnost (komunita, sousedé…)</div></li>
    <li><div style="text-align: justify">práce (kolegové, nadřízený…)</div></li>
    </ul>
    </div></li>
    <li><div style="text-align: justify">Jestliže není jedinec schopný zvládat situaci sám, je vhodné ho nasměrovat na adekvátní následnou péči ke kompetentním odborníkům.</div></li>
    </ul>""";

const html_article_2_2 = """<h2>Navaž kontakt</h2>
        <ul>
            <li><div style="text-align: justify">Najdi odvahu zasaženou osobu oslovit – <em>„Jsem hasič...“, „Stalo se...“, „Jak se jmenujete?“</em></div></li>
            <li><div style="text-align: justify">Ujisti, dodej pocit bezpečí – <em>„Nebojte se, jsem u Vás, budu potřebovat Vaši spolupráci…“</em></div></li>
        </ul>
        <h2>Zjisti zdravotní stav</h2>
        <ul>
            <li><div style="text-align: justify">Je zraněný? Ošetři nebo ošetření zprostředkuj! <em>„Slyšíte mě? Bolí Vás něco? Víte, co se stalo?“</em>
            </div></li>
            <li><div style="text-align: justify">Chraň soukromí - dbej na důstojnost.</div></li>
            <li><div style="text-align: justify">Chraň zasaženou osobu před zvědavci, případně před médii.</div></li>
        </ul>
        <h2>Informuj, co se děje, a naslouchej</h2>
        <ul>
            <li><div style="text-align: justify">Podávej reálné informace. <em>„Jsem tu proto, ...“, Bude se dít…“, „Lékař je na cestě.“, „Teď uslyšíte ránu, protože...“</em> atd.</div></li>
            <li><div style="text-align: justify">Vyslechni. Podpoř.</div></li>
            <li><div style="text-align: justify">Vyhni se frázím. Mluv jednoduše, v krátkých větách.</div></li>
        </ul>
        <h2>Zjišťuj a zajišťuj základní potřeby</h2>
        <ul>
            <li><div style="text-align: justify">Zajisti základní potřeby - pět T: teplo, ticho, tekutiny, transport, tišící prostředky. I o dospělého pečuj jako o dítě.</div></li>
            <li><div style="text-align: justify">Pil? Byl na záchodě?</div></li>
            <li><div style="text-align: justify">Je mu dost teplo? Nabídni deku či svetr.</div></li>
            <li><div style="text-align: justify">Je někdo s ním? Postrádá někoho?</div></li>
            <li><div style="text-align: justify">Má se o co opřít? Posaď, podepři, opři, hledej opatrně tělesný kontakt.</div></li>
            <li><div style="text-align: justify">Má se kde umýt? Zajisti.</div></li> 
        </ul>
        <h2>Hlídej bezpečí</h2>
        <ul>
            <li><div style="text-align: justify">Odveď ho, je-li mu místo nebezpečné nebo nepříjemné. (Využij váš automobil nebo pomoc lidí v okolí.)
            </div></li>
            <li><div style="text-align: justify">Zabraň pohybu, je-li pro něj nebezpečný. Jinak se můžeš pohybovat s ním.</div></li>
            <li><div style="text-align: justify">Dbej na vlastní bezpečí.</div></li>
            <li><div style="text-align: justify">Spolupracuj s druhými na místě. (Se sousedy, s cestujícími z vozů, kteří zastavili, aby pomohli, atd.)
            </div></li>
            <li><div style="text-align: justify">Pomoz přemýšlet o dalším postupu. (Odvoz domů, náhradní ubytování, apod.)</div></li>
            <li><div style="text-align: justify">Řeš situaci tady a teď, příp. bezprostřední následující kroky, nehledej dlouhodobá řešení.</div></li>
        </ul>
        <h2>Předej do péče blízkých osob nebo dalších služeb</h2>
            <div style="text-align: justify"><p>Pomoz je kontaktovat. Koho, ať si určí sám. Můžeš využít:
                <ul>
                    <li><div style="text-align: justify">místní zdroje - sousedy, starostu, místní instituce,</div></li>
                    <li><div style="text-align: justify">přes operační středisko kontakty na odborné služby - krizová centra, linky důvěry,</div></li>
                    <li><div style="text-align: justify">tel. konzultace s psychologem IZS (příp. si jej vyžádej na místo), týmy posttraumatické a psychologické péče.</div></li>
                </ul>
            </p>
""";

const html_article_2_3 = """
<h2>Přibliž se</h2>
<ul>
    <li><div style="text-align: justify">Najdi odvahu, zasaženého oslovit (na projevení lidskosti nemusíš být odborník).</div></li>
    <li><div style="text-align: justify">Představ se.</div></li>
</ul>
<h2>Podepři</h2>
<ul>
    <li><div style="text-align: justify">Zajisti bezpečí.</div></li>
    <li><div style="text-align: justify">Odveď zasaženého z exponovaného místa. Posaď ho, opři o zeď, podepři svým tělem, stabilizační pozicí.</div></li>
    <li><div style="text-align: justify">Chraň lidskou důstojnost zasaženého.</div></li>
</ul>
<h2 >Připomínej realitu</h2>
<ul>
    <li><div style="text-align: justify">Oslovuj (jménem, příjmením, titulem).</div></li>
    <li><div style="text-align: justify">Ptej se, zda ví, co se stalo, kde se nachází.</div></li>
    <li><div style="text-align: justify">Poskytuj potřebné informace, pomáhej zorientovat se v situaci.</div></li>
    <li><div style="text-align: justify">Zaměřte se na dýchání – i slovně: „klidný nádech- výdech“.</div></li>
</ul>
<h2 >Podpoř</h2>
<ul>
    <li><div style="text-align: justify">Podpoř projevení emocí.</div></li>
    <li><div style="text-align: justify">Uklidni zasaženého, že jeho prožívání a projevy jsou v takových náročných situacích normální.</div></li>
    <li><div style="text-align: justify">Projev důvěru v jeho schopnosti situaci zvládnout.</div></li>
</ul>
<h2 >Pečuj</h2>
<ul>
    <li><div style="text-align: justify">Zjišťuj potřeby, nabízej vodu, přikrývku, kapesník, umožnění vykonat intimní potřebu.</div></li>
    <li><div style="text-align: justify">Naslouchej (neboj se i mlčet).</div></li>
    <li><div style="text-align: justify">Citlivě poskytuj tělesný kontakt.</div></li>
</ul>
<h2 >Předej</h2>
    <ul>
        <li><div style="text-align: justify">Nemusíš zvládat vše sám.</div></li>
        <li><div style="text-align: justify">Předej zdravotníkům, příbuzným, kamarádům, sousedům.</div></li>
        <li><div style="text-align: justify">Pokuste se společně najít kontakt, např. v mobilu.</div></li>
    </ul>
""";

const html_article_3_1_1 = """
<h2 >Co to je?</h2>
<ul>
    <li><div style="text-align: justify">Cílem je uklidnit a zorientovat emocionálně rozrušené nebo dezorientované osoby.</div></li>
    <li><div style="text-align: justify">Silné emoce, strnulost a úzkost jsou očekávatelné reakce po neštěstí. Většina zasažených bude schopna se uklidnit sama.</div></li>
    <li><div style="text-align: justify">Věnujte pozornost těm jedincům, jejichž reakce ovlivňují jejich schopnost fungovat.</div></li>
</ul>
<h2 >Kdo ji potřebuje?</h2>
<ul>
    <li><div style="text-align: justify">Dezorientovaný: věnuje se nesmyslnému dezorganizovanému chování.</div></li>
    <li><div style="text-align: justify">Odpojený: strnulý, překvapivě nedotčený událostí.</div></li>
    <li><div style="text-align: justify">Zmatený: neschopný porozumět tomu, co se děje kolem něj, nechápe smysl.</div></li>
    <li><div style="text-align: justify">V panice: extrémně úzkostný, neschopen se vyrovnat s událostí, oči doširoka otevřené a těkající.</div></li>
    <li><div style="text-align: justify">Nekontrolovaně vzlykající, hyperventilující.</div></li>
    <li><div style="text-align: justify">Přespříliš zaměstnaný: neschopen přemýšlet o čemkoliv jiném.</div></li>
    <li><div style="text-align: justify">V popření: odmítající přijmout, že se událost stala.</div></li>
    <li><div style="text-align: justify">Ve stavu šoku: neschopen se pohnout, ztuhlý.</div></li>
    <li><div style="text-align: justify">Nepřítomně hledící („čumí do blba“), mající skelný zrak.</div></li>
    <li><div style="text-align: justify">Nereagující na verbální otázky nebo povely.</div></li>
    <li><div style="text-align: justify">Projevující se zuřivým hledáním.</div></li>
    <li><div style="text-align: justify">Zavalený starostmi.</div></li>
</ul>
<h2 >Jak postupovat?</h2>
<ul>
    <li><div style="text-align: justify">Před intervencí se na krátkou chvilku zastavte a všimněte si sami sebe.</div></li>
    <li><div style="text-align: justify">Zůstaňte klidní, potichu a přítomní, spíše než abyste se pokoušeli mluvit přímo na dotyčného člověka.</div></li>
    <li><div style="text-align: justify">Stůjte poblíž, i když děláte jiné věci, tak abyste byli k dispozici, když to bude osoba potřebovat.</div></li>
    <li><div style="text-align: justify">Nabídněte podporu a pomozte zasaženému se soustředit na vybrané zvládnutelné pocity, myšlenky a cíle.</div></li>
    <li><div style="text-align: justify">Poskytujte ověřené informace, které osobu orientují v prostředí.</div></li>
    <li><div style="text-align: justify">Vyjasněte dezinformace nebo nedorozumění o tom, co se děje, a proberte starosti týkající se bezpečnosti.</div></li>
    <li><div style="text-align: justify">Vysvětlete, že intenzivní emoce mohou přicházet a odcházet ve vlnách, a že někdy může pomoci použití uklidňovacích technik (dýchání, rozptýlení) nebo podpora přátel/rodiny.</div></li>
    <li><div style="text-align: justify">Jestliže tyto reakce pokračují a ovlivňují fungování jedince, můžete zvážit použití uzemnění (grounding). </div></li>
</ul>""";

const html_article_3_1_2 = """
<h2 >Zasaženým lidem můžete nabídnout tento postup:</h2>
<ul>
    <li><div style="text-align: justify">„Pohodlně se posaďte, bez překřížených rukou a nohou.</div></li>
    <li><div style="text-align: justify">Pomalu, normálně dýchejte.</div></li>
    <li><div style="text-align: justify">Vyberte a pojmenujte 5 nestresujících věcí, které jsou kolem vás.</div></li>
    <li><div style="text-align: justify">Pomalu, normálně dýchejte.</div></li>
    <li><div style="text-align: justify">Identifikujte a pojmenujte 5 nestresujících zvuků, které slyšíte.</div></li>
    <li><div style="text-align: justify">Pomalu, normálně dýchejte.</div></li>
    <li><div style="text-align: justify">Vyberte a pojmenujte 5 nestresujících věcí, které můžete cítit.</div></li>
    <li><div style="text-align: justify">Pomalu, normálně dýchejte.“</div></li>
    <li><div style="text-align: justify">Pro děti</div></li>
    <li><div style="text-align: justify">Můžete jim říct, aby vyjmenovaly barvy, které kolem sebe vidí. Například položte tyto otázky:</div></li>
    <li><div style="text-align: justify">„Můžeš říct 5 barev, které vidíš z tohoto místa, kde sedíš?</div></li>
    <li><div style="text-align: justify">Vidíš něco modrého? Něco žlutého? Něco zeleného?“</div></li>
</ul>""";

const html_article_3_1_3 = """
<ul>
    <li><div style="text-align: justify">Vyhledejte lékaře, je-li to vhodné/nutné.</div></li>
</ul>""";

const html_article_3_2_1 = """
<h2 >Co to je?</h2>
<div style="text-align: justify"><p>Cílem je zvýšit okamžité a trvalé bezpečí, a poskytnout fyzickou a emocionální útěchu.</p>
<h2 >Jak postupovat?</h2>
<ul>
<li><div style="text-align: justify">Poskytněte poslední, přesné a aktuální informace. Pokuste se vyhnout sdělování informací, které jsou nepřesné nebo velmi znepokojující.</div></li>
<li><div style="text-align: justify">Soustřeďte se na činnosti, které jsou aktivní, praktické (použijte dostupné zdroje) a známé (čerpající z předchozích zkušeností).</div></li>
<li><div style="text-align: justify">Identifikujte a proberte starosti o bezpečí. Najděte odpovědné osoby, které mohou vyřešit starosti o bezpečí, jež jsou mimo vaši kontrolu.</div></li>
<li><div style="text-align: justify">Najděte jednoduchý způsob, jak vytvořit příjemnější okolní prostředí.</div></li>
<li><div style="text-align: justify">Monitorujte média a různé sociální sítě, abyste zachytili nepravdivé informace a uvedli je na pravou míru.</div></li>
<li><div style="text-align: justify">Podporujte skupinové a sociální interakce k podpoře přirozeného vyrovnávání se s událostí.</div></li>
<li><div style="text-align: justify">Chraňte zasažené před dalším vystavováním nepříjemným zkušenostem nebo připomínáním traumatu (např. sledovat záchranné práce nebo média).</div></li>
<li><div style="text-align: justify">Buďte k dispozici dětem bez doprovodu.</div></li>
</ul>
""";

const html_article_3_2_2 = """
<ul>
    <li><div style="text-align: justify">Sehněte se na výšku jejich očí, abyste byli méně ohrožujícími.</div></li>
    <li><div style="text-align: justify">Představte se a navažte kontakt.</div></li>
    <li><div style="text-align: justify">Postarejte se o základní potřeby (jídlo, voda, zdravotní ošetření).</div></li>
    <li><div style="text-align: justify">Zeptejte se na identifikační údaje (jméno, jména rodičů / pečujících osob a sourozenců, adresu domů, na školu).
    </div></li>
    <li><div style="text-align: justify">Uvědomte odpovědné osoby.</div></li>
    <li><div style="text-align: justify">Poskytněte dětem informace, kdo se o ně postará a co bude následovat.</div></li>
    <li><div style="text-align: justify">Neslibujte, co nemůžete splnit, a nelžete jim.</div></li>
    <li><div style="text-align: justify">Ujistěte se, že je o děti postaráno.</div></li>
</ul>
""";

const html_article_3_2_3 = """<ul>
<li><div style="text-align: justify">Poskytněte čas a podporu lidem, kteří pohřešují někoho blízkého.</div></li>
</ul>
""";

const html_article_3_2_4 = """
<ul>
    <li><div style="text-align: justify">Popište kroky, které byly podniknuty k nalezení pohřešované osoby.</div></li>
    <li><div style="text-align: justify">Dovolte jim, aby kladli otázky, a informujte je, co vše se dělá, aby byla pohřešovaná osoba nalezena.</div></li>
    <li><div style="text-align: justify">Podpořte jejich aktivity při hledání, které je nevystaví nebezpečí (zavolat přátelům, vyplnit registrační údaje).</div></li>
    <li><div style="text-align: justify">Pomozte členům rodiny pochopit, že mohou mít různé reakce (někteří se vzdávají naděje, jiní doufají) a podpořte je, aby byli trpěliví, měli pochopení a respektovali tyto rozdíly.</div></li>
    <li><div style="text-align: justify">Připravte je na poskytování údajů úřadům.</div></li>
</ul>""";

const html_article_3_2_5 = """
<ul>
<li><div style="text-align: justify">Postarejte se o lidi, kterým zemřel někdo blízký.</div></li>
</ul>
""";

const html_article_3_2_6 = """
<ul>
    <li><div style="text-align: justify">Uvědomte si své limity při mluvení o smrti. Pokud je pro vás těžké mluvit o smrti, poproste o pomoc kolegu.</div></li>
    <li><div style="text-align: justify">Poskytněte bezpečný prostor k truchlení.</div></li>
    <li><div style="text-align: justify">Naslouchejte pozorně a empaticky.</div></li>
    <li><div style="text-align: justify">Nechte je, aby vám řekli, co potřebují.</div></li>
    <li><div style="text-align: justify">Mluvte na jejich úrovni, zvláště s dětmi.</div></li>
    <li><div style="text-align: justify">Pamatujte na to základní – zůstaňte klidní a nespěchejte.</div></li>
    <li><div style="text-align: justify">Připomeňte jim, že neexistuje jediná správná cesta truchlení a že různí členové rodiny mohou jednat a cítit se různě. Podpořte trpělivost, porozumění a respekt.</div></li>
    <li><div style="text-align: justify">Ujistěte truchlící, že jejich reakce jsou pochopitelné.</div></li>
    <li><div style="text-align: justify">Očekávejte široké pole reakcí při truchlení, někteří lidé vyjadřují silné emoce a někteří je nedávají najevo.
    </div></li>
    <li><div style="text-align: justify">U dětí se mohou nálady rychle střídat. Projevy jsou různé, i když se děti chovají jako obvykle, neznamená to, že netruchlí.</div></li>
</ul>
""";

const html_article_3_2_7 = """
<ul>
<li><div style="text-align: justify">Odpovídejte na dotazy, jak pomoci dětem a dospívajícím vyrovnat se se smrtí někoho blízkého.</div></li>
</ul>
""";

const html_article_3_2_8 = """
<ul>
    <li><div style="text-align: justify">Ujistěte děti, že jsou milovány a že o ně bude postaráno.</div></li>
    <li><div style="text-align: justify">Vnímejte, jestli jsou děti připraveny mluvit o tom, co se stalo.</div></li>
    <li><div style="text-align: justify">Nenuťte děti mluvit. Někdy mohou potřebovat aktivity k odvedení pozornosti (hraní, kreslení, poslouchání
        muziky), aby se uklidnily.</div></li>
    <li><div style="text-align: justify">Odpovídejte na jejich otázky krátce, jednoduše, poctivě a přiměřeně věku.</div></li>
    <li><div style="text-align: justify">Naslouchejte jim, neodsuzujte, nekritizujte.</div></li>
    <li><div style="text-align: justify">Odpovězte na jejich otázky o pohřbech, pohřbívání, modlitbě a jiných rituálech.</div></li>
    <li><div style="text-align: justify">Buďte připraveni odpovídat na jejich otázky znovu a znovu.</div></li>
    <li><div style="text-align: justify">Nebojte se říct, že neznáte odpověď na otázku.</div></li>
    <li><div style="text-align: justify">Pamatujte, že děti různého věku mají rozdílné reakce na smrt někoho blízkého.</div></li>
</ul>
""";

const html_article_3_2_9 = """<ul>
<li><div style="text-align: justify">Jak děti chápou v různém věku smrt?</div></li>
</ul>
""";
const html_article_3_2_10 =
    """<div style="text-align: justify">Dětské porozumění smrti se liší dle jejich věku a předchozí zkušenosti se smrtí a je silně ovlivněno rodinou, vírou a kulturními faktory.</div>
<h3 >Předškolní děti</h3>
<div style="text-align: justify">Nemusí rozumět, že smrt je trvalá, a mohou věřit, že se osoba může vrátit. Potřebují pomoci s potvrzením fyzické reality, že je osoba mrtvá (zesnulý už nedýchá, nehýbe se, nic necítí a necítí se nepohodlně nebo necítí bolest). Mohou mít starosti, že se něco špatného může stát jinému členovi rodiny.</div>
<h3 >Děti školního věku</h3>
<div style="text-align: justify">Rozumí fyzické realitě smrti, ale mohou personifikovat smrt jako monstrum nebo kostru. Touží, aby se jejich milovaná osoba vrátila, mohou zakoušet pocity přítomnosti „ducha“.</div>
<h3 >Dospívající</h3>
<div style="text-align: justify">Chápou, že smrt je nevratný proces. Ztráta člena rodiny nebo přítele může spustit vztek a impulzivní rozhodnutí, jako je opuštění školy, útěk nebo zneužívání drog a alkoholu. Tyto problémy vyžadují okamžitou pozornost rodiny nebo školy.</div>
""";

const html_article_3_3_1 = """<h2 >Co to je?</h2>
<div style="text-align: justify"><p>Poskytnutí praktické pomoci zasaženým při řešení jejich okamžitých potřeb a obav.</p>
<h2 >Jak postupovat?</h2>
<ul>
<li><div style="text-align: justify">Rozpoznejte nejnaléhavější potřeby.</div></li>
<li><div style="text-align: justify">Vyjasněte si potřeby zasažených.</div></li>
</ul>
""";
const html_article_3_3_2 = """<h2 >Dospělý/Pečovatel</h2>
<div style="text-align: justify">„Z toho, co jste mi řekla, paní Nováková, jsem pochopil, že právě teď je vaším hlavním cílem najít manžela a ujistit se, že je v pořádku. Musíme se zaměřit na to, abychom vám pomohli se s ním dostat do kontaktu. Pojďme vymyslet plán, jak budeme postupovat při získávání informací.“</div>
<h2 >Dospívající/Dítě</h2>
<div style="text-align: justify">„Mám pocit, že právě teď se hodně obáváš několika různých věcí. Co se stane s vaším domem, kdy přijde tvůj táta a co se bude dít dál. Všechny tyto věci jsou důležité, ale pojďme přemýšlet o tom, co je nejdůležitější právě v tuto chvíli, a zkusme vymyslet nějaký plán.“</div>""";

const html_article_3_3_3 = """<ul>
        <li><div style="text-align: justify">Prodiskutujte plán, který je praktický a zároveň proveditelný.</div></li>
        <li><div style="text-align: justify">Při řešení problému se zaměřte na návrhy a rozhodnutí zasažených.</div></li>
        </ul>
        """;
const html_article_3_3_4 = """<h2 >Jednání při řešení potřeb zasažených</h2>
<ul>
<li><div style="text-align: justify">Pomozte zasaženým se zahájením jejich plánů, protože stres může mít negativní dopad na poznávací schopnosti a schopnosti řešit problémy. Je možné, že budou potřebovat vaši pomoc při plánování schůzek, s kompletováním administrativy a s dalšími věcmi.</div></li>
<li><div style="text-align: justify">Pomozte zasaženým uvědomit si jejich potřeby a rozdělte jejich realizaci do menších kroků. Budou mít lepší pocit, že se dokáží s daným problémem vypořádat a mít prospěch z vědomí, že mohou zajistit sami sebe.</div></li>
<li><div style="text-align: justify">Pamatujte, že každý zasažený prožívá situaci jinak. Někteří lidé mohou být aktivně samostatní, zatímco jiní se budou více spoléhat na informace a instrukce od vás. Přizpůsobte svůj přístup potřebám zasažených a dané situaci.</div></li>
</ul>
""";
const html_article_3_3_5 = """<ul>
        <li><div style="text-align: justify">Před pomocí zasaženým si uvědomte, jaké služby jsou pro ně dostupné. Poté budete připraveni zajistit užitečnou praktickou asistenci, při které uplatníte základní prostředky, jako je jídlo, oblečení, bydlení, zdravotnická péče, finanční pomoc a další služby.</div></li>
        <li><div style="text-align: justify">Ujistěte se, že vaše informace jsou přesné a aktuální. Znalostí dostupných poskytovaných služeb pomůžete zasaženým stanovit reálná očekávání.</div></li>
        </ul>""";

const html_article_3_4_1 = """<h2 >Co to je?</h2>
<div style="text-align: justify"><p>Zahájení komunikace se zasaženými nenásilným, nevtíravým, citlivým způsobem.</p>
<h2 >Jak postupovat?</h2>
<ul>
<li><div style="text-align: justify">Nejdříve reagujte na ty, kteří vás vyhledají.</div></li>
<li><div style="text-align: justify">Pokud se na vás současně obrátí více lidí, navažte kontakt s tolika lidmi, s kolika jste schopni.</div></li>
<li><div style="text-align: justify">Projevení zájmu a starostlivosti může být nápomocné pro lidi, kteří se cítí mizerně nebo zmateně.</div></li>
<li><div style="text-align: justify">Představte se jménem, uveďte svou funkci a krátce popište svou úlohu.</div></li>
</ul>
""";
const html_article_3_4_2 = """<div style="text-align: justify"><p>Například:
        <em>„Dobrý den. Jmenuji se <strong>___</strong>. Jsem záchranář. Snažím se zjistit, jak se tady lidem vede, jestli jim můžu nějak pomoci. Nevadilo by vám, kdybych si s vámi na chvíli popovídal? Mohu se zeptat, jak se jmenujete? Paní Nováková, než začneme s povídáním, nepotřebujete právě teď něco? Například trochu vody nebo přikrývku?“</em></p>
        """;
const html_article_3_4_3 = """<ul>
        <li><div style="text-align: justify">Pokud jedinec odmítne vaši nabídku pomoci, respektujte jeho/její rozhodnutí a informujte, na koho se může později obrátit.</div></li>
        <li><div style="text-align: justify">Nepředpokládejte, že s vámi lidé budou hned mluvit a reagovat na vás pozitivně. Někomu může trvat déle, než bude cítit dostatek bezpečí, jistoty a důvěry.</div></li>
        <li><div style="text-align: justify">Zeptejte se na okamžité potřeby.</div></li>
        <li><div style="text-align: justify">Respektujte osobní a kulturní hranice týkající se fyzického doteku, osobního prostoru a očního kontaktu.</div></li>
        <li><div style="text-align: justify">Pokuste se zajistit trochu soukromí a věnujte zasaženému vaši plnou pozornost.</div></li>
        </ul>
        """;
const html_article_3_5_1 = """<h2 >Co to je?</h2>
<div style="text-align: justify"><p>Shromažďování a vyhodnocování informací začíná okamžitě po navázání kontaktu a pokračuje v průběhu první psychické pomoci. Vaše schopnost shromažďovat informace bude omezena časem, potřebami zasažených, prioritami a dalšími faktory.</p></div>
<h2 >Jak postupovat?</h2>
<ul>
    <li><div style="text-align: justify">Shromážděte dostatek informací, abyste byli schopní reagovat na okamžité potřeby a obavy zasažených.</div></li>
    <li><div style="text-align: justify">Tento proces je neformální a probíhá během celé komunikace se zasaženým. Vyhněte se tomu, aby měl člověk pocit, že ho vyslýcháte.</div></li>
    <li><div style="text-align: justify">Držte se toho, co vám člověk říká. Vyhněte se požadování podrobných popisů prožité události, které by mohly vyvolat dodatečnou tíseň.</div></li>
    <li><div style="text-align: justify">Je důležité ptát se na otázky, které vám mohou pomoci se rozhodnout:<ul>
            <li><div style="text-align: justify">jaké základní kroky PPP jsou na místě,</div></li>
            <li><div style="text-align: justify">zda je třeba zasaženého okamžitě předat odborníkovi,</div></li>
            <li><div style="text-align: justify">jestli jsou potřeba další služby.</div></li>
        </ul>
    </div></li>
</ul>""";
const html_article_3_6_1 = """<h2>Co to je?</h2>
<div style="text-align: justify"><p>Pomoc navázat kontakt s blízkými osobami a dalšími zdroji podpory zahrnující členy rodiny, přátele a sousedy.</p>
<h2 >Jak postupovat</h2>
<ul>
    <li><div style="text-align: justify">Pomozte zasaženým se spojit s rodinou, přáteli a dalšími milovanými osobami (osobně, telefonicky, emailem, přes sociální sítě).</div></li>
    <li><div style="text-align: justify">Pobídněte zasažené k vytvoření nových kontaktů s bezprostředně dostupnými podporujícími osobami.</div></li>
    <li><div style="text-align: justify">Zlepšete přístup ke komunitním zdrojům nebo dalším zdrojům podpory, které mohou usnadnit zotavení (sociální služby, služby zabývající se závislostí, dětská péče, církevní podpora).</div></li>
    <li><div style="text-align: justify">Podejte pomocnou ruku těm, kteří jsou uzavření do sebe.</div></li>
</ul>""";
const html_article_3_6_2 = """<h2 >Pomáhání těm, co jsou uzavření do sebe:</h2>
<ul>
<li><div style="text-align: justify">Pokud mají sebevražedné sklony, okamžitě vyhledejte krizového interventa, případně lékařskou pomoc.</div></li>
<li><div style="text-align: justify">Pokud se zdráhají vyhledat pomoc, zjistěte proč. Možnými důvody mohou být: jsou zdrcení; neví, co potřebují; cítí se zahanbeně; obávají se, že zatěžují ostatní; pochybují, že podpora je k dispozici nebo mají strach z reakce ostatních, když požádají o pomoc; aktuálně pomoc nechtějí či nepotřebují.</div></li>
<li><div style="text-align: justify">Pomozte jim identifikovat způsob podpory, který by mohl být nejvíce nápomocný.</div></li>
<li><div style="text-align: justify">Pomozte jim vymyslet plán, jak se k této podpoře dostat, nebo jak se spojit s ostatními.</div></li>
<li><div style="text-align: justify">Proberte důležitost sociální podpory ve vypořádávání se s problémem.</div></li>
<li><div style="text-align: justify">Pamatujte, že někteří lidé navazovat kontakty na daném místě nechtějí. Zaměřte se tedy na kontakty, se kterými se mohou spojit ve svém obvyklém prostředí.</div></li>
</ul>
""";
const html_article_3_6_3 = """<ul>
<li><div style="text-align: justify">Podpořte zasažené při vyhledání pomoci i při tom, když chtějí pomoci druhým. Svým příkladem můžete předvést pozitivní podporující reakce.</div></li>
</ul>
""";
const html_article_3_6_4 =
    """<div style="text-align: justify"><p>Demonstrujte pozitivní a nápomocnou komunikaci. Předveďte podporující reakce pomocí následujících typů vyjádření:</p>
<h2>Reflektující</h2>
<em>„To, co mi říkáte, zní jako…“</em> nebo <em>„Z toho, co říkáte, mi připadá, jako byste byl…“</em>
<h2>Objasňující</h2>
<em>„Mám pravdu, když řeknu…?“</em> nebo <em>„Řekněte, jestli se mýlím, ale zní to jako byste říkal…“</em>
<h2>Podporující</h2>
<em>„Žádný div, že se cítíte…“</em> nebo <em>„Opravdu mě mrzí, že je to pro vás tak těžké období.“</em>
<h2>Posilující</h2>
<em>„Co jste dělal v minulosti, když jste byl ve složité situaci, abyste se cítil lépe?“</em>
""";
const html_article_3_7_1 = """<h2>Co to je?</h2>
<div style="text-align: justify"><p>Poskytování informací o stresových reakcích a zvládání za účelem snížit úzkost a podporovat dobré zvládání.</p>
<h2>Jak postupovat</h2>
<ul>
<li><div style="text-align: justify">Poskytněte základní informace o běžných stresových reakcích na traumatické události.</div></li>
</ul>
""";
const html_article_3_7_2 = """<h2>Běžné stresové reakce jsou:</h2>
<h3>Vtíravé reakce</h3>
Do mysli se vrací traumatický zážitek. To zahrnuje vtíravé myšlenky, představy události nebo sny o tom, co se stalo. Také to mohou být znepokojující emoční nebo fyzické reakce při vzpomínce na traumatický zážitek.
<h3>Vyhýbání se</h3>
Zasažení se pokouší oprostit nebo se chránit před úzkostí. Mohou se vyhýbat o traumatické události mluvit, myslet na ni nebo potlačují pocity s ní spojené. Vyhýbají se čemukoli, co by událost připomínalo, třeba určitým lidem nebo místům.
<h3>Fyzické napětí</h3>
Fyzické změny mohou tělo nutit reagovat, jako by nebezpečí bylo stále aktuální, i když už reálně pominulo. Zasažení neustále dávají pozor, snadno se vylekají nebo jsou nervózní, podráždění nebo rozmrzelí, mívají výbuchy vzteku nebo pláče, obtížně spí a těžko se soustředí.
<h3>Zármutek</h3>
Reakce po smrti milované osoby nebo velké ztrátě (vztahu, role, majetku...). Tyto ztráty mohou vést k pocitům smutku, zlosti, viny, lítosti. Touží, aby se situace vrátila zpět před událost.
<h3>Traumatický zármutek</h3>
Reakce, které nastanou, když  byli zasažení svědky traumatické smrti milované osoby. Mohou se zaměřovat na okolnosti smrti, přemýšlet, jak se smrti dalo předejít, jaké měly být poslední okamžiky a kdo je na vině. Tyto reakce mohou způsobovat komplikované truchlení.
<h3>Deprese</h3>
Mezi reakce patří depresivní nebo podrážděná nálada, ztráta chuti k jídlu, problémy se spánkem, vyčerpání, pocity bezcennosti nebo viny a někdy myšlenky na sebevraždu. Deprese bývá spojená se zármutkem a souvisí s nesnázemi po události.
<h3>Fyzické reakce</h3>
Tyto reakce zahrnují tělesná strádání, jako jsou bolesti hlavy, závratě, bolesti břicha, bolesti svalů, zrychlený srdeční tep, tíha na hrudi, hyperventilaci a střevní problémy. Zasažení mohou prožívat tyto typy reakcí, i když předtím neměli žádné zásadní fyzické zranění nebo nemoc.
""";
const html_article_3_7_3 = """<ul>
<li><div style="text-align: justify">Mluvte se zasaženými o připomínkách traumatu, a jak se s nimi lze vypořádat.</div></li>
</ul>
""";
const html_article_3_7_4 =
    """<div style="text-align: justify"><p>Připomínky traumatu, ztráty nebo změny, stejně jako trvající tíživá životní situace, mohou vyvolávat znepokojující myšlenky a pocity dlouho po události.</p>
<h2>Připomínky traumatu</h2>
Situace, která vyvolává znepokojující myšlenky a pocity spojené s událostí. Mohou to být zrakové vjemy, zvuky, místa, vůně či pachy, určití lidé, určitá denní doba, situace, nebo dokonce pocity. Kupříkladu po tornádu to může být zvuk větru, deště, helikoptéry, křik a další podněty, které má člověk spojené s událostí.
<h2>Připomínky ztráty</h2>
Situace, kvůli kterým může vytanout na mysli nepřítomnost milované osoby nebo ztraceného majetku a přinést silné pocity, jako je smutek, touha nebo nervozita (fotografie apod.).
<h2>Připomínky změny</h2>
Situace, které lidem připomínají, jak se jejich život změnil následkem katastrofy. Mohou to být osoby, místa, věci, aktivity nebo tíživá životní situace. Kupříkladu probuzení se ráno v odlišné posteli, chození do jiné školy nebo bytí na jiném místě.
""";
const html_article_3_7_5 = """<ul>
        <li><div style="text-align: justify">Zdůrazněte, že tyto stresové reakce jsou běžné a pochopitelné. Nicméně pokud přetrvávají déle než 4 až 6 týdnů, nebo se zhoršují a oslabují fungování, člověk by měl vyhledat další pomoc.</div></li>
        <li><div style="text-align: justify">Zjistěte, jaké způsoby vypořádávání použili zasažení v minulosti, a pomozte jim zvážit, zda jsou vhodné pro nynější situaci.</div></li>
        <li><div style="text-align: justify">Hledejte společně další strategie zvládání, jak se s problémem vypořádat.</div></li>
        </ul>
        """;
const html_article_3_7_6 = """<ul>
<li><div style="text-align: justify">Mluvte o tom s někým nebo hledejte přítomnost blízkého člověka (někdy stačí s někým jen být).</div></li>
<li><div style="text-align: justify">Dělejte něco, co vás baví.</div></li>
<li><div style="text-align: justify">Dopřejte si adekvátní odpočinek a jezte zdravá jídla.</div></li>
<li><div style="text-align: justify">Pokuste se udržovat obvyklý rozvrh.</div></li>
<li><div style="text-align: justify">Dávejte si pravidelné přestávky.</div></li>
<li><div style="text-align: justify">Používejte humor.</div></li>
<li><div style="text-align: justify">Naplánujte si příjemné aktivity.</div></li>
<li><div style="text-align: justify">Zaměřte se na něco praktického, co můžete dělat právě teď.</div></li>
<li><div style="text-align: justify">Používejte relaxační metody.</div></li>
<li><div style="text-align: justify">Účastněte se podpůrných skupin.</div></li>
<li><div style="text-align: justify">Můžete si psát deník.</div></li>
<li><div style="text-align: justify">Vyhledejte poradenské služby, pokud cítíte, že by to bylo vhodné.</div></li>
<li><div style="text-align: justify">Vyhněte se alkoholu, zneužívání látek, vzteku a obviňování, návykovému chování, extrémnímu stranění se a izolaci od ostatních a riskantnímu chování.</div></li>
</ul>
""";
const html_article_3_7_7 = """<ul>
<li><div style="text-align: justify">Učte jednoduché relaxační techniky.</div></li>
</ul>
""";
const html_article_3_7_8 =
    """<div style="text-align: justify"><p>Učení jednoduchých relaxačních technik:</p>
<ul>
<li><div style="text-align: justify">Dechová cvičení mohou pomoci mírnit pocity stresu a napětí.</div></li>
<li><div style="text-align: justify">Pravidelné cvičení může zlepšit spánek, stravování a psychický stav.</div></li>
<li><div style="text-align: justify"><strong>Projděte se zasaženými následující kroky:</strong><ul>
<li><div style="text-align: justify">Pomalu se nadechněte (tisíc jedna, tisíc dva, tisíc tři) skrz váš nos a pohodlně naplňte své plíce a nadechněte se až do břicha.</div></li>
<li><div style="text-align: justify">Tiše a jemně řeknete sami sobě: „Mé tělo je naplněno klidem.“ </div></li>
<li><div style="text-align: justify">Pomalu vydechněte (tisíc jedna, tisíc dva, tisíc tři) skrz vaše ústa  všechen vzduch z plic.</div></li>
<li><div style="text-align: justify">Tiše a jemně řekněte sami sobě: „Mé tělo se zbavuje napětí.“</div></li>
<li><div style="text-align: justify">Tohle pětkrát pomalu a pohodlně zopakujte.</div></li>
<li><div style="text-align: justify">Toto cvičení provádějte tolikrát denně, kolikrát je potřeba.</div></li>
</ul>
</div></li>
</ul>
""";
const html_article_3_7_9 = """<ul>
<li><div style="text-align: justify">Pomozte rodinám se s událostí vypořádat.</div></li>
</ul>
""";
const html_article_3_7_10 =
    """<div style="text-align: justify"><p>Pomoc rodinám při vypořádávání se s problémem:</p>
<ul>
<li><div style="text-align: justify">Vysvětlete, že každý člen rodiny bude mít rozdílné reakce a cestu k zotavení. Podpořte je, aby byli trpěliví, chápající a respektující k těmto rozdílům.</div></li>
<li><div style="text-align: justify">Udržujte zaběhlé zvyklosti v rodině  (čas jídla, čas jít spát, čas probouzení, čas čtení a čas na hraní).</div></li>
<li><div style="text-align: justify">Upevněte roli rodičů a zodpovězte otázky, které mají ohledně svých dětí.</div></li>
<li><div style="text-align: justify">Podpořte členy rodiny v aktivitách, které jim mohou pomoci lépe pochopit druhého, a tím ho i podpořit.</div></li>
</ul>
""";
const html_article_3_7_11 = """<ul>
<li><div style="text-align: justify">Pracujte se hněvem.</div></li>
</ul>
""";
const html_article_3_7_12 =
    """<div style="text-align: justify"><p>Řešení problémů s hněvem:</p>
<ul>
<li><div style="text-align: justify">Zeptejte se, co člověka trápí.</div></li>
<li><div style="text-align: justify">Vysvětlete, že rozumíte, proč jsou naštvaní, a že pocity hněvu a frustrace jsou po katastrofě běžné.</div></li>
<li><div style="text-align: justify">Požádejte zasažené, aby určili cíle, kterých chtějí dosáhnout.</div></li>
<li><div style="text-align: justify">Pomozte jim porozumět, že zadržování vzteku je může zranit a že jim zvládnutí vzteku může pomoci dosáhnout jejich cílů.</div></li>
<li><div style="text-align: justify">Některé způsoby ovládnutí vzteku zahrnují:<ul>
<li><div style="text-align: justify">Vezměte si „time-out“ nebo „vychladněte“ (odejděte a zklidněte se).</div></li>
<li><div style="text-align: justify">Promluvte si s kamarádem/kamarádkou o tom, co vás rozčiluje.</div></li>
<li><div style="text-align: justify">Upusťte páru fyzickým cvičením.</div></li>
<li><div style="text-align: justify">Pište si deník.</div></li>
<li><div style="text-align: justify">Připomeňte si, že nepřiměřený vztek vám nepomůže dosáhnout vašich cílů a že může poškodit důležité vztahy.</div></li>
<li><div style="text-align: justify">Rozptylte se pozitivními aktivitami.</div></li>
<li><div style="text-align: justify">Zkuste se podívat na svou situaci v jiném světle.</div></li>
<li><div style="text-align: justify">Rodičům/opatrovatelům: mějte po ruce nějakou dospělou osobu na dočasný dohled nad aktivitami vašich dětí, zejména když se cítíte naštvaně nebo podrážděně.</div></li>
<li><div style="text-align: justify">Navrhněte dětem nebo dospívajícím, aby se zapojili do aktivit, které jim mohou pomoci vyjádřit jejich pocity, jako je kreslení obrázků, psaní do deníku, sehrávání situací s hračkami a skládání písniček.</div></li>
<li><div style="text-align: justify">Pomozte dětem a dospívajícím přehrát si situaci, která je štve nebo deptá.</div></li>
</ul>
</div></li>
<li><div style="text-align: justify">Pokud se agresivní projevy stanou nekontrolovatelnými nebo jsou násilné, vyhledejte odbornou pomoc, případně kontaktujte policii.</div></li>
</ul>
""";
const html_article_3_7_13 = """<ul>
<li><div style="text-align: justify">Věnujte se vysoce negativním emocím.</div></li>
</ul>
""";
const html_article_3_7_14 =
    """<div style="text-align: justify"><p>Práce s negativními emocemi (sebeobviňování, pocit viny)</p>
<ul>
<li><div style="text-align: justify">Postupujte následovně:<ul>
<li><div style="text-align: justify">Pomozte zasaženým vidět situaci z jiného úhlu pohledu.</div></li>
<li><div style="text-align: justify">Jaké jsou další způsoby, jak o situaci uvažovat?</div></li>
<li><div style="text-align: justify">Jak by pravděpodobně reagovali, kdyby jejich dobrý/á kamarád/kamarádka o sobě takto mluvili? Co by mu/jí řekli? Můžou říct to samé sami sobě?</div></li>
</ul>
</div></li>
<li><div style="text-align: justify">Pomozte objasnit nedorozumění, fámy a zkreslení, které zvyšují tíseň, neodůvodněný pocit viny nebo studu.</div></li>
<li><div style="text-align: justify">Děti a dospívající mají tendenci obviňovat sami sebe. Připomeňte jim, že nejsou na vině, a to i v případě, že o tom nemluví.</div></li>
<li><div style="text-align: justify">I když cítí pocit viny, nemusí to znamenat, že situaci zavinili.</div></li>
<li><div style="text-align: justify">Při události, kdy je osoba nějakým způsobem zodpovědná a vyrovnává se s pocitem viny, by měla být odkázána na odborné poradenství. Tyto situace už jdou mimo rámec krátkodobých zásahů, jako je PPP.</div></li>
</ul>
""";
const html_article_3_7_15 = """<ul>
<li><div style="text-align: justify">Pomozte řešit problémy se spánkem.</div></li>
</ul>
""";
const html_article_3_7_16 =
    """<div style="text-align: justify"><p>Podporujte následující zdravé návyky:</p>
<ul>
<li><div style="text-align: justify">Choďte spát a vstávejte každý den ve stejný čas.</div></li>
<li><div style="text-align: justify">Omezte požívání alkoholu, alkohol narušuje spánek.</div></li>
<li><div style="text-align: justify">Odpoledne a večer omezte požívání kofeinových nápojů.</div></li>
<li><div style="text-align: justify">Zvyšte pravidelné cvičení, ale ne těsně před spaním.</div></li>
<li><div style="text-align: justify">Odpočiňte si před tím, než jdete spát, dělejte něco uklidňujícího.</div></li>
<li><div style="text-align: justify">Omezte zdřímnutí přes den na 15 minut a omezte zdřímnutí po 16. hodině.</div></li>
<li><div style="text-align: justify">Poraďte, že sdílet obavy a dostat od ostatních podporu může spánek v průběhu času zlepšit.</div></li>
<li><div style="text-align: justify">Připomeňte rodičům, že je běžné, když děti chtějí v noci zůstat rodičům nablízku a dokonce s nimi chtějí spát v posteli. Dočasné změny ve spacích návycích jsou v pořádku, pokud se rodiče s dětmi dohodnou, že se po čase navrátí k běžným spacím návykům.</div></li>
</ul>
""";
const html_article_3_7_17 = """<ul>
<li><div style="text-align: justify">Řešte užívání alkoholu a návykových látek.</div></li>
</ul>
""";
const html_article_3_7_18 =
    """<div style="text-align: justify"><p>Řešení užívání alkoholu a návykových látek:</p>
<ul>
<li><div style="text-align: justify">Proberte se zasaženými, jaká jsou pozitiva a negativa v užívání alkoholu a drog pro vyrovnání se s danou situací.</div></li>
<li><div style="text-align: justify">Snažte se je motivovat, aby tyto strategie opustili. Navrhněte jiné možnosti.</div></li>
</ul>
""";

const html_article_4_0 =
    """<div style="text-align: justify"><p>Základní potřeby lidí se zdravotním postižením či komunikačními odlišnostmi při mimořádných událostech jsou ve velké míře stejné jako u všech ostatních lidí. Odlišný je ale způsob jejich vyjadřování a naplňování.</p>""";

const html_article_4_1 =
    """<div style="text-align: justify"><p>Zrakové postižení má mnoho podob. Může jít o lidi nevidomé od narození, osleplé později, osoby s poruchami zraku následkem nehody, lidi, kteří při nehodě ztratili brýle, slabozraké, jedince se zbytky zraku různého charakteru nebo o velmi staré lidi s fyziologickou ztrátou zraku. Charakter potřebné pomoci se u těchto osob může odlišovat, stejně jako jejich schopnosti vnímání, orientace a sebeobsluhy. Psychický rozvoj lidí se zrakovým postižením je totožný s rozvojem u vidících lidí (dochází ke shodnému vývoji myšlení, emocí i sociálních schopností).</p></div>
<h2>Zásady pro krizovou asistenci a komunikaci s nevidomými</h2>
<ul>
    <li><div style="text-align: justify">Postupujeme podle zásad první psychické pomoci.</div></li>
    <li><div style="text-align: justify">Dbáme na zachování lidské důstojnosti a úcty nevidomého.</div></li>
    <li><div style="text-align: justify">Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám.</div></li>
    <li><div style="text-align: justify">Podporujeme vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence.</div></li>
    <li><div style="text-align: justify">Zeptáme se této osoby, zda a jak vidí. Přizpůsobíme se jejím schopnostem.</div></li>
    <li><div style="text-align: justify">Při evakuaci ponecháváme potřebné kompenzační pomůcky (mobily, čtecí lupy, notebooky, diktafon apod.)</div></li>
    <li><div style="text-align: justify">Neoddělujeme nevidomého od vodícího psa. Vodícího psa evakuujeme společně s nevidomým. Na vodícího psa nevoláme, nehladíme ho, nekrmíme, nerozptylujeme.</div></li>
    <li><div style="text-align: justify">S věcmi nevidomého nebo slabozrakého nemanipulujeme bez jeho vědomí (má prostorovou představu, kam co dal). Při změnách přemístění věcí vždy jedince informujeme.</div></li>
    <li><div style="text-align: justify">Nelitujeme, respektujeme odlišnosti ve vnímání a komunikaci.</div></li>
    <li><div style="text-align: justify">Při komunikaci využíváme všechny způsoby vnímání (sluch, hmat, zbytky zraku).</div></li>
    <li><div style="text-align: justify">Při oslovení nevidomého se lehce dotkneme jeho paže (upoutáme pozornost). Oslovujeme jej jménem. Důležité je se představit a říci svou roli a úkoly. Podání ruky nahrazuje vizuální kontakt, při představení řekneme „Podávám vám ruku“. </div></li>
    <li><div style="text-align: justify">Mluvíme přímo na nevidomého, nikoliv na jeho průvodce. (Nevidí, ale rozhoduje za sebe sám.) Pokud je to možné, udržujte oční kontakt s osobou s poruchou zraku, pokud mluvíme bokem, pozná to.</div></li>
    <li><div style="text-align: justify">Při komunikaci bez obav používáme obraty jako: „vidět, kouknout se, mrknout, prohlédnout si“. </div></li>
    <li><div style="text-align: justify">Zjistíme, zda má kompenzační pomůcky.</div></li>
    <li><div style="text-align: justify">Při vedení nevidomého do neznámých prostor slovně popisujeme okolí, sanitku, místnost pro ukrytí a její vybavení. Ukážeme, kde se nachází toaleta, splachovadlo, papír, umyvadlo. Ujistíme se, že je toaleta čistá.
    </div></li>
    <li><div style="text-align: justify">Aktivně informujeme o situaci. Popisujeme viděné aspekty. Vysvětlujeme postupy.</div></li>
    <li><div style="text-align: justify">Ptáme se, zda je osoba připravena na úkon (např. aplikace injekce, ošetření).</div></li>
    <li><div style="text-align: justify">Nezbytné jsou informace o tom, co bude (např. „Teď to bude tlačit, teď to bude pálit“).</div></li>
    <li><div style="text-align: justify">Pozor na emotivní zvolání bez vysvětlení (např. „ježišmarja“), mohou zmást a zvyšovat úzkost.</div></li>
    <li><div style="text-align: justify">Nepoužíváme výrazy „támhle“, „tady“, ale spíše „hned za vámi“, „dva metry před vámi“ apod.</div></li>
    <li><div style="text-align: justify">Neznámé pomůcky a vybavení necháme nevidomého osahat (vidí hmatem), vše vysvětlíme a popíšeme.</div></li>
    <li><div style="text-align: justify">Veškeré činnosti a úkony slovně popisujeme (např.: „Tady je dýchací maska, sáhněte na ni, teď vám ji nasadím na hlavu…Kdyby vás to tlačilo, řekněte…“). </div></li>
    <li><div style="text-align: justify">V dýchací technice nemusí být záchranáře slyšet. Aby nás nevidomý rozpoznal, přiložíme jeho ruce na helmu apod. Pomocí rukou naznačujeme úmysl, např. nasadit vyváděcí masku.</div></li>
    <li><div style="text-align: justify">V neznámém prostoru přesně popisujeme (únikovou) cestu a případná nebezpečí. Kde není volný průchod pozor, na překážky a předměty na zemi, na předměty ve výšce hlavy (lešení, trosky, dopisní schránku na zdi domu, změny v povrchu chodníku apod.). Např. „Tři kroky dopředu, teď sehněte hlavu, je tam tyč. Teď velký krok, teď jdu před vás… Teď úkrok doleva, před námi leží prkna, a už můžeme jít rovně“. </div></li>
    <li><div style="text-align: justify">Při doprovázení se domluvíme na jeho způsobu. Při vedení např. nabídneme paži a necháme nevidomého se zavěsit – zpravidla se lehce dotýká našeho lokte či spodní hrany předloktí, to jej vede. Průvodce je zpravidla vpředu. Nevidomého nikdy netlačíme před sebou ani netáhneme za ruku za sebou. U vozidla jej stavíme k otevřeným dveřím nebo dáváme ruku na kliku. Při nástupu do vozidla upozorníme na výšku podlahy a položíme jeho ruku na horní hranu dveří.</div></li>
    <li><div style="text-align: justify">Při procházení úzkým průchodem jde průvodce jako první.</div></li>
    <li><div style="text-align: justify">Při usazení nevidomého položíme jeho ruku na opěradlo, necháme ho se samostatně posadit. Nestrkáme ho, nenecháme ho tápat v prostoru.</div></li>
    <li><div style="text-align: justify">Na podávaný předmět položíme jeho ruku, popisujeme, kde a co leží.</div></li>
    <li><div style="text-align: justify">Při chůzi po schodech dáváme ruku nevidomého na zábradlí, druhou rukou se do nás může zavěsit. Informujeme o směru chůze nahoru/dolů. Upozorníme na první a poslední schod. Upozorníme na obrubník chodníku či překážky. Upozorníme na věci na stole, které by mohl shodit.</div></li>
    <li><div style="text-align: justify">Nevidomého neopouštíme bez upozornění, pokud se potřebujeme vzdálit, zanecháme ho u pevné opory a informujeme ho, na jak dlouho odcházíme (např. „Potřebuji na chvíli odejít, tady je zábradlí, počkejte tu na mně, za 5 minut se vrátím.“). </div></li>
    <li><div style="text-align: justify">Nikdy nechodíme za nevidomým jako „hlídač“, bez komunikace či nabídky pomoci.</div></li>
</ul>""";
const html_article_4_2 =
    """<div style="text-align: justify"><p>Sluchové postižení má mnoho podob. Může jít o lidi neslyšící od narození, ohluchlé až po rozvoji řeči, osoby se zbytky sluchu, ohluchlé ve stáří, lidi se ztrátou sluchu dočasnou po úraze, lidi, kteří ztratili při nehodě naslouchadla apod. Pouze někteří sluchově postižení ovládají znakový jazyk, někteří umí odezírat, někteří používají kompenzační pomůcky (naslouchadla, kochleární implantát, přepis řeči). Někteří ohluchlí mohou žít po dlouhou dobu v izolaci. Charakter potřebné pomoci se u těchto lidí může odlišovat, stejně jako jejich schopnosti vnímání, orientace a sebeobsluhy. Psychický rozvoj lidí ohluchlých před rozvojem řeči se může lišit od průměrné populace (méně je rozvinuta abstraktní složka myšlení, odlišný je smysl pro humor, odlišnosti mohou být v chápání sociálních interakcí).<br>Velmi specifická situace je u lidí hluchoslepých nebo u lidí s částečnou ztrátou jak zraku, tak sluchu.</p></div>
<h2>Zásady pro krizovou asistenci a komunikaci se sluchovým postižením</h2>
<ul>
    <li><div style="text-align: justify">Postupujeme podle zásad první psychické pomoci.</div></li>
    <li><div style="text-align: justify">Ke stabilizaci osob pomáhá dodávání informací (včasných, přiměřených a pravdivých) a komunikace s blízkými. Zprostředkujeme kontakt s blízkou osobou a případně s tlumočníkem znakového jazyka.</div></li>
    <li><div style="text-align: justify">Při komunikaci s člověkem se sluchovým postižením vždy dbáme na zachování jeho lidské důstojnosti a úcty.</div></li>
    <li><div style="text-align: justify">Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám.</div></li>
    <li><div style="text-align: justify">Usilujeme o vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence.</div></li>
    <li><div style="text-align: justify">Zeptáme se, zda a jak slyší. Přizpůsobíme se jeho schopnostem a komunikačním možnostem (tlumočník, zajištění přepisu slov do textu, bloky a tužky, kreslení obrázků).</div></li>
    <li><div style="text-align: justify">Při varování používáme jiné komunikační cesty, než je jen zvuk.</div></li>
    <li><div style="text-align: justify">Při evakuaci ponecháváme potřebné kompenzační pomůcky (naslouchadla, kochleární implantát, mobily, notebooky apod.).</div></li>
    <li><div style="text-align: justify">Nelitujeme, respektujeme odlišnosti ve vnímání a komunikaci.</div></li>
    <li><div style="text-align: justify">Při komunikaci využíváme všechny způsoby vnímání (zrak, hmat, zbytky sluchu).</div></li>
    <li><div style="text-align: justify">Při oslovení neslyšícího se lehce dotkneme jeho paže (nebo jinak upoutáme jeho pozornost).</div></li>
    <li><div style="text-align: justify">Na neslyšícího mluvíme tváří v tvář, na nedoslýchavého z té strany, kde má zachovány zbytky sluchu.</div></li>
    <li><div style="text-align: justify">Používáme krátké věty. Zřetelně artikulujeme, nestoupáme si proti světlu. Ne všichni neslyšící umí dobře odezírat. Nekřičíme. Ověříme si, zda nám osoba správně porozuměla.</div></li>
    <li><div style="text-align: justify">Používáme všechny způsoby komunikace (gesta, posunky, psaní na papír, předtištěné obrázky, piktogramy, předtištěné věty, textové zprávy, notebook, překladač v mobilu, znaky na kartičkách, některá slova znakového jazyka), např.: „Jsem zdravotník. Jste zraněn? Bolí vás něco? Ukažte na obrázku, kde vás bolí…“ Pokud není k dispozici tlumočník, hlavní je tužka a papír. Postup a jiné informace píšeme v jednoduchých větách na papír.
    </div></li>
    <li><div style="text-align: justify">I když neumíme znakový jazyk, můžeme gesta použít intuitivně (pantomima). Demonstrujeme např. nasazení pomůcek na sobě. Naznačíme následný zdravotnický úkon.</div></li>
    <li><div style="text-align: justify">Pokud to lze, zajistíme tlumočníka (lze tlumočit např. přes aplikaci Tichá Linka, nebo přes Skype, případně použít on-line službu <a href="http://www.appn.cz">www.appn.cz</a>, <a href="http://www.tkcn.cz">www.tkcn.cz</a>).</div></li>
    <li><div style="text-align: justify">Mluvíme vždy přímo na neslyšícího, nikoliv na jeho tlumočníka. (Neslyší, ale rozhoduje za sebe sám.) Tlumočník stojí vedle nás čelem k neslyšícímu.</div></li>
    <li><div style="text-align: justify">Aktivně informujeme o situaci. Vysvětlujeme postupy. Veškeré činnosti a úkony neverbálně naznačujeme nebo píšeme.</div></li>
    <li><div style="text-align: justify">Neptáme se, zda porozuměl, ale raději „Co jste mi rozuměl?“.</div></li>
    <li><div style="text-align: justify">Pozor neexistuje univerzální znakový jazyk, každý národ má vlastní.</div></li>
    <li><div style="text-align: justify">Velkou úzkost může způsobit u neslyšícího nemožnost použít ke komunikaci ruce (zranění, spoutání).</div></li>
    <li><div style="text-align: justify">Pozor na nebezpečné situace, kdy neslyšící nemá zvukové informace - například o vozidlech či strojích mimo zorný úhel.</div></li>
</ul>""";

const html_article_4_3 =
    """<div style="text-align: justify"><p>Psychické potíže mají mnoho podob. Může jít o lidi s vrozeným postižením intelektu různé hloubky, osoby se stařeckou demencí, osoby trpící posttraumatickou stresovou poruchou, hlubokou depresí, fobiemi,intoxikované osoby, osoby s potížemi autistického spektra či trpící psychózou apod. Rozumové schopnosti, sociální schopnosti a emoční projevy mohou být u jednotlivců velmi individuální i proměnlivé. Mimořádná událost může u některých lidí vést k dekompenzaci psychického stavu, jiní dovedou jednat velmi racionálně a organizovaně. U některých lidí dochází ke kombinacím psychického a fyzického či smyslového postižení. Charakter potřebné pomoci se u těchto lidí může odlišovat, stejně jako jejich schopnosti myšlení, vnímání, orientace a sebeobsluhy. Mnohdy pomůže větší trpělivost, vnímavost a ohleduplný přístup.</p></div>
<h2>Zásady pro krizovou asistenci a komunikaci s lidmi s psychickými potížemi</h2>
<ul>
    <li><div style="text-align: justify">Postupujeme podle zásad první psychické pomoci.</div></li>
    <li><div style="text-align: justify">Při komunikaci s člověkem s psychickými potížemi vždy dbáme na zachování jeho lidské důstojnosti a úcty.</div></li>
    <li><div style="text-align: justify">Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám.</div></li>
    <li><div style="text-align: justify">Usilujeme o vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence.</div></li>
    <li><div style="text-align: justify">Přizpůsobíme se jeho schopnostem a komunikačním možnostem.</div></li>
    <li><div style="text-align: justify">Nelitujeme, respektujeme odlišnosti ve vnímání, myšlení a komunikaci.</div></li>
    <li><div style="text-align: justify">Bereme na vědomí, že se může jednat o lidi s postižením intelektu v různé intenzitě, jsou individualitami. Pomaleji chápou. Na změny potřebují více času. Někteří nemluví nebo mají další zdravotní problémy. Někteří mohou dobře rozumět, jiní mohou být mentálně na úrovni velmi malého dítěte.</div></li>
    <li><div style="text-align: justify">Zásah do běžných stereotypů je může rozrušit, proto jednejme klidně, citlivě a trpělivě. Neznámé situace a náhlé změny prožívají většinou velmi citlivě.</div></li>
    <li><div style="text-align: justify">Podobně jako u dětí je mohou uniformy a pomůcky vystrašit.</div></li>
    <li><div style="text-align: justify">Potřebují klidné a neměnné prostředí (ať už v rodině nebo pobytovém zařízení). Vlivem stresu se mohou i ve známém prostředí pohybovat zmateně. Proto je vždy doprovázíme nebo i vedeme za ruku.</div></li>
    <li><div style="text-align: justify">Pro zvýšení pocitu bezpečí vedeme osoby s mentálním postižením pokud možno pohromadě. Neoddělujeme je od známých osob.</div></li>
    <li><div style="text-align: justify">Při mimořádných situacích mohou být vystrašení a zmatení.</div></li>
    <li><div style="text-align: justify">Mluvme na ně pomalu a jednoduše. Ty co nemluví, chytněme za ruku.</div></li>
    <li><div style="text-align: justify">K uklidnění může přispět vzít si sebou některé své osobní předměty nebo hračku.</div></li>
    <li><div style="text-align: justify">Mentálně postižení lidé nemusí vždy rozumět našim informacím a pokynům, pak se mohou chovat neočekávaným způsobem. Vždy si ověříme, zda nám porozuměli a pochopili, co mají dělat.</div></li>
    <li><div style="text-align: justify">Mentální postižení může být kombinováno s postižením tělesným a smyslovým. Je třeba i toto zohledňovat.</div></li>
    <li><div style="text-align: justify">Pokud je musíme evakuovat do neznámých prostor, ujišťujeme je zvýšeně, že budou v bezpečí a že na novém místě budou mít vše, co potřebují. Nápodoba hraje roli.</div></li>
    <li><div style="text-align: justify">Pomáhá fyzická opora, dotyk, jednoduché vysvětlování, vlídný přístup, pozitivní emoce.</div></li>
    <li><div style="text-align: justify">Mluvme v krátkých jednoduchých větách a konkrétně, ale není vždy vhodné mluvit s těmito lidmi jako s malými dětmi.</div></li>
    <li><div style="text-align: justify">Nepoužíváme odborné termíny a abstraktní pojmy. Tito lidé většinou nerozumí ironii, nadsázce a dvojsmyslům.</div></li>
    <li><div style="text-align: justify">I když se někteří nedovedou vyjadřovat slovy, mluvené řeči alespoň částečně rozumí. Dobře reagují na neverbální komunikaci.</div></li>
    <li><div style="text-align: justify">Využívejme přívětivé a povzbudivé mimiky, znázorňujících gest a pohybů.</div></li>
    <li><div style="text-align: justify">Zvýšeně dodáváme pocit bezpečí, nenecháváme je samotné, udržujeme s nimi oční kontakt.</div></li>
</ul>
<div style="text-align: justify"><p>Desatero komunikace s pacienty s poruchou autistického spektra (vytvořeno organizací APLA – JM).</p>
""";

const html_article_4_3_1 = """
<ul>
<li><div style="text-align: justify">Autismus není patrný na první pohled, pozná se až podle nestandardního chování pacienta, které je potřeba tolerovat. Ačkoli se může na první pohled zdát, že je pacient rozmazlený, zlobivý či vzpurný, ve skutečnosti má jen  nepřekonatelný strach z neznámého prostředí a z nové situace.</div></li>
<li><div style="text-align: justify">Důležitá je spolupráce s tlumočníkem, který pacienta s autismem vždy doprovází. Právě ten zná všechny důležité informace o konkrétním pacientovi, proto je dobré jeho rady a doporučení vhodného jednání s pacientem respektovat.</div></li>
<li><div style="text-align: justify">Jednoduchá uklidňující sdělení v krátkých větách jsou tou nejlepší formou slovní komunikace.</div></li>
<li><div style="text-align: justify">Je běžné, že pacienti s autismem jsou smyslově přecitlivělí. Proto je třeba se připravit na zvýšenou citlivost na zvuky, tóny, světlo, pachy či dotek neznámé osoby.</div></li>
<li><div style="text-align: justify">Při lékařském vyšetření je vhodné objednat pacienta na konkrétní čas. Nejlepší je zvát pacienta s PAS na úplném začátku či konci ordinačních hodin, kdy je v čekárně jen minimum lidí a vždy ho brát přednostně před ostatními pacienty.</div></li>
<li><div style="text-align: justify">Je třeba tolerovat pacientův odmítavý přístup k lékařskému vyšetření. Snaha zlomit tento negativismus není vhodná a většinou situaci jen zhorší.</div></li>
<li><div style="text-align: justify">Při všech vyšetřeních a úkonech je nezbytná přítomnost doprovodu. Ten je v dané situaci jeho jedinou jistotou, oporou i tlumočníkem s okolím.</div></li>
<li><div style="text-align: justify">K úspěšnému lékařskému ošetření není vždy potřeba narkóza. Je dobré zvážit, zda k použití tlumících prostředků neexistuje jednodušší a méně radikální alternativa.</div></li>
<li><div style="text-align: justify">Je vhodné zvážit, zda jsou všechna vyšetření nezbytně nutná. Na některých tradičních lékařských postupech není potřeba vždy trvat.</div></li>
<li><div style="text-align: justify">Hospitalizace pacienta by také měla být důkladně zvážena. Pokud je nezbytná, je velice vhodné absolvovat pobyt v nemocnici s rodičem či jinou blízkou osobou, na kterou je pacient vázán. Je to dobré nejen pro pacienta, ale i pro ošetřující personál.</div></li>
</ul>
""";
const html_article_4_4 =
    """<div style="text-align: justify"><p>Pohybová postižení mají mnoho podob. Může jít o lidi s vrozeným postižením motoriky končetin a trupu, osoby s postižením pohybu po úrazu, lidi s omezenou hybností danou věkem, lidi s různou úrovní sebeobsluhy a různými kompenzačními prostředky (protézy, berle, chodítka, invalidní vozík). U některých se může objevovat současné postižení mluvidel a mimických svalů. Jen u některých lidí dochází ke kombinacím psychického a fyzického či smyslového postižení.Charakter potřebné pomoci se u těchto lidí odlišuje, stejně jako jejich schopnosti sebeobsluhy. Mnohdy pomůže zeptat se daného jedince, jak napomoci při transportu a jaké jsou jeho specifické potřeby.</p></div>
<h2">Zásady pro krizovou asistenci a komunikaci s lidmi s pohybovým postižením</h2>
    <ul>
        <li><div style="text-align: justify">Postupujeme podle zásad první psychické pomoci.</div></li>
        <li><div style="text-align: justify">Při komunikaci s člověkem s pohybovým postižením vždy dbáme na zachování jeho lidské důstojnosti a úcty.</div></li>
        <li><div style="text-align: justify">Nabízíme pomoc, zeptáme se, v čem můžeme pomoci a co může udělat sám.</div></li>
        <li><div style="text-align: justify">Usilujeme o vlastní aktivitu jednotlivce, nebereme mu jeho vlastní kompetence.</div></li>
        <li><div style="text-align: justify">Přizpůsobíme se jeho schopnostem.</div></li>
        <li><div style="text-align: justify">Nelitujeme, respektujeme odlišnosti v pohybu. Často se se svým postižením již naučili žít.</div></li>
        <li><div style="text-align: justify">Přistupujeme k jednotlivci jako k jakékoli jiné osobě.</div></li>
        <li><div style="text-align: justify">U některých tělesných postižení je zasažena i motorika mluvidel a obličeje. Tělesně postižený se může jevit jako postižený mentálně, i když není. Ověříme si stav postižení a vždy zachováváme jeho lidskou důstojnost.</div></li>
        <li><div style="text-align: justify">Ptáme se, co jedinec dokáže zvládnout sám a s čím můžeme pomoci.</div></li>
        <li><div style="text-align: justify">Necháme si od něj poradit, jak s ním zacházet.</div></li>
        <li><div style="text-align: justify">Pozor na zvýšenou lámavost kostí nebo svalovou ochablost – při nevhodném zacházení je riziko zranění.</div></li>
        <li><div style="text-align: justify">Někteří neudrží tělo bez opory.</div></li>
        <li><div style="text-align: justify">Při pomoci s vozíkem postupujeme podle rad vozíčkáře.</div></li>
        <li><div style="text-align: justify">Pokud nelze evakuovat po schodech, může být transport vozíčkáře prováděn na nosítkách. Některé osoby lze evakuovat vsedě, jiné jen vleže.</div></li>
        <li><div style="text-align: justify">Popisujeme situaci a vysvětlujeme kroky transportu.</div></li>
        <li><div style="text-align: justify">S osobou evakuujeme i její vozík či jiné kompenzační pomůcky.</div></li>
        <li><div style="text-align: justify">Pokud pomůcky chybí, můžeme využít improvizované transportní prostředky.</div></li>
        <li><div style="text-align: justify">Manipulace s vozíkem – složení/rozložení: zajistíme brzdy, zvedneme podnožku, tahem za látku sedadla vozík složíme. Tlakem na sedadlo rozložíme. Pozor na zranění prstů při manipulaci.</div></li>
        <li><div style="text-align: justify">Při transportu ze schodů: jedna osoba uchopí vozík za zadní madla a nakloní jej dozadu na velká kola, druhá osoba přidržuje vozík dole za pevnou část konstrukce u předních koleček. Lehkou osobu můžeme snést i s vozíkem.</div></li>
        <li><div style="text-align: justify">Podobně se postupuje při cestě do schodů (vozíčkář je vyvážen pozadu).</div></li>
        <li><div style="text-align: justify">Pokud má vozíčkář dostatečnou sílu v rukou, může mu na schodišti pomáhat jen jedna osoba. Před sjížděním/vyjížděním schodiště je nutné naklonit vozík na zadní kola, aby osoba nevypadla.</div></li>
        <li><div style="text-align: justify">Na nerovném, kamenitém či písčitém terénu vezeme vozík s osobou pozpátku. (Při tlačení dopředu se kola boří).</div></li>
        <li><div style="text-align: justify">Chceme-li osobu vysadit z vozíku, musíme se zeptat, jakým způsobem ji můžeme uchopit.</div></li>
        <li><div style="text-align: justify">Před usazením na vozík jej zabrzdíme a odkloníme postranní opěrku.</div></li>
        <li><div style="text-align: justify">Osobu používající berle nezvedáme ze země za ruce, ale uchopíme zezadu v pase.</div></li>
        <li><div style="text-align: justify">Při přenášení tělesně postiženého podpíráme jeho páteř, ne každý má dostatečnou sílu v rukou, aby se nás mohl přidržovat. Stále hlídáme polohu ochrnutých a nehybných končetin. (Nemusí sám cítit zavadění či zranění).</div></li>
        <li><div style="text-align: justify">Elektrické vozíky je lépe přenášet bez vozíčkáře (váží 100 kg a víc). Motory lze vzadu odpojit a vozík lze odtlačit mechanicky.</div></li>
        <li><div style="text-align: justify">Při přesunu z elektrického na mechanický vozík se odmontuje bočnice.</div></li>
        <li><div style="text-align: justify">Je třeba ověřit potřebnou šíři dveří WC.</div></li>
        <li><div style="text-align: justify">Při podávání stravy se informujeme, zda je schopen se najíst samostatně, nakrájet si jídlo apod.</div></li>
    </ul>""";
const html_article_4_5 =
    """<div style="text-align: justify"><p>Při práci s cizinci při mimořádných událostech je klíčovou kompetencí jazyková komunikace. Dále hrají roli také kulturní odlišnosti. Ty se týkají komunikace, stravování, odívání, ubytování, náboženských rituálů, nakládání se zemřelými, pohřebních potřeb a zvyklostí apod. Specifické potřeby mají také zástupci některých náboženských menšin. Pokud nemáme potřebné interkulturní znalosti, obrátíme se na zástupce daného zastupitelského úřadu či tlumočníka, nebo se i přímo ptáme zástupců zasažených osob, co je v jejich zemi důležité, co potřebují, na co nemáme zapomenout apod.</p></div>
""";
const html_article_5_1 = """<ul>
        <li><div style="text-align: justify">nevšímat si zasažených, chovat se, jako by neexistovali,</div></li>
        <li><div style="text-align: justify">spěchat, být netrpělivý, vyvíjet nátlak,</div></li>
        <li><div style="text-align: justify">v rozhovoru vyslýchat, kritizovat, soudit, kázat,</div></li>
        <li><div style="text-align: justify">lhát, překrucovat informace,</div></li>
        <li><div style="text-align: justify">vnucovat jim vlastní rady, zkušenosti,</div></li>
        <li><div style="text-align: justify">zlehčovat jejich ztrátu, prožitou událost,</div></li>
        <li><div style="text-align: justify">dávat planou útěchu a naději,</div></li>
        <li><div style="text-align: justify">slibovat, co nemůžeme splnit,</div></li>
        <li><div style="text-align: justify">neustále mluvit, zaplňovat ticho,</div></li>
        <li><div style="text-align: justify">dělat za zasažené to, co by mohli dělat sami,</div></li>
        <li><div style="text-align: justify">pomáhat, když se na to necítíme,</div></li>
        <li><div style="text-align: justify">užívat alkohol či drogy.</div></li>
        </ul>
        """;
const html_article_6_1 = """<h2>Reakce v chování dětí</h2>
<ul>
    <li><div style="text-align: justify">dítě se lepí na rodiče nebo jiné známé dospělé lidi</div></li>
    <li><div style="text-align: justify">bezmoc a pasivita</div></li>
    <li><div style="text-align: justify">znovu se počůrává nebo si znovu cucá palec</div></li>
    <li><div style="text-align: justify">bojí se tmy</div></li>
    <li><div style="text-align: justify">nechce spát samo</div></li>
    <li><div style="text-align: justify">zvýšeně pláče</div></li>
</ul>
<h2>Tělesné projevy u dětí</h2>
<ul>
    <li><div style="text-align: justify">ztráta chuti k jídlu</div></li>
    <li><div style="text-align: justify">bolesti bříška </div></li>
    <li><div style="text-align: justify">nevolnost</div></li>
    <li><div style="text-align: justify">problémy se spaním, noční můry</div></li>
    <li><div style="text-align: justify">potíže v řeči</div></li>
    <li><div style="text-align: justify">tiky</div></li>
</ul>
<h2>Reakce v prožívání dětí</h2>
<ul>
    <li><div style="text-align: justify">úzkost</div></li>
    <li><div style="text-align: justify">strach téměř ze všeho</div></li>
    <li><div style="text-align: justify">podrážděnost</div></li>
    <li><div style="text-align: justify">výbuchy zlosti</div></li>
    <li><div style="text-align: justify">smutek</div></li>
    <li><div style="text-align: justify">staženost, ponoření se do sebe</div></li>
</ul>
<h2>Možnost postupu</h2>
<ul>
    <li><div style="text-align: justify">slovně ujišťujte a tělesně utišujte, pochovejte</div></li>
    <li><div style="text-align: justify">opakovaně objasňujte neporozumění a mylné představy</div></li>
    <li><div style="text-align: justify">při ukládání do postýlky se mazlete</div></li>
    <li><div style="text-align: justify">pomáhejte pojmenovávat emoce</div></li>
    <li><div style="text-align: justify">vyhněte se zbytečným odloučením</div></li>
    <li><div style="text-align: justify">dočasně dovolte dítěti, aby spalo v ložnici rodičů</div></li>
    <li><div style="text-align: justify">připomínky události zbavujte tajemna a dohadů</div></li>
    <li><div style="text-align: justify">povzbuzujte projevy týkající se ztrát (např. smrti včetně smrti domácích zvířat, ztrát hraček)</div></li>
    <li><div style="text-align: justify">hlídejte, nakolik je dítě vystaveno působení médií</div></li>
    <li><div style="text-align: justify">povzbuzujte vyjádření prostřednictvím hry</div></li>
</ul>
<div style="text-align: justify"><div style="font-size: small">
Dle <span style="text-decoration: underline"><a href="https://permanent.fdlp.gov/lps79620/SMA05-4025.pdf">U.S. Department of Health and Human Services. Mental Health Response to Mass Violence and Terrorism: A Field Guide. (DHHS Pub. No. SMA 4025. Rockville, MD: Center for Mental Health Services, Substance Abuse and Mental Health Services Administration, 2005)</a></span></div></div>""";

const html_article_6_2 = """<h2>Reakce v chování dětí</h2>
<ul>
    <li><div style="text-align: justify">zhoršil/a se v učení </div></li>
    <li><div style="text-align: justify">chodí za školu</div></li>
    <li><div style="text-align: justify">doma nebo ve škole druhé napadá </div></li>
    <li><div style="text-align: justify">chová se přespříliš aktivně, nebo otupěle - hloupě </div></li>
    <li><div style="text-align: justify">kňourá, lepí se, chová se jako menší dítě</div></li>
    <li><div style="text-align: justify">častěji soupeří s mladšími sourozenci o pozornost rodičů </div></li>
    <li><div style="text-align: justify">přehrávání traumatu, hra na traumatické téma</div></li>
</ul>
<h2>Tělesné projevy u dětí</h2>
<ul>
    <li><div style="text-align: justify">změny chuti k jídlu </div></li>
    <li><div style="text-align: justify">bolesti hlavy</div></li>
    <li><div style="text-align: justify">bolesti břicha </div></li>
    <li><div style="text-align: justify">poruchy spánku, noční můry </div></li>
    <li><div style="text-align: justify">tělesné stesky</div></li>
</ul>
<h2>Reakce v prožívání dětí</h2>
<ul>
    <li><div style="text-align: justify">strach z pocitů </div></li>
    <li><div style="text-align: justify">odvracení se od přátel a obvyklých činností </div></li>
    <li><div style="text-align: justify">připomínky události vyvolávají strach a obavy</div></li>
    <li><div style="text-align: justify">výbuchy zlosti</div></li>
    <li><div style="text-align: justify">zaujetí zločinem a zločinci, bezpečím a smrtí </div></li>
    <li><div style="text-align: justify">výčitky svědomí</div></li>
    <li><div style="text-align: justify">vina</div></li>
</ul>
<h2>Možnosti postupu</h2>
<ul>
    <li><div style="text-align: justify">věnujte dítěti více pozornosti a zájmu </div></li>
    <li><div style="text-align: justify">dočasně zmírněte požadavky na výkon ve škole i doma</div></li>
    <li><div style="text-align: justify">vymezte citlivě ale pevně hranice pro předvádění se a podobné chování</div></li>
    <li><div style="text-align: justify">zaměstnávejte dítě činnostmi dobrými pro rehabilitaci a obnovu a strukturujte mu čas nenáročnými domácími pracemi</div></li>
    <li><div style="text-align: justify">povzbuzujte vyjadřování pocitů a myšlenek slovem i hrou</div></li>
    <li><div style="text-align: justify">naslouchejte, když bude dítě opakovaně vyprávět o traumatizující události </div></li>
    <li><div style="text-align: justify">objasňujte dítěti jeho mylné představy a zkreslení</div></li>
    <li><div style="text-align: justify">rozpoznejte připomínky události a pomáhejte mu s nimi </div></li>
    <li><div style="text-align: justify">vystavte školní program zaměřený na vrstevnickou oporu, na činnosti, při nichž je možné se projevit a vyjádřit, na osvětu o traumatu a zločinu, na krizovou připravenost a plánování, na vytipování ohrožených dětí</div></li>
</ul>
<div style="text-align: justify"><div style="font-size: small">
Dle <span style="text-decoration: underline"><a href="https://permanent.fdlp.gov/lps79620/SMA05-4025.pdf">U.S. Department of Health and Human Services. Mental Health Response to Mass Violence and Terrorism: A Field Guide. (DHHS Pub. No. SMA 4025. Rockville, MD: Center for Mental Health Services, Substance Abuse and Mental Health Services Administration, 2005)</a></span></div></div>""";
const html_article_6_3 = """<h2>Reakce v chování dospívajících</h2>
<ul>
    <li><div style="text-align: justify">zhoršil/a se v učení </div></li>
    <li><div style="text-align: justify">doma nebo ve škole se chová vzdorovitě, revoltuje </div></li>
    <li><div style="text-align: justify">znovu se chová nezodpovědně, ač už tomu bylo jinak </div></li>
    <li><div style="text-align: justify">chová se neklidně nebo apaticky, projevuje úbytek energie</div></li>
    <li><div style="text-align: justify">dostává se do rozporu se zákonem</div></li>
    <li><div style="text-align: justify">hazarduje</div></li>
    <li><div style="text-align: justify">společensky se stahuje, uzavírá do sebe</div></li>
    <li><div style="text-align: justify">prudce mění vztahy a prudce se mění ve vztazích</div></li>
    <li><div style="text-align: justify">pije alkohol nebo užívá nezákonné drogy</div></li>
</ul>
<h2>Tělesné projevy u dospívajících</h2>
<ul>
    <li><div style="text-align: justify">změny chuti k jídlu </div></li>
    <li><div style="text-align: justify">bolesti hlavy</div></li>
    <li><div style="text-align: justify">problémy v oblasti trávicí soustavy </div></li>
    <li><div style="text-align: justify">vyrážky </div></li>
    <li><div style="text-align: justify">stesky na nejasné bolesti a pobolívání </div></li>
    <li><div style="text-align: justify">poruchy spánku</div></li>
</ul>
<h2>Reakce a prižívání dospívajících</h2>
<ul>
    <li><div style="text-align: justify">ztráta zájmu o vrstevníky, koníčky a zájmové činnosti </div></li>
    <li><div style="text-align: justify">smutek nebo deprese</div></li>
    <li><div style="text-align: justify">úzkost a obavy o bezpečí </div></li>
    <li><div style="text-align: justify">odpor vůči autoritě</div></li>
    <li><div style="text-align: justify">pocity neschopnosti a bezmoci</div></li>
    <li><div style="text-align: justify">vina, výčitky svědomí, stud a plachost </div></li>
    <li><div style="text-align: justify">touha po odplatě</div></li>
</ul>
<h2>Možnosti postupu</h2>
<ul>
    <li><div style="text-align: justify">věnujte dospívající/mu více pozornosti a zájmu </div></li>
    <li><div style="text-align: justify">dočasně zmírněte požadavky na výkon ve škole i doma</div></li>
    <li><div style="text-align: justify">povzbuzujte debaty o prožitcích traumatu s vrstevníky a důležitými dospělými</div></li>
    <li><div style="text-align: justify">netrvejte na tom, aby se o svých pocitech bavil/a s rodiči </div></li>
    <li><div style="text-align: justify">vypořádejte se s tendencemi k lehkomyslnosti a bezohlednosti, pojmenujte je</div></li>
    <li><div style="text-align: justify">propojujte chování a prožívání s událostí </div></li>
    <li><div style="text-align: justify">podporujte cvičení a další tělesné činnosti </div></li>
    <li><div style="text-align: justify">povzbuzujte návrat ke společenským činnostem, k atletice, klubům atp.</div></li>
    <li><div style="text-align: justify">účast na činnostech v obci a na školních akcích </div></li>
    <li><div style="text-align: justify">vytvářejte školní programy zaměřené na vrstevnickou oporu a vytipování ohrožených mladistvých, podpůrné skupiny pro ohrožené studenty, telefonické kontaktní linky v tísni, nízkoprahová kontaktní střediska </div></li>
</ul>
<div style="text-align: justify"><div style="font-size: small">
Dle <span style="text-decoration: underline"><a href="https://permanent.fdlp.gov/lps79620/SMA05-4025.pdf">U.S. Department of Health and Human Services. Mental Health Response to Mass Violence and Terrorism: A Field Guide. (DHHS Pub. No. SMA 4025. Rockville, MD: Center for Mental Health Services, Substance Abuse and Mental Health Services Administration, 2005)</a></span></div></div>""";
const html_article_6_4 = """<h2>Reakce v chování dospělých</h2>
<ul>
    <li><div style="text-align: justify">špatně spí </div></li>
    <li><div style="text-align: justify">vyhýbá se připomínkám události</div></li>
    <li><div style="text-align: justify">je přespříliš aktivní</div></li>
    <li><div style="text-align: justify">ochraňuje blízké </div></li>
    <li><div style="text-align: justify">snadno se rozpláče</div></li>
    <li><div style="text-align: justify">zlostně vybuchuje </div></li>
    <li><div style="text-align: justify">častěji než dříve se dostává do konfliktů s rodinou </div></li>
    <li><div style="text-align: justify">reaguje se zvýšenou ostražitostí a bdělostí </div></li>
    <li><div style="text-align: justify">izoluje se, stahuje, mlčí, vypíná </div></li>
    <li><div style="text-align: justify">častěji než dříve pije alkohol nebo užívá nezákonné drogy</div></li>
</ul>
<h2>Tělesné projevy u dospělých</h2>
<ul>
    <li><div style="text-align: justify">nevolnost </div></li>
    <li><div style="text-align: justify">bolesti hlavy</div></li>
    <li><div style="text-align: justify">únava, vyčerpání </div></li>
    <li><div style="text-align: justify">nepohoda v oblasti trávicí soustavy </div></li>
    <li><div style="text-align: justify">změny chuti k jídlu </div></li>
    <li><div style="text-align: justify">tělesné stesky</div></li>
    <li><div style="text-align: justify">zhoršení chronických stavů</div></li>
</ul>
<h2>Reakce v prožívání dospělých</h2>
<ul>
    <li><div style="text-align: justify">šok, dezorientace, otupělost</div></li>
    <li><div style="text-align: justify">deprese, smutek</div></li>
    <li><div style="text-align: justify">žal </div></li>
    <li><div style="text-align: justify">podrážděnost, zlost</div></li>
    <li><div style="text-align: justify">úzkost, strach</div></li>
    <li><div style="text-align: justify">zoufalství, bezmocnost</div></li>
    <li><div style="text-align: justify">vina, pochybnosti o sobě</div></li>
    <li><div style="text-align: justify">výkyvy nálad</div></li>
</ul>
<h2>Možnosti postupu</h2>
<ul>
    <li><div style="text-align: justify">ochraňujte, nasměrujte a propojujte </div></li>
    <li><div style="text-align: justify">zajistěte přístup k neodkladné lékařské péči </div></li>
    <li><div style="text-align: justify">podporujte nasloucháním a poskytněte příležitost mluvit o prožitcích a ztrátách</div></li>
    <li><div style="text-align: justify">poskytujte časté a aktualizované zprávy o záchranných pracích, o obnově a zotavování </div></li>
    <li><div style="text-align: justify">dodejte kontakty atp. pro odpovědi na otázky </div></li>
    <li><div style="text-align: justify">pomáhejte s určováním, co má přednost, a s řešením problémů </div></li>
    <li><div style="text-align: justify">pomáhejte rodině usnadňovat komunikaci a účelně fungovat </div></li>
    <li><div style="text-align: justify">poskytujte rady rodinám a informace o traumatické zátěži a o vyrovnávání se s ní, o reakcích dětí </div></li>
    <li><div style="text-align: justify">poskytujte informace o policejním vyšetřování a trestním řízení, o roli zdravotnických záchranářů atp.</div></li>
    <li><div style="text-align: justify">poskytujte služby pro oběti trestných činů </div></li>
    <li><div style="text-align: justify">posuzujte stav a tam, kde je třeba, doporučte navazující pomoc </div></li>
    <li><div style="text-align: justify">poskytujte informace o možných navazujících službách a dalších zdrojích</div></li>
</ul>
<div style="text-align: justify"><div style="font-size: small">
Dle <span style="text-decoration: underline">U.S. Department of Health and Human Services. Mental Health Response to Mass Violence and Terrorism: A Field Guide. (DHHS Pub. No. SMA 4025. Rockville, MD: Center for Mental Health Services, Substance Abuse and Mental Health Services Administration, 2005)</span></div></div>""";
const html_article_6_5 = """<h2>Reakce v chování seniorů</h2>
<ul>
    <li><div style="text-align: justify">stahuje se a izoluje </div></li>
    <li><div style="text-align: justify">neochotně opouští domov</div></li>
    <li><div style="text-align: justify">objevují se problémy s pohybem </div></li>
    <li><div style="text-align: justify">problematicky se přizpůsobuje, je-li přestěhován/a</div></li>
</ul>
<h2>Tělesné projevy u seniorů</h2>
<ul>
    <li><div style="text-align: justify">zhoršení chronických nemocí </div></li>
    <li><div style="text-align: justify">poruchy spánku</div></li>
    <li><div style="text-align: justify">problémy s pamětí </div></li>
    <li><div style="text-align: justify">tělesné příznaky </div></li>
    <li><div style="text-align: justify">zvýšená citlivost a náchylnost k přehřátí a podchlazení </div></li>
    <li><div style="text-align: justify">tělesná a smyslová omezení (zrak, sluch) ztěžují zotavování</div></li>
</ul>
<h2>Reakce v prožívání seniorů</h2>
<ul>
    <li><div style="text-align: justify">deprese</div></li>
    <li><div style="text-align: justify">zoufání nad ztrátami</div></li>
    <li><div style="text-align: justify">apatie </div></li>
    <li><div style="text-align: justify">zmatenost, dezorientace</div></li>
    <li><div style="text-align: justify">podezíravost</div></li>
    <li><div style="text-align: justify">neklid, zlost</div></li>
    <li><div style="text-align: justify">obavy z umístění do ústavu </div></li>
    <li><div style="text-align: justify">úzkost z neznámého prostředí </div></li>
    <li><div style="text-align: justify">rozpaky z přijímání podpor a výpomocí </div></li>
</ul>
<h2>Možnosti postupu</h2>
<ul>
    <li><div style="text-align: justify">v rozhovoru důrazně a vytrvale ujišťujte </div></li>
    <li><div style="text-align: justify">pro orientaci poskytujte informace</div></li>
    <li><div style="text-align: justify">postarejte se, aby bylo pamatováno na tělesné potřeby (voda, jídlo, teplo)</div></li>
    <li><div style="text-align: justify">používejte vícero posuzovacích metod, neboť problémy bývají podhodnoceny</div></li>
    <li><div style="text-align: justify">pomáhejte při obnově kontaktů s rodinou a dalšími opěrnými systémy</div></li>
    <li><div style="text-align: justify">pomozte získat zdravotní a finanční pomoc </div></li>
    <li><div style="text-align: justify">povzbuzujte rozhovor o traumatické zkušenosti, o ztrátách a podporujte vyjádření pocitů </div></li>
    <li><div style="text-align: justify">poskytujte pomoc určenou obětem trestných činů</div></li>
</ul>
<div style="text-align: justify"><div style="font-size: small">
Dle <span style="text-decoration: underline">U.S. Department of Health and Human Services. Mental Health Response to Mass Violence and Terrorism: A Field Guide. (DHHS Pub. No. SMA 4025. Rockville, MD: Center for Mental Health Services, Substance Abuse and Mental Health Services Administration, 2005)</span></div></div>""";
const html_article_7_1 =
    """<div style="text-align: justify"><p>Zátěžová situace je často pro jedince:</p>
<ul>
    <li><div style="text-align: justify">nečitelná,</div></li>
    <li><div style="text-align: justify">neřešitelná,</div></li>
    <li><div style="text-align: justify">nezvládnutelná, protože chybějí prostředky k řešení,</div></li>
    <li><div style="text-align: justify">ohrožující,</div></li>
</ul>
<div style="text-align: justify"><p>proto psychika aktivuje obranné mechanismy. Obranný mechanismus je schopnost mysli se různými způsoby nevědomě bránit proti uvědomění nepříjemných emocí, fantazií, traumat, impulzů. Překrucuje realitu nebo ji popírá. Při zátěži člověk může použít jeden nebo více z asi 40 popsaných obranných mechanismů, přičemž neví, že ho používá, neuvědomuje si ho. Krátkodobě plní pro jedince i skupinu významnou funkci, také proto je z použití neobviňujeme, nekritizujeme, nehodnotíme, nesnažíme se jich za každou cenu zbavit. Přistupujeme citlivě a laskavě, přitom pevně. Obranné mechanismy se vyskytují jak na straně zasažených, tak i pomáhajících. V zátěžové situaci nám pomáhají přežít, pokud přetrvávají dlouho, snižují kvalitu života.</p></div>
<h2>Obranné mechanismy:</h2>
<h3>Disociace</h3>
<div style="text-align: justify"><p>Mnoho druhů projevů od poruch paměti a vnímání až k necítění částí těla, ztuhnutí nebo křečím, psychika se rozštěpí na části, které za normálních okolností jsou propojené.</p>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Bdělá přítomnost, maximální trpělivost, klid, jemný přístup, podpora těla a probíhajícího procesu.</p>
<div style="text-align: justify"><p><strong>Co nedělat?</strong>
Nezakazujeme projevy disociace, neoznačujeme zasažené jako nespolupracující.</p>
<h3>Derealizace</h3>
<div style="text-align: justify"><p>Zasažený prožívá minimum emocí, připadá si jako vytržený ze života, ze současného dění, pocit „jako ve filmu“.</p>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Popisujeme, co se děje a co pravděpodobně by zasažený mohl prožívat, pocit neskutečna označujeme jako normální v té situaci.</p>
<h3>Vyhýbání se</h3>
<div style="text-align: justify"><p>Odvádění řeči jinam, nápadné nehledění na zdroj ohrožení, odcházení jinam.</p>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Ujišťujeme, že je běžné v takové situaci snažit se tomu vyhnout, respektujeme.</p>
<h3>Depersonalizace</h3>
<div style="text-align: justify"><p>Vlastní já, vlastní tělo se může zdát být cizí, neskutečné, ne vlastní.</p></div>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Normalizujeme pocity.</p></div>
<h3>Popření</h3>
<div style="text-align: justify"><p>Jednoduše popírá, co je zjevné: „Ne, to není pravda. Spletli jste se. Jedná se o někoho jiného.“</p></div>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Mluvíme o realitě jak je, a nepřekrucujeme ji: „Ano, opravdu zemřel. Je mi to líto, nedá se dělat víc. Udělali jsme vše, co bylo možné. V této chvíli je po smrti.“</p></div>
<div style="text-align: justify"><p><strong>Co nedělat?</strong>
Nepřejímáme obranné mechanismy zasažených, nepoužíváme slova jako zesnul, odešel apod., jsou výrazem popření a nenabízíme falešné naděje.</p></div>
<h3>Racionalizace</h3>
<div style="text-align: justify"><p>Rozumově vysvětluje něco nepřijatelného: <em>„Už ho nic nebolí. Je to tak lepší. Jen by se trápil. Co je to vlastně za život v tomto krutém světě? Teď je mu lépe.“</em></p></div>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Uznáme, že ho současná ztráta, bolest či trápení může bolet víc, než si kdokoliv může vůbec představit.</p></div>
<div style="text-align: justify"><p><strong>Co nedělat?</strong>
Nepotvrzujeme ani nevyvracíme, nehádáme se se zasaženými.</p></div>
<h3>Obrana ve vnímání</h3>
<div style="text-align: justify"><p>Některé věci prostě zasažený přehlíží, neslyší, necítí. Tím se chrání před pro něj v tuto chvíli nebezpečnými informacemi.</p></div>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Jsme trpěliví, vydržíme to s ním. Některé informace musíme opakovat několikrát.</p></div>
<div style="text-align: justify"><p><strong>Co nedělat?</strong></p></div>
<div style="text-align: justify"><p>Nevyčítáme, kolikrát mu to ještě musíme říkat, nebereme to osobně jako útok či nespolupráci.</p></div>
<h3>Regrese</h3>
<div style="text-align: justify"><p>Zasažený se začne chovat jako mladší, případně jako dítě. Například staří lidé a děti mohou usnout, starší děti si začnou cucat paleček, počůrají se, kňourají, šišlají. Chtějí být opečováváni jako batolata. Zbavují se odpovědnosti.</p></div>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Jsme maximálně laskaví, vyjadřujeme pochopení, projevujeme klid, slovy, hlasem i beze slov. Současně jim vracíme jejich kompetence a zodpovědnost.</p></div>
<div style="text-align: justify"><p><strong>Co nedělat?</strong>
Nezesměšňujeme zasažené nebo nezakazujeme projevy regrese, netlačíme do zralejšího chování. Nerozhodujeme za ně, neděláme za ně, na co stačí.</p></div>
<h3>Agrese</h3>
<div style="text-align: justify"><p>Zasažený vykazuje tendence ublížit sobě nebo druhým, rozbít, ničit, nadávat, slovně napadat, pomlouvat.</p></div>
<div style="text-align: justify"><p><strong>Co dělat?</strong>
Pojmenujeme, co vše ho může hněvat, normalizujeme hněv jako reakci na zátěž, oddělujeme od násilného chování, snažíme se zabránit zranění či ničení, chráníme se. Nabízíme sociálně přijatelné způsoby vybití</p></div>""";
const html_article_7_2 = """
<div style="text-align: justify"><p>Není vhodné mlátit např. do věcí!!! Užití abreaktivních technik je mnohem lepší než mlátit pěstmi – hrozí ublížení si, navíc lidé mívají často blbé pocity, že něco ničí nebo někomu ubližují (pak stud, nenávist atd.) Navíc je to příliš prudká reakce, kdy lidi jdou do afektu a nemají to pod kontrolou, což je špatně!!! Naopak je třeba mít limit.</p></div>
<div style="text-align: justify"><p>Limit dostává i tím, že se zeď pod jeho energií nezhroutí.</p></div>
<ul>
    <li><div style="text-align: justify">
        <div style="text-align: justify"><p>cviky proti zdi (limit), u lidí s nahromaděnou energií, neklid</p></div>
        <ul>
            <li><div style="text-align: justify">tlačení nohama v leže na zádech proti zdi (doplnit obrázek)</div></li>
            <li><div style="text-align: justify">tlačení rukama proti zdi (doplnit obrázek)</div></li>
            <li><div style="text-align: justify">tlačení do futer od dveří (doplnit obrázek)</div></li>
            <li><div style="text-align: justify">tlačení dlaněmi proti sobě, s nádechem tlačíme, zadržíme dech a zvýšíme úsilí na maximum možného a s výdechem povolíme</div></li>
        </ul>
    </div></li>
    <li><div style="text-align: justify">
    <p>Všechny cviky komentujeme</p>
        <ul>
            <li><div style="text-align: justify"><em>„Vidím, že máte strašně moc energie. Vím o něčem, co pomáhá lidem, kteří se cítí takhle. Chcete to zkusit?“</em></div></li>
            <li><div style="text-align: justify"><em>„Kolik té energie je bezpečné, abyste vyjádřil ven?“</em></div></li>
            <li><div style="text-align: justify"><em>„10%.....30%...“</em></div></li>
            <li><div style="text-align: justify"><em>„Když vám to půjde, přidejte zvuk…“</em></div></li>
        </ul>
    </div></li>
    <li><div style="text-align: justify">
        <div style="text-align: justify"><p>Doporučení</p>
        <ul>
            <li><div style="text-align: justify">Vždy dělat cviky s ním1 <em>„to je v pořádku, že…“ „je to rychlé a bezpečné“</em></div></li>
            <li><div style="text-align: justify">ženy: <ul>
                    <li><div style="text-align: justify">opřít se zády o zeď a tlačit</div></li>
                </ul>
            </div></li>
            <li><div style="text-align: justify">muži/ženy: <ul>
                    <li><div style="text-align: justify">jednou rukou (oboustranně)</div></li>
                    <li><div style="text-align: justify">dvěma rukama</div></li>
                </ul>
            </div></li>
            <li><div style="text-align: justify">dospívající: <ul>
                    <li><div style="text-align: justify">nohou</div></li>
                </ul>
            </div></li>
        </ul>
    </div></li>
    <li><div style="text-align: justify">
        <p>Kdy to nedělat?</p>
        <ul>
            <li><div style="text-align: justify">Nikdy ne v mánii!</div></li>
            <li><div style="text-align: justify">Kontraindikace jen vysoký nitrooční tlak.</div></li>
        </ul>
    </div></li>
</ul>""";
const html_article_7_3 = """<h3>Co nedělat?</h3>
<div style="text-align: justify"><p>Nesnažíme se ho zavřít někam, nereagujeme na jeho agresi svou agresí, nesnažíme se ho potrestat, nezesilujeme nebo neprovokujeme jeho hněv.</p>
<h2><strong>Projekce</strong></h2>
<div style="text-align: justify"><p>Připisování vlastních nepřijatelných vlastností nebo emocí druhým lidem <em>„Vy jste smutný, naštvaný….“.</em></p>
<h3>Co dělat?</h3>
<div style="text-align: justify"><p>Popíšeme, co cítíme, nabídneme, že i zasažený se tak může cítit, může se na nás hněvat.</p>
<h3>Co nedělat?</h3>
<div style="text-align: justify"><p>Nepopíráme a nepotlačujeme své pocity, nevysvětlujeme pouze racionálně.</p>
""";
const html_article_8_1 = """<h2>Defusing</h2>
<ul>
    <li><div style="text-align: justify">intervence pro malou skupinu</div></li>
    <li><div style="text-align: justify">pro homogenní skupinu</div></li>
    <li><div style="text-align: justify">ukončení situace</div></li>
    <li><div style="text-align: justify">všichni byli vystaveni traumatu přibližně stejně</div></li>
    <li><div style="text-align: justify">trvání 20–45 min.</div></li>
    <li><div style="text-align: justify">slouží k „začištění“ události</div></li>
</ul>
<h2>Úvod</h2>
<ul>
    <li><div style="text-align: justify">představení týmu a uvedení pravidel</div></li>
    <li><div style="text-align: justify">důraz na důvěrnost</div></li>
    <li><div style="text-align: justify">nikdo nebude nucen mluvit</div></li>
    <li><div style="text-align: justify">mluvte o své vlastní zkušenosti</div></li>
    <li><div style="text-align: justify">tým bude nejdříve poslouchat – informovat bude později</div></li>
    <li><div style="text-align: justify">zkušenost každého jedince je důležitá</div></li>
    <li><div style="text-align: justify">účelem není hledat viníka či někoho obviňovat</div></li>
    <li><div style="text-align: justify">účelem není kritika či vyšetřování</div></li>
    <li><div style="text-align: justify">dbejte na dodržování pravidel</div></li>
</ul>
<h2>Explorace (Fakta) – ptejte se….</h2>
<ul>
    <li><div style="text-align: justify">Co se z Vašeho pohledu stalo?</div></li>
    <li><div style="text-align: justify">Kdo dorazil první? Co se stalo? Co bylo dále?</div></li>
    <li><div style="text-align: justify">Byl jste přímo v kontaktu se zasaženými?</div></li>
    <li><div style="text-align: justify">Co pro Vás bylo podstatné/významné?</div></li>
    <li><div style="text-align: justify">Co ve Vás přetrvává?</div></li>
    <li><div style="text-align: justify">Něco dalšího?</div></li>
</ul>
<h2>Informace</h2>
<ul>
    <li><div style="text-align: justify">informujte o možných reakcích</div></li>
    <li><div style="text-align: justify">normalizujte jejich projevy</div></li>
    <li><div style="text-align: justify">nabídněte doporučení, co obvykle pomáhá </div></li>
    <li><div style="text-align: justify">varujte před alkoholem, drogami, tučným jídlem, nevhodným - jídlem, kofeinem a nikotinem</div></li>
    <li><div style="text-align: justify">podpořte smysluplné aktivity</div></li>
    <li><div style="text-align: justify">shrňte důležité informace a dejte prostor pro dotazy</div></li>
    <li><div style="text-align: justify">poskytněte letáky/kontakty</div></li>
</ul>""";
const html_article_8_2 = """<ul>
        <li><div style="text-align: justify">30 minutová intervence pro velkou skupinu</div></li>
        <li><div style="text-align: justify">pro zasahující/homogenní skupinu</div></li>
        <li><div style="text-align: justify">po katastrofách/událostech velkého rozsahu</div></li>
        <li><div style="text-align: justify">provádí se pouze jedenkrát, po prvotním vystavení události</div></li>
        <li><div style="text-align: justify">provádí se po ukončení záchranných a likvidačních prací</div></li>
        <li><div style="text-align: justify">pro skupiny, které byly přibližně stejnou měrou vystaveny traumatu</div></li>
        <li><div style="text-align: justify">začíná se stručným úvodem interventa</div></li>
        <li><div style="text-align: justify">poskytují se informace/zdůrazňují se instrukce pro vyrovnání se se stresem</div></li>
        <li><div style="text-align: justify">ptáme se na dotazy/komentáře        </div></li>
        <li><div style="text-align: justify">při demobilizaci mluví zasahující příslušníci zřídka </div></li>
        <li><div style="text-align: justify">informační část trvá max. 10 minut</div></li>
        <li><div style="text-align: justify">20 minut je vyhrazeno na odpočinek a jídlo</div></li>
        <li><div style="text-align: justify">zasahující mohou jít domů či jít plnit své další povinnosti</div></li>
        <li><div style="text-align: justify">zasahující příslušníci by se neměli vrátit na místo události nejméně dalších 6 hodin</div></li>
        </ul>
        """;
const html_article_8_3 = """<ul>
<li><div style="text-align: justify">intervence pro velkou skupinu</div></li>
<li><div style="text-align: justify">30–45 minut</div></li>
<li><div style="text-align: justify">pro lidi, kteří nejsou zasahující</div></li>
<li><div style="text-align: justify">vyžaduje myšlení a plánování</div></li>
<li><div style="text-align: justify">používán před, během nebo po krizové události</div></li>
<li><div style="text-align: justify">opakuje se podle změny situace</div></li>
<li><div style="text-align: justify">vyžaduje homogenní skupinu</div></li>
<li><div style="text-align: justify">skupiny se stejnou měrou vystavení události</div></li>
<li><div style="text-align: justify">nehrozí žádné bezprostřední nebezpečí</div></li>
<li><div style="text-align: justify">shromažďují se dohromady specifické skupiny</div></li>
<li><div style="text-align: justify">důvěryhodný představitel prezentuje fakta</div></li>
<li><div style="text-align: justify">stručná řízená diskuze/otázky a odpovědi</div></li>
<li><div style="text-align: justify">podání informací/instrukcí/zdůraznění dovedností, které pomáhají </div></li>
<li><div style="text-align: justify">otevření možnosti následného setkání všech zúčastněných</div></li>
</ul>
""";
const html_article_9_1 = """
<h2>Jak se o sebe postarat po náročném zásahu?</h2>
<ul>
        <li><div style="text-align: justify">Věnuji pozornost dechu. Dýchám hluboce a pomalu. Uvědomuji si nádechy a výdechy.</div></li>
        <li><div style="text-align: justify">Zaměřím se na to, co cítím a jaké mne napadají myšlenky. Neodháním je. Uvědomím si, že prožívám mnoho různých pocitů, např. smutek, radost, vztek, úžas, pocit neskutečna, uspokojení, neklidu, rozčarování… Přichází například i pocit hněvu a bezmoci z toho, že jsem nemohl udělat více nebo že jsem nemohl udělat vůbec nic. Pocity přijímám, patří k silnému zážitku. Neutápím se v nich, registruji je. Současně si uvědomuji, že jsem udělal vše, co bylo momentálně v mých silách a ostatní také.</div></li>
        <li><div style="text-align: justify">Zrekapituluji pro sebe, co jsem měl chuť udělat, ale nemohl jsem <em>(„Řekl bych třeba necitlivému kolegovi, ať je raději ticho, nebo bych ho poslal někam pryč.“, „Měl jsem chuť dát tomu opilému řidiči pěstí, kopnout ho.“, „Nejraději bych utekl někam daleko, pryč od toho všeho.“</em>).</div></li>
        <li><div style="text-align: justify">Uvědomím si, že to, co jsem prožil, není běžná lidská zkušenost a mám právo být po určitou dobu také trochu „mimo“, než se s tímto prožitkem vyrovnám.</div></li>
        <li><div style="text-align: justify">Nezůstanu se svým zážitkem sám, mluvím o něm se svými blízkými, kolegy v práci apod. co nejdříve po zásahu. Případně si vyžádám intervenci psychologa jen pro sebe nebo pro celou skupinu, která může být zasažena.</div></li>
        <li><div style="text-align: justify">Pokud máte potřebu, kontaktujte své blízké, abyste se ujistili, že je u vás doma vše v pořádku.</div></li>
        <li><div style="text-align: justify">Připomenu si, co mi jindy pomohlo v nějaké tíživé situaci. Pomohlo by mi to také nyní? Nebo potřebuji něco jiného?</div></li>
        <li><div style="text-align: justify">Pokud mi obvykle pomáhá sprcha, koupel, hudba, film, pohyb, práce, dopřeji si je.</div></li>
        <li><div style="text-align: justify">Snažím se co nejdříve zapojit do normálního života, dělám běžné věci v obvyklou dobu, piji nápoje bez alkoholu, věnuji pozornost tomu, co jím, zachovávám hygienu, zacvičím si, uklidím po sobě, jdu spát.</div></li>
        </ul>
        <div style="text-align: justify"><p>Pokud u sebe od události pozoruji příznaky: narušený spánek nebo nespavost, děsivé sny, tendenci uchylovat se k alkoholu a zneužívat léky, vyhýbání se lidem, nepřiměřenou slovní a fyzickou agresivitu, necitlivost a cynismus, neobvyklé nálady, nevysvětlitelný hněv a smutek, pak potřebuji odbornou pomoc a je dobré ji vyhledat. Můžete kontaktovat člena Týmu posttraumatické péče (peera) či Vašeho krajského psychologa.</p></div>
        """;
