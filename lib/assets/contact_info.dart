final List<ContactInfo> contacts = [
  new ContactInfo("PL Bohnice Praha", "https://www.bohnice.cz/", "284016111"),
  new ContactInfo("RIAPS Praha",
      "https://www.csspraha.cz/kc-riaps", "222586768"),
  new ContactInfo(
      "Krizové centrum Ostrava", "https://www.kriceos.cz/", "596110883"),
  new ContactInfo(
      "Dětské krizové centrum Praha", "https://www.ditekrize.cz/", "777715215"),
  new ContactInfo("Krizové centrum psychiatrické kliniky  FN Brno",
      "https://www.fnbrno.cz/psychiatricka-klinika/k1484", "532232078"),
  new ContactInfo("Krizové centrum Spirála Ústí nad Labem",
      "https://www.spirala-ul.cz/", "475603390"),
  new ContactInfo(
      "Dětské krizové centrum Olomouc", "https://ssp-ol.cz/", "585427141"),
  new ContactInfo(
      "Krizové centrum Olomouc", "https://www.olomouc.charita.cz/", "734435078"),
  new ContactInfo(
      "Krizové centrum Jihlava", "https://pestalozzi.cz/", "727803665"),
  new ContactInfo(
      "Dětské krizové centrum Hradec Králové",
      "https://www.adra.cz/pomoc-v-cr/poradna-hradec-kralove/detske-krizove-centrum",
      "606824104"),
  new ContactInfo("Krizové centrum pro děti a rodinu České Budějovice",
      "https://www.ditevkrizi.cz/", "776763176"),
  new ContactInfo("Res vitae Karlovy Vary", "https://www.resvitae.cz/"),
  new ContactInfo("Linky důvěry", "https://www.capld.cz/linky-duvery-cr/"),
  new ContactInfo("Občanské poradny", "https://www.obcanskeporadny.cz/"),
  new ContactInfo("Psychosociální pomoc",
      "https://www.hzscr.cz/clanek/standardy-psychosocialni-krizove-pomoci-a-spoluprace.aspx"),
  new ContactInfo("Pohřeb", "https://www.pohreb.cz/"),
  new ContactInfo("SOS centrum Diakonie ČCE - SKP v Praze",
      "https://www.soscentrum.cz", "222514040")
];

class ContactInfo {
  String name, webAddress, phone;

  ContactInfo(this.name, this.webAddress, [this.phone]);
}
