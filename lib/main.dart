import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:ppp_flutter/application.dart';
import 'package:ppp_flutter/utils/sentry_reporter.dart' as sentryReporter;

final buildTypeTags = "build_type";
final platformTags = "platform";

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  const config = MethodChannel("ppp.nic.cz/config");

  String dsn = await config.invokeMethod('getSentryDsn');

  var packageInfo = await PackageInfo.fromPlatform();

  final SentryClient sentryClient = SentryClient(SentryOptions(dsn: dsn));

  runZonedGuarded(() async {
    runApp(MaterialApp(home: MainWidgetPage()));
    }, (exception, stackTrace) async {
      await sentryReporter.reportSentryException(sentryClient, exception, stackTrace);
    });


  /*final SentryClient sentryClient = SentryClient(
      dsn: dsn,
      environmentAttributes: Event(release: packageInfo.version, tags: {
        buildTypeTags: isInDebugMode ? "Debug" : "Release",
        platformTags:
            Platform.isAndroid ? "Android" : Platform.isIOS ? "iOS" : "unknown",
      }));

  runZoned(() async {
    runApp(MaterialApp(home: MainWidgetPage()));
  }, onError: (error, stackTrace) async {
    await sentryReporter.reportSentryError(sentryClient, error, stackTrace);
    exit(0);
  });*/
}

bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}
