import 'package:flutter/material.dart';
import 'package:ppp_flutter/utils/utils.dart';

abstract class AdaptivePageViewState extends State {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    if (useTabletView(width, height)) {
      return buildTabletLayout(width, height);
    }

    return buildMobileLayout(width, height);
  }

  Widget buildTabletLayout(width, height);

  Widget buildMobileLayout(width, height);
}