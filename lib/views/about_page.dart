import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'adaptive_page.dart';
import 'package:ppp_flutter/assets/app_resources.dart';
import 'package:auto_size_text/auto_size_text.dart';

class InfoScreenWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InfoScreenWidgetState();
  }
}

class InfoScreenWidgetState extends AdaptivePageViewState {
  @override
  Widget buildMobileLayout(width, height) {
    return Scaffold(
        appBar: AppBar(
          title: AutoSizeText(aboutAppTitle),
        ),
        body: Container(
            alignment: Alignment.center,
            child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width * 0.95,
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(20.0),
                      alignment: Alignment.center,
                      child: Image(
                        image: AssetImage(aboutAppImageAsset),
                        width: 130.0,
                        height: 130.0,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(20.0),
                      alignment: Alignment.center,
                      child: Image(
                        image: AssetImage(aboutAppNicLogoImageAsset),
                        width: 160.0,
                        height: 80.0,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 20.0),
                      child: AutoSizeText(
                        appLicense,
                        style: TextStyle(fontSize: 18.0),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          if (!await launchUrl(Uri.parse(licenseLink))) {
                            throw Exception('Could not launch $licenseLink');
                          }
                        },
                        child: AutoSizeText(
                          licenseLink,
                          style: TextStyle(
                              fontSize: 18.0,
                              decoration: TextDecoration.underline,
                              fontStyle: FontStyle.italic),
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                    Container(
                      child: AutoSizeText(
                        aboutAppDetails,
                        style: TextStyle(fontSize: 18.0),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ))));
  }

  @override
  Widget buildTabletLayout(width, height) {
    return Scaffold(
        appBar: AppBar(
          title: AutoSizeText(aboutAppTitle),
        ),
        body: Container(
            alignment: Alignment.center,
            child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width * 0.7,
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(30.0),
                      alignment: Alignment.center,
                      child: Image(
                        image: AssetImage(aboutAppImageAsset),
                        width: 130.0,
                        height: 130.0,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(20.0),
                      alignment: Alignment.center,
                      child: Image(
                        image: AssetImage(aboutAppNicLogoImageAsset),
                        width: 160.0,
                        height: 80.0,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 20.0),
                      child: AutoSizeText(
                        appLicense,
                        style: TextStyle(fontSize: 18.0),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          if (!await launchUrl(Uri.parse(licenseLink))) {
                            throw Exception('Could not launch $licenseLink');
                          }
                        },
                        child: AutoSizeText(
                          licenseLink,
                          style: TextStyle(
                              fontSize: 18.0,
                              decoration: TextDecoration.underline,
                              fontStyle: FontStyle.italic),
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                    Container(
                      child: AutoSizeText(
                        aboutAppDetails,
                        style: TextStyle(fontSize: 18.0),
                        textAlign: TextAlign.justify,
                      ),
                    )
                  ],
                ))));
  }
}
