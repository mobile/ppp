import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:ppp_flutter/assets/contact_info.dart';
import 'package:ppp_flutter/views/adaptive_page.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ContactsPageState();
  }
}

class _ContactsPageState extends AdaptivePageViewState {
  @override
  Widget buildMobileLayout(width, height) {
    var maxWidth = width;

    return Container(
        constraints: BoxConstraints(maxWidth: maxWidth),
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
        alignment: Alignment.center,
        child: Container(
          constraints: BoxConstraints(maxWidth: maxWidth * 0.95),
          child: Column(
            children: <Widget>[
              Expanded(
                  child: ListView(
                      children: contacts
                          .map((contact) => ContactCard(contact, false))
                          .toList())),
            ],
          ),
        ));
  }

  @override
  Widget buildTabletLayout(width, height) {
    return Container(
        constraints: BoxConstraints(maxWidth: width),
        alignment: Alignment.center,
        child: Container(
          constraints: BoxConstraints(maxWidth: width * 0.7),
          child: Column(
            children: <Widget>[
              Expanded(
                  child: GridView.extent(
                      maxCrossAxisExtent: 300.0,
                      children: contacts
                          .map((contact) => ContactCard(contact, true))
                          .toList())),
            ],
          ),
        ));
  }
}

class ContactCard extends StatefulWidget {
  final ContactInfo contactInfo;
  bool tabletView;

  ContactCard(this.contactInfo, this.tabletView);

  @override
  State<StatefulWidget> createState() {
    return ContactCardState(contactInfo);
  }
}

class ContactCardState extends AdaptivePageViewState {
  final ContactInfo contactInfo;

  ContactCardState(this.contactInfo);

  @override
  Widget buildMobileLayout(width, height) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
      child: Container(
        width: width,
        height: 100.0,
        child: Row(
          children: <Widget>[
            Flexible(
                fit: FlexFit.tight,
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsetsDirectional.fromSTEB(10.0, 5.0, 5.0, 5.0),
                  child: Container(
                      child: AutoSizeText(contactInfo.name,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.w500),
                          softWrap: true)),
                )),
            ButtonsBarWidget(contactInfo)
          ],
        ),
      ),
    );
  }

  @override
  Widget buildTabletLayout(width, height) {
    return Card(
      child: Container(
        child: Column(
          children: <Widget>[
            Flexible(
              fit: FlexFit.tight,
              child: Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsetsDirectional.fromSTEB(10.0, 5.0, 5.0, 5.0),
                child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: AutoSizeText(contactInfo.name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 24.0),
                        maxLines: 4,
                        softWrap: true)),
              ),
            ),
            ButtonsBarWidget(contactInfo)
          ],
        ),
      ),
    );
  }
}

class ButtonsBarWidget extends StatefulWidget {
  final ContactInfo contactInfo;

  ButtonsBarWidget(this.contactInfo);

  @override
  State<StatefulWidget> createState() {
    return ButtonsBarWidgetState(contactInfo);
  }
}

class ButtonsBarWidgetState extends AdaptivePageViewState {
  final ContactInfo contactInfo;

  ButtonsBarWidgetState(this.contactInfo);

  @override
  Widget buildMobileLayout(width, height) {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      child: Row(
        children: getContactButtons(contactInfo, 36.0),
        mainAxisAlignment: MainAxisAlignment.end,
      ),
    );
  }

  @override
  Widget buildTabletLayout(width, height) {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
      child: Row(
        children: getContactButtons(contactInfo, 42.0),
        mainAxisAlignment: MainAxisAlignment.end,
      ),
    );
  }

  openPhone() async {
    final Uri phone = Uri(
      scheme: 'tel',
      path: contactInfo.phone, // your number
    );
    String tel = "tel:" + contactInfo.phone;
    if (!await launchUrl(phone)) {
      throw Exception('Could not launch $tel');
    }
  }

  openWeb() async {
    String web = contactInfo.webAddress;
    if (!await launchUrl(Uri.parse(web))) {
      throw Exception('Could not launch $web');
    }
  }

  List<Widget> getContactButtons(ContactInfo contactInfo, size) {
    List<Widget> buttons = [];

    if (contactInfo.phone != null) {
      buttons.add(Container(
          child: IconButton(
              icon: Icon(Icons.phone),
              iconSize: size,
              onPressed: () => openPhone())));
    }

    buttons.add(Container(
        child: IconButton(
            icon: Icon(Icons.language),
            iconSize: size,
            onPressed: () => openWeb())));

    return buttons;
  }
}
