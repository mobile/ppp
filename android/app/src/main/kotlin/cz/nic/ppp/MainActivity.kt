package cz.nic.ppp

import androidx.annotation.NonNull
import cz.nic.ppp.BuildConfig.SENTRY_DSN
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {

    private val CHANNEL = "ppp.nic.cz/config"
    private val SENTRY = "getSentryDsn"


    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
                .setMethodCallHandler { call, result ->
                    if (call.method == SENTRY) {
                        val sentryDsn = SENTRY_DSN;
                        if (sentryDsn != null) {
                            result.success(sentryDsn);
                        } else {
                            result.error("UNAVAILABLE", "NO Sentry DSN was found", null)
                        }
                    } else {
                        result.notImplemented()
                    }
                }
    }

}
