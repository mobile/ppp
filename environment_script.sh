#!/usr/bin/env bash

GRADLE_PROPERTIES_FILE=~/.gradle/gradle.properties
DEBUG_CONFIG=ios/Flutter/Debug.xcconfig
RELEASE_CONFIG=ios/Flutter/Release.xcconfig

sed -i '' '/PPP_SENTRY_DSN/d' $DEBUG_CONFIG

sed -i '' '/PPP_SENTRY_DSN/d' $RELEASE_CONFIG

function getProperty {
    PROP_KEY=$1
    PROP_VALUE=`cat $GRADLE_PROPERTIES_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
    echo $PROP_VALUE
}

function setHttpsForConfig {
    OLD_PROPERTY=$1
    NEW_PROPERTY=$(echo $OLD_PROPERTY | cut -c 9-)
    echo 'https:/$()/'$NEW_PROPERTY
}

echo "Read properties..."

SENTRY_DSN_RELEASE=$(setHttpsForConfig $(getProperty "PPP_SENTRY_DSN_RELEASE"))
SENTRY_DSN_DEBUG=$(setHttpsForConfig $(getProperty "PPP_SENTRY_DSN_DEBUG"))

echo "Debug config setting..."

echo "PPP_SENTRY_DSN="$SENTRY_DSN_DEBUG >> $DEBUG_CONFIG

echo "Release config setting..."

echo "PPP_SENTRY_DSN="$SENTRY_DSN_RELEASE >> $RELEASE_CONFIG

echo "Success!"