#!/usr/bin/env bash

INFO_PLIST=$1

VERSION_CODE_GIT=$(git log -1 --format=%at)
VERSION_NAME_GIT=$(git describe --abbrev=0 --tags)

echo "VERSION CODE:"$VERSION_CODE_GIT
echo "VERSION NAME:"$VERSION_NAME_GIT

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $VERSION_CODE_GIT" "$INFO_PLIST"
/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $VERSION_NAME_GIT" "$INFO_PLIST"
