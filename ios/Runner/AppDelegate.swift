import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

            let controller : FlutterViewController = window?.rootViewController as! FlutterViewController;

            let configChannel = FlutterMethodChannel(name: "ppp.nic.cz/config",
                                                     binaryMessenger: controller.binaryMessenger)
            configChannel.setMethodCallHandler({(call: FlutterMethodCall, result: FlutterResult) -> Void in
                if("getSentryDsn" == call.method){
                    result(Bundle.main.infoDictionary?["PPPSentryDSN"])
                }
            })

    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
